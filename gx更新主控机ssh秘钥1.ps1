﻿#建议保存编码为：bom头 + utf8
#判断有秘钥1后，移动到秘钥3。重生成秘钥1。

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	if (Test-Path -LiteralPath "$env:USERPROFILE\.ssh\id_rsa")
	{
		if (Test-Path -LiteralPath "${global:kasini3000目录}\ssh_key_files_old2")
		{
			Remove-Item -Path "${global:kasini3000目录}\ssh_key_files_old2\*"
		}
		else
		{
			Write-Warning "警告，找不到旧2密钥路径，将建立它"
			mkdir -Name "${global:kasini3000目录}\ssh_key_files_old2"
		}
		Move-Item -Force -LiteralPath "$env:USERPROFILE\.ssh\id_rsa" -Destination "${global:kasini3000目录}\ssh_key_files_old2"
		Move-Item -Force -LiteralPath "$env:USERPROFILE\.ssh\id_rsa.pub" -Destination "${global:kasini3000目录}\ssh_key_files_old2"
		& 'jl建立主控机ssh秘钥1z.ps1'
	}
	else
	{
		& 'jl建立主控机ssh秘钥1z.ps1'
	}
}


if ($IsLinux -eq $True)
{
	if (Test-Path -LiteralPath '/root/.ssh/id_rsa')
	{
		if (Test-Path -LiteralPath '/etc/kasini3000/ssh_key_files_old2/')
		{
			Remove-Item -Path '/etc/kasini3000/ssh_key_files_old2/*'
		}
		else
		{
			Write-Warning "警告，找不到旧2密钥路径，将建立它"
			mkdir -Name '/etc/kasini3000/ssh_key_files_old2/'
		}
		Move-Item -Force -LiteralPath '/root/.ssh/id_rsa' -Destination '/etc/kasini3000/ssh_key_files_old2/'
		Move-Item -Force -LiteralPath '/root/.ssh/id_rsa.pub' -Destination '/etc/kasini3000/ssh_key_files_old2/'
		& 'jl建立主控机ssh秘钥1z.ps1'
	}
	else
	{
		& 'jl建立主控机ssh秘钥1z.ps1'
	}
	chmod 600 /root/.ssh/id_rsa
	chmod 600 /root/.ssh/id_rsa.pub
	chmod 600 /etc/kasini3000/ssh_key_files_old2/*
	chmod 600 /etc/kasini3000/ssh_key_files_old1/*
}

exit 0
