﻿#建议保存编码为：bom头 + utf8

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$esxi宿主机ip
)


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$global:当前被控机_ip = $null
$global:当前被控机_组 = $null
$global:当前被控机_uuid = $null
$global:当前被控机_os = $null
$global:当前被控机_esxi宿主机 = $null
& 'zd只读nodelist文件.ps1'

$当前被控机 = $global:所有被控机 | Where-Object { $_.ip -eq $esxi宿主机ip }
if ($当前被控机.ip -ne $被控机ip地址)
{
	Write-Error "nodelist文件中找不到这个ip地址： ${esxi宿主机ip}"
	$global:当前被控机_esxi宿主机 = @{ip = '错误：nodelist文件中找不到这个ip地址' }

	if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
	{
		& "${global:kasini3000目录}\admin_gui\pic\bc随机报错背景图片.ps1"
	}
	function global:prompt
	{
		"`e[91m`e[44m【{0}】`e[0m{1}> " -f $global:当前被控机_esxi宿主机.ip,$PWD
	}
	exit 11
}

[System.Net.IPAddress]$ip2 = 0
if (   [System.Net.IPAddress]::TryParse(($当前被控机.ip),[ref]$ip2)      )
{
	$global:当前被控机_esxi宿主机 = $当前被控机
	Connect-VIServer -Server $global:当前被控机_esxi宿主机.ip  -User $global:当前被控机_esxi宿主机.用户名 -Password $global:当前被控机_esxi宿主机.当前密码
	#-Port $global:当前被控机_esxi宿主机.端口
	#& 'sx1刷新单个被控机对象的_os类型属性.ps1'
	if ($当前被控机.被控机分组名 -match 'Vcenter')
	{
		Get-VMHost
	}

	if ($当前被控机.被控机分组名 -match 'vmhost')
	{
		Get-VM
	}

	function global:prompt
	{
		"`e[37m`e[44m【{0}】`e[0m{1}> " -f $global:当前被控机_esxi宿主机.ip,$PWD
	}
}
else
{
	Write-Error "nodelist文件中找到这个ip地址： ${esxi宿主机ip}，但ip不合法"
	if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
	{
		& "${global:kasini3000目录}\admin_gui\pic\bc随机报错背景图片.ps1"
	}
	exit 12
}

exit 0
