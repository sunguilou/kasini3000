﻿#建议保存编码为：bom头 + utf8

param
(
	[String]$脚本文件名,
	[scriptblock]$powershell代码块 = { },
	$端口 = 22,
	$传入参数,
	[bool]$复制主控机node_script目录到被控机 = $false
)


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

Write-Verbose '开始在 被控机uuid 执行'

if ($global:当前被控机_uuid -eq $null)
{
	Write-Error "${global:当前被控机_uuid} 为空"
	exit 1
}

if ($global:当前被控机_uuid.length -gt 1)
{
	Write-Error "${global:当前被控机_uuid} 数量太多"
	exit 2
}


$win = 'win7','win8','win10','win2008r2','win2012r2','win2016','win2019'
$linux = 'centos7','centos8','debian8','debian9','debian10','ubuntu1404','ubuntu1604','ubuntu1804','alpine','ubuntu2004'
$all = $win + $linux

if ($global:当前被控机_uuid.被控机os类型 -notin $all)
{
	& 'sx1刷新单个被控机对象的_os类型属性.ps1'
}

if ($global:当前被控机_uuid.被控机os类型 -notin $all)
{
	Write-Error  ("错误：当前被控机【{0}】【端口不通】或【账户密码不对】。任务无法运行。退出码4" -f ${global:当前被控机_uuid}.ip)
	exit 4
}

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	if ( $global:当前被控机_uuid.被控机os类型 -in $win)
	{
		& 'run_win2win5985_pwd.ps1'  -目的ip地址 $global:当前被控机_uuid.ip -powershell代码块 $powershell代码块  -脚本文件名 $脚本文件名  -传入参数 $传入参数 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
	}

	if ( $global:当前被控机_uuid.被控机os类型 -in $linux)
	{
		& 'run_win2linux_key_pwd.ps1'  -目的ip地址 $global:当前被控机_uuid.ip -powershell代码块 $powershell代码块  -脚本文件名 $脚本文件名  -传入参数 $传入参数  -端口 $端口 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
	}
}

if ($IsLinux -eq $True)
{
	if ( $global:当前被控机_uuid.被控机os类型 -in $win)
	{
		Write-Error "无法从【linux主控机】控制【win被控机】"
		exit 3
	}

	if ( $global:当前被控机_uuid.被控机os类型 -in $linux)
	{
		& 'run_linux2linux_key_pwd.ps1'  -目的ip地址 $global:当前被控机_uuid.ip -powershell代码块 $powershell代码块  -脚本文件名 $脚本文件名  -传入参数 $传入参数  -端口 $端口 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
	}
}

Write-Verbose '结束在 被控机uuid 执行'

exit 0
