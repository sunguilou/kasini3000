﻿#建议保存编码为：bom头 + utf8

param
(
	[Alias("file")][String]$ps1脚本文件名,

	[parameter(Position = 0)][Alias("scriptblock")][scriptblock]$powershell代码块 = { },

	[Alias("allparameter")]$全部传入参数,

	$复制主控机node_script目录到被控机 = $false
)

function yg严格测试布尔型变量($被测试的布尔形变量)
{
	if (($被测试的布尔形变量 -eq 1) -or ($被测试的布尔形变量 -eq $true) -or ($被测试的布尔形变量 -eq 'true'))
	{
		return $true
	}
	elseif (($被测试的布尔形变量 -eq 0) -or ($被测试的布尔形变量 -eq $false) -or ($被测试的布尔形变量 -eq 'false'))
	{
		return $false
	}
	else
	{
		Write-Error '错误：不合法的布尔型值，错误码999'
		exit 999
	}
}

[bool]$复制主控机node_script目录到被控机 = yg严格测试布尔型变量 $复制主控机node_script目录到被控机



if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$msg =
@'
欢迎来到【卡死你3000 run】，这是一个简易命令运行器。别名=【树懒运行】
它可以顺序（非并发）运行命令。它没有异步。
它直接输出。它没有日志。它不写数据库。
它的使用场合是：直接运行【命令块】或【脚本】，用于调试。
'@


Write-Host $msg -ForegroundColor Green

& 'v-kai开启详细信息输出.ps1'

$script:在k_commit中 = $false

if ($global:当前被控机_ip -ne $null)
{
	& 'k_run_ip.ps1' -脚本文件名 $脚本文件名 -powershell代码块 $powershell代码块 -全部传入参数 $全部传入参数 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
}
else
{
	if ($global:当前被控机_uuid -ne $null)
	{
		& 'k_run_uuid.ps1' -脚本文件名 $脚本文件名 -powershell代码块 $powershell代码块 -全部传入参数 $全部传入参数 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
	}
	else
	{
		if ($global:当前被控机_组 -ne $null)
		{
			& 'k_run_g.ps1' -脚本文件名 $脚本文件名 -powershell代码块 $powershell代码块 -全部传入参数 $全部传入参数 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
		}
		else
		{
			if ($global:当前被控机_os -ne $null)
			{
				& 'k_run_os.ps1' -脚本文件名 $脚本文件名 -powershell代码块 $powershell代码块 -全部传入参数 $全部传入参数 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
			}
			else
			{
				& 'k_run_all.ps1' -脚本文件名 $脚本文件名 -powershell代码块 $powershell代码块 -全部传入参数 $全部传入参数 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
			}
		}
	}
}

Write-Host '卡死你3000 run 执行完毕' -ForegroundColor Green

exit 0
