﻿#建议保存编码为：bom头 + utf8

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$url1 = 'https://gitee.com/chuanjiao10/kasini3000.git'
$url2 = 'https://github.com/chuanjiao11/kasini3000.git'

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	Set-Location $env:TEMP
	Remove-Item -LiteralPath "${env:TEMP}\kasini3000" -Recurse -Force -Confirm:$false
	Start-Sleep -Seconds 3
	mkdir "${env:TEMP}\kasini3000"

	try
	{
		$null = Invoke-RestMethod -Uri 'https://gitee.com/chuanjiao10/' -TimeoutSec 10
	}
	catch
	{
		Write-Error '【卡死你3000】gitee上的git源连接失败'
		$gitee = $false
	}
	finally
	{

	}
	$gitee = $True

	if ($gitee -eq $True)
	{
		Write-Verbose '正在使用gitee源更新。。。'
		Set-Location 'c:\ProgramData\kasini3000\master_script\kasini3000\git\cmd'
		git.exe clone --verbose  $url1  "${env:TEMP}\kasini3000"
		Remove-Item -LiteralPath "${env:TEMP}\kasini3000\.git" -Recurse -Force -Confirm:$false
		Copy-Item -LiteralPath "${env:TEMP}\kasini3000" -Destination 'c:\ProgramData\kasini3000' -Force -Recurse
		Start-Sleep -Seconds 3
		Remove-Item -LiteralPath "${env:TEMP}\kasini3000" -Recurse -Force -Confirm:$false
	}
	else
	{
		try
		{
			$null = Invoke-RestMethod -Uri 'https://github.com/chuanjiao11/' -TimeoutSec 10
		}
		catch
		{
			Write-Error '【卡死你3000】github上的git源连接又失败。【卡死你3000】升级失败退出。退出码1'
			exit 1
		}
		finally
		{

		}
		Write-Verbose '正在使用github源更新。。。'
		Set-Location 'c:\ProgramData\kasini3000\master_script\kasini3000\git\cmd'
		git.exe clone --verbose  $url2  "${env:TEMP}\kasini3000"
		Remove-Item -LiteralPath "${env:TEMP}\kasini3000\.git" -Recurse -Force -Confirm:$false
		Copy-Item -LiteralPath "${env:TEMP}\kasini3000" -Destination 'c:\ProgramData\kasini3000' -Force -Recurse
		Start-Sleep -Seconds 3
		Remove-Item -LiteralPath "${env:TEMP}\kasini3000" -Recurse -Force -Confirm:$false
	}
}

if ($IsLinux -eq $True)
{
	$env:LANG = 'en_US.UTF-8'
	Set-Location /tmp
	Remove-Item -LiteralPath "/tmp/kasini3000" -Recurse -Force -Confirm:$false
	Start-Sleep -Seconds 3
	mkdir "/tmp/kasini3000"

	try
	{
		$null = Invoke-RestMethod -Uri 'https://gitee.com/chuanjiao10/' -TimeoutSec 10
	}
	catch
	{
		Write-Error '【卡死你3000】gitee上的git源连接失败'
		$gitee = $false
	}
	finally
	{

	}
	$gitee = $True

	if ($gitee -eq $True)
	{
		Write-Verbose '正在使用gitee源更新。。。'
		/usr/bin/git clone --verbose  $url1  "/tmp/kasini3000"
		Remove-Item -LiteralPath "/tmp/kasini3000/.git" -Recurse -Force -Confirm:$false
		Copy-Item -LiteralPath "/tmp/kasini3000" -Destination '/etc' -Force -Recurse
		Start-Sleep -Seconds 3
		Remove-Item -LiteralPath "/tmp/kasini3000" -Recurse -Force -Confirm:$false
	}
	else
	{
		try
		{
			$null = Invoke-RestMethod -Uri 'https://github.com/chuanjiao11/' -TimeoutSec 10
		}
		catch
		{
			Write-Error '【卡死你3000】github上的git源连接又失败。【卡死你3000】升级失败退出。退出码1'
			exit 1
		}
		finally
		{

		}
		Write-Verbose '正在使用github源更新。。。'
		/usr/bin/git clone --verbose  $url2  "/tmp/kasini3000"
		Remove-Item -LiteralPath "/tmp/kasini3000/.git" -Recurse -Force -Confirm:$false
		Copy-Item -LiteralPath "/tmp/kasini3000" -Destination '/etc' -Force -Recurse
		Start-Sleep -Seconds 3
		Remove-Item -LiteralPath "/tmp/kasini3000" -Recurse -Force -Confirm:$false
	}
}

Write-Host -ForegroundColor green '【卡死你3000】升级成功！'
exit 0
