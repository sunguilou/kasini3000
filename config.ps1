﻿#建议保存编码为：bom头 + utf8

if ($global:config -eq $true)
{
	exit
}
$global:config = $true



$global:kcommit并发进程数 = 99 #max count for run simultaneously job by k-commit.ps1
$global:kcommit并发任务_超时 = New-TimeSpan -Hours 1 -Minutes 1
$global:kaiiit后台任务_循环间隔_秒 = 9


$global:kcommitrs并发线程数 = 300 #max count for run simultaneously job by k-commit-rs.ps1
$global:kcommitrs并发任务_超时 = New-TimeSpan -Minutes 11
$global:kcommitrs_的ps版本 = $global:p_w_s_h_6

$global:k库_定时任务_每次执行任务数 = 10 #start new kasini3000 tasks count every cycle #After each loop time，kasini3000 start [number] count of new tasks



$global:u库_定时任务_循环在_秒 = 1,11,21,31,41,51 #User timed loop tasks run at second every minute



$global:u库_进程级别_定时任务_最大并发进程数 = 99 #max count for run simultaneously job by u库_计划表_添加动作331.ps1
$global:u库_进程级别_定时任务_每次执行任务数 = 10 #start new user tasks count every cycle
$global:u库_进程级别_定时任务_超时 = New-TimeSpan -Hours 2 -Minutes 1 # time over process-level scheduled tasks
$global:u库_进程级别任务_的ps版本 = $global:p_w_s_h_7 #psversion ,$global:p_w_s_h_7,$global:p_w_s_h_6,$global:p_w_s_h


$global:u库_线程级别_定时任务_启用 = $false # if true，Enable thread-level scheduled tasks
$global:u库_线程级别_定时任务_最大并发线程数 = 669 #max count for run simultaneously job by k-commit-rs.ps1 and thread-level scheduled tasks.
$global:u库_线程级别_定时任务_每次执行任务数 = 60 #start new user tasks count every cycle
$global:u库_线程级别_定时任务_建立新进程间隔_分钟 = 5 # Rotate a new process every n minutes on thread-level scheduled tasks.
$global:u库_线程级别_定时任务_超时 = New-TimeSpan -Minutes 16 #time over thread-level scheduled tasks
$global:u库_线程级别任务_的ps版本 = $global:p_w_s_h_6 #psversion ,$global:p_w_s_h_7,$global:p_w_s_h_6,$global:p_w_s_h


$global:被控机密码_变更周期 = New-TimeSpan -days 30 #win node change pwd cycle
$global:被控机秘钥_变更周期 = New-TimeSpan -days 60 #linux node change ssh key cycle



if ($IsLinux -eq $True)
{
	$global:kasini3000目录 = '/etc/kasini3000' 	#kasini3000 main folder
	$global:被控机列表文件 = "${global:kasini3000目录}/nodelist.csv" #kasini3000 node list file
	$global:主控机库目录 = "${global:kasini3000目录}/lib" #kasini3000 lib folder
	$global:主控机脚本目录 = "${global:kasini3000目录}/master_script" #kasini3000 master script folder
	$global:被控机脚本目录 = "${global:kasini3000目录}/node_script" #kasini3000 node script folder
	$global:主控机db目录 = "${global:kasini3000目录}/cmdb" #kasini3000 cmdb folder
	$global:卡死你3000日志文件全路径 = "$global:主控机db目录/d当前库/卡死你3000日志文件.txt" #kasini3000 log file
	$global:错误信息文件 = "${global:kasini3000目录}/errormssage_zh-cn.ps1" #kasini3000 error message file
	$env:LANG = 'zh_CN.UTF-8'
}



if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	$global:kasini3000目录 = 'c:\ProgramData\kasini3000'
	$global:被控机列表文件 = "${global:kasini3000目录}\nodelist.csv"
	$global:主控机库目录 = "${global:kasini3000目录}\lib"
	$global:主控机脚本目录 = "${global:kasini3000目录}\master_script"
	$global:被控机脚本目录 = "${global:kasini3000目录}\node_script"
	$global:主控机db目录 = "${global:kasini3000目录}\cmdb"
	$global:卡死你3000日志文件全路径 = "$global:主控机db目录\d当前库\卡死你3000日志文件.txt"
	$global:主控机admin_gui目录 = "${global:kasini3000目录}\admin_gui"
	$global:错误信息文件 = "${global:kasini3000目录}\errormssage_zh-cn.ps1"
}

exit 0
