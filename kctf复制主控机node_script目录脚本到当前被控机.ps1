﻿#建议保存编码为：bom头 + utf8


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$win = 'win7','win8','win10','win2008r2','win2012r2','win2016','win2019'
$linux = 'centos7','centos8','debian8','debian9','debian10','ubuntu1404','ubuntu1604','ubuntu1804','alpine','ubuntu2004'

if ($global:当前被控机_ip -eq $null)
{
	#Write-Error "错误：当前被控机 ${global:当前被控机_ip} 为空"
	exit 1
}

if ($global:当前被控机_ip.length -gt 1)
{
	Write-Error  "错误：当前被控机 ${global:当前被控机_ip} 数量太多"
	exit 2
}

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	if ( $global:当前被控机_ip.被控机os类型 -in $win)
	{
		& 'k_copyto_ip.ps1' -目的ip地址 $global:当前被控机_ip.ip -端口 $端口 -LiteralPath 'c:\ProgramData\kasini3000\node_script' -Destination 'c:\ProgramData\kasini3000\node_script' -Recurse
	}

	if ( $global:当前被控机_ip.被控机os类型 -in $linux)
	{
		& 'k_copyto_ip.ps1' -目的ip地址 $global:当前被控机_ip.ip -端口 $端口 -LiteralPath 'c:\ProgramData\kasini3000\node_script' -Destination '/etc/kasini3000/node_script' -Recurse
	}
}

if ($IsLinux -eq $True)
{
	if ( $global:当前被控机_ip.被控机os类型 -in $win)
	{
		Write-Error  "错误：不支持"
		exit 3
	}

	if ( $global:当前被控机_ip.被控机os类型 -in $linux)
	{
		& 'k_copyto_ip.ps1' -目的ip地址 $global:当前被控机_ip.ip -端口 $端口 -LiteralPath '/etc/kasini3000/node_script' -Destination '/etc/kasini3000/node_script' -Recurse
	}
}

exit 0
