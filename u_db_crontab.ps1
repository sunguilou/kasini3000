﻿#建议保存编码为：bom头 + utf8
#
Start-Sleep -Seconds 1
if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}


$private:u库crontab的磁盘pid = & 'u_db_crontab_读pid.ps1'
if ( ($private:u库crontab的磁盘pid -eq $null) -or ($private:u库crontab的磁盘pid -eq '') )
{

}
else
{
	$private:u库crontab的进程 = Get-Process -id $private:u库crontab的磁盘pid
}

if ($private:u库crontab的进程 -eq $null)
{

}
else
{
	if ( ($private:u库crontab的进程.ProcessName -eq 'pwsh') -or ($private:u库crontab的进程.ProcessName -eq 'powershell') )
	{
		Write-Error "错误：已经有u库crontab的进程，正在运行中！退出码2"
		exit 2
	}
}

& 'u_db_crontab_写pid.ps1'


#----------------------------------------------【u库，进程级别，定时任务】----------------------------------------------
#$global:u库_线程级别_定时任务_启用 -eq $True，u库，进程级别，定时任务，也会执行。
$private:u库_进程级别_定时任务_的磁盘pid = & 'u库定时任务_进程级别_读pid.ps1'
if ( ($private:u库_进程级别_定时任务_的磁盘pid -eq $null) -or ($private:u库_进程级别_定时任务_的磁盘pid -eq '') )
{

}
else
{
	$private:u库_进程级别_定时任务_的进程 = Get-Process -id $private:u库_进程级别_定时任务_的磁盘pid
}

if ($private:u库_进程级别_定时任务_的进程 -eq $null)
{
	$private:无u库_进程级别_定时任务_的进程 = $True
}
else
{
	if ( ($private:u库_进程级别_定时任务_的进程.ProcessName -eq 'pwsh') -or ($private:u库_进程级别_定时任务_的进程.ProcessName -eq 'powershell') )
	{
		Write-Error "信息：已经有u库，进程级别，定时任务的进程，正在运行中！"
	}
	else
	{
		$private:无u库_进程级别_定时任务_的进程 = $True
	}
}

if ( ($private:无u库_进程级别_定时任务_的进程 -eq $True) -and ((Get-Date).Minute -lt 58))
{
	if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
	{
		Start-Process -FilePath  "$global:u库_进程级别任务_的ps版本"  -ArgumentList " -WindowStyle hidden -file ${global:kasini3000目录}/u库定时任务_进程级别.ps1"
	}

	if ($IsLinux -eq $True)
	{
		Start-Process -FilePath  "$global:u库_进程级别任务_的ps版本"  -ArgumentList " -file ${global:kasini3000目录}/u库定时任务_进程级别.ps1"
	}
}
# 在第59分钟，不会新建u库_进程级别_定时任务。
if ((Get-Date).Minute -eq 58)
{
	if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
	{
		Start-Process -FilePath  "$global:u库_进程级别任务_的ps版本"  -ArgumentList " -WindowStyle hidden -file ${global:kasini3000目录}/u库定时任务_进程级别.ps1"
	}

	if ($IsLinux -eq $True)
	{
		Start-Process -FilePath  "$global:u库_进程级别任务_的ps版本"  -ArgumentList " -file ${global:kasini3000目录}/u库定时任务_进程级别.ps1"
	}
}

#----------------------------------------------【u库，线程级别，定时任务】----------------------------------------------
if ($global:u库_线程级别_定时任务_启用 -eq $True )
{
	$private:u库_线程级别_定时任务_的磁盘pid = & 'u库定时任务_线程级别_读pid.ps1'
	if ( ($private:u库_线程级别_定时任务_的磁盘pid -eq $null) -or ($private:u库_线程级别_定时任务_的磁盘pid -eq '') )
	{

	}
	else
	{
		$private:u库_线程级别_定时任务_的进程 = Get-Process -id $private:u库_线程级别_定时任务_的磁盘pid
	}

	if ($private:u库_线程级别_定时任务_的进程 -eq $null)
	{
		$private:无u库_线程级别_定时任务_的进程 = $True
	}
	else
	{
		if ( ($private:u库_线程级别_定时任务_的进程.ProcessName -eq 'pwsh') -or ($private:u库_线程级别_定时任务_的进程.ProcessName -eq 'powershell') )
		{
			Write-Error "信息：已经有u库，线程级别，定时任务的进程，正在运行中！"

			$private:u库_线程级别_定时任务的pid文件 = "$log日志文件全目录/u库_线程级别_pid.txt"
			$private:pid文件对象 = Get-ChildItem -Path $private:u库_线程级别_定时任务的pid文件
			$private:多少分钟前建立的pid文件 = (Get-Date) - $private:pid文件对象.LastWriteTime
			if ($private:多少分钟前建立的pid文件.TotalMinutes -eq ($global:u库_线程级别_定时任务_建立新进程间隔_分钟 - 1))
			{
				if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
				{
					Start-Process -FilePath  "$global:u库_线程级别任务_的ps版本"  -ArgumentList " -WindowStyle hidden -file ${global:kasini3000目录}/u库定时任务_线程级别.ps1"
				}

				if ($IsLinux -eq $True)
				{
					Start-Process -FilePath  "$global:u库_线程级别任务_的ps版本"  -ArgumentList " -file ${global:kasini3000目录}/u库定时任务_线程级别.ps1"
				}

				Write-Verbose '信息：u库，线程级别，定时任务，正在建立新进程'
			}
		}
		else
		{
			$private:无u库_线程级别_定时任务_的进程 = $True
		}
	}

	if ($private:无u库_线程级别_定时任务_的进程 -eq $True)
	{
		if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
		{
			Start-Process -FilePath  "$global:u库_线程级别任务_的ps版本"  -ArgumentList " -WindowStyle hidden -file ${global:kasini3000目录}/u库定时任务_线程级别.ps1"
		}

		if ($IsLinux -eq $True)
		{
			Start-Process -FilePath  "$global:u库_线程级别任务_的ps版本"  -ArgumentList "-file ${global:kasini3000目录}/u库定时任务_线程级别.ps1"
		}

		Write-Verbose '信息：u库，线程级别，定时任务，正在建立新进程'
	}
}

exit 0
