﻿#建议保存编码为：bom头 + utf8

param
(
	[String]$目的ip地址 = $global:当前被控机_uuid.ip,
	$端口 = $global:当前被控机_uuid.端口,
	[String]$LiteralPath,
	[String]$Path,
	[String]$Destination,
	[Switch]$Recurse
	#缩水封装
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

Write-Verbose '从主控机到被控机，复制文件开始'

if ($global:当前被控机_uuid -eq $null)
{
	Write-Error "${global:当前被控机_uuid} 为空"
	exit 1
}

if ($global:当前被控机_uuid.length -gt 1)
{
	Write-Error "${global:当前被控机_uuid} 数量太多"
	exit 2
}

$win = 'win7','win8','win10','win2008r2','win2012r2','win2016','win2019'
$linux = 'centos7','centos8','debian8','debian9','debian10','ubuntu1404','ubuntu1604','ubuntu1804','alpine','ubuntu2004'

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	if ( $global:当前被控机_uuid.被控机os类型 -in $win)
	{
		if ( ($端口 -eq '') -or ($端口 -eq $null) )
		{
			$端口 = 5985
		}
		try
		{
			Test-WSMan -ComputerName $目的ip地址 -Port $端口 -ErrorAction stop
		}
		catch
		{
			Write-Error "错误：目的ip地址【${目的ip地址}】端口不通"
			exit 12
		}
		finally
		{

		}

		& 'zd只读nodelist文件.ps1'
		$当前被控机 = $global:所有被控机 | Where-Object { $_.ip -eq $目的ip地址 }
		if ($当前被控机.ip -ne $目的ip地址)
		{
			Write-Error "错误：当前被控机获取失败 ${当前被控机}"
			exit 13
		}

		$PSRemoting服务器用户名 = $当前被控机.用户名
		$用户名2 = "${目的ip地址}\${PSRemoting服务器用户名}"
		$密码明文 = $当前被控机.当前密码
		$密码密文 = ConvertTo-SecureString $密码明文 -AsPlainText -Force
		$用户名和密码捆绑后的授权信息 = New-Object System.Management.Automation.PSCredential ($用户名2,$密码密文)

		Write-Verbose '使用密码，连接w2w5985开始'
		$private:连接3 = New-PSSession -ComputerName $目的ip地址 -Port $端口 -Credential $用户名和密码捆绑后的授权信息
		if ($private:连接3 -eq $null)
		{
			Write-Error "使用密码，在${目的ip地址}上连接w2w5985失败"
			exit 21
		}

		Write-Verbose '连接成功。现在开始复制文件：'
		if ($LiteralPath -eq '')
		{
			if ($Recurse -eq $True)
			{
				Copy-Item -ToSession $private:连接3 -Path $Path -Destination $Destination -Force -Recurse
			}
			else
			{
				Copy-Item -ToSession $private:连接3 -Path $Path -Destination $Destination -Force
			}
		}
		else
		{
			if ($Recurse -eq $True)
			{
				Copy-Item -ToSession $private:连接3 -LiteralPath $LiteralPath -Destination $Destination -Force -Recurse
			}
			else
			{
				Copy-Item -ToSession $private:连接3 -LiteralPath $LiteralPath -Destination $Destination -Force
			}
		}
		Write-Verbose '复制文件完成，即将断开连接。'
		Remove-PSSession -Session $private:连接3
	}

	if ( $global:当前被控机_uuid.被控机os类型 -in $linux)
	{
		if ( $PSVersionTable.psversion.major -ge 6)
		{

		}
		else
		{
			Write-Error "在win中连接linux，依赖powershell 6"
			exit 12
		}

		if ( ($端口 -eq '') -or ($端口 -eq $null) )
		{
			$端口 = 22
		}

		Write-Verbose '使用ssh秘钥1，连接w2l开始'
		[string]$private:temp011 = ssh.exe -l root -i "$env:USERPROFILE\.ssh\id_rsa"  ${目的ip地址} -p $端口 -o PasswordAuthentication=no 'date' *>&1
		Write-Verbose $private:temp011
		if ( $private:temp011.ToLower().Contains('Permission denied'.ToLower()) )
		{
			$秘钥1连接成功 = $false
			Write-Verbose "使用ssh秘钥1，在${目的ip地址}上连接w2l失败"
		}
		else
		{
			$秘钥1连接成功 = $true
		}

		if ( $private:temp011.ToLower().Contains('timed out'.ToLower()) )
		{
			Write-Error	 "使用ssh秘钥1，在${目的ip地址}上w2l连接超时"
			exit 13
		}

		if ($秘钥1连接成功 -eq $true)
		{
			$private:连接1 = New-PSSession -HostName ${目的ip地址} -Port $端口 -UserName root -KeyFilePath "$env:USERPROFILE\.ssh\id_rsa"
			Write-Verbose '【连接1】连接成功。现在开始复制文件：'
			if ($LiteralPath -eq '')
			{
				if ($Recurse -eq $True)
				{
					$env:LANG = 'en_US.UTF-8'
					Copy-Item -ToSession $private:连接1 -Path $Path -Destination $Destination -Force -Recurse
				}
				else
				{
					$env:LANG = 'en_US.UTF-8'
					Copy-Item -ToSession $private:连接1 -Path $Path -Destination $Destination -Force
				}
			}
			else
			{
				if ($Recurse -eq $True)
				{
					$env:LANG = 'en_US.UTF-8'
					Copy-Item -ToSession $private:连接1 -LiteralPath $LiteralPath -Destination $Destination -Force -Recurse
				}
				else
				{
					$env:LANG = 'en_US.UTF-8'
					Copy-Item -ToSession $private:连接1 -LiteralPath $LiteralPath -Destination $Destination -Force
				}
			}
			Write-Verbose '【连接1】复制文件完成，即将断开连接。'
			Remove-PSSession -Session $private:连接1
		}
		else
		{
			Write-Verbose '使用ssh秘钥2，连接w2l开始'
			[string]$private:temp012 = ssh.exe -l root -i "${global:kasini3000目录}\ssh_key_files_old1\id_rsa"  ${目的ip地址} -p $端口 -o PasswordAuthentication=no 'date' *>&1
			Write-Verbose $private:temp012
			if ( $private:temp012.ToLower().Contains('Permission denied'.ToLower()) )
			{
				Write-Error "使用ssh秘钥2，在${目的ip地址}上连接w2l失败"
				exit 15
			}

			$private:连接2 = New-PSSession -HostName ${目的ip地址} -Port $端口 -UserName root -KeyFilePath  "${global:kasini3000目录}\ssh_key_files_old1\id_rsa"
			Write-Verbose '【连接2】连接成功。现在开始复制文件：'
			if ($LiteralPath -eq '')
			{
				if ($Recurse -eq $True)
				{
					$env:LANG = 'en_US.UTF-8'
					Copy-Item -ToSession $private:连接2 -Path $Path -Destination $Destination -Force -Recurse
				}
				else
				{
					$env:LANG = 'en_US.UTF-8'
					Copy-Item -ToSession $private:连接2 -Path $Path -Destination $Destination -Force
				}
			}
			else
			{
				if ($Recurse -eq $True)
				{
					$env:LANG = 'en_US.UTF-8'
					Copy-Item -ToSession $private:连接2 -LiteralPath $LiteralPath -Destination $Destination -Force -Recurse
				}
				else
				{
					$env:LANG = 'en_US.UTF-8'
					Copy-Item -ToSession $private:连接2 -LiteralPath $LiteralPath -Destination $Destination -Force
				}
			}
			Write-Verbose '【连接2】复制文件完成，即将断开连接。'
			Remove-PSSession -Session $private:连接2
		}
	}
}

if ($IsLinux -eq $True)
{
	if ( $global:当前被控机_uuid.被控机os类型 -in $win)
	{
		Write-Error "无法从【linux主控机】复制文件到【win被控机】"
		exit 1
	}

	if ( $global:当前被控机_uuid.被控机os类型 -in $linux)
	{
		if ( ($端口 -eq '') -or ($端口 -eq $null) )
		{
			$端口 = 22
		}

		Write-Verbose '使用ssh秘钥1，连接l2l开始'
		[string]$private:temp011 = ssh -l root -i '/root/.ssh/id_rsa'  ${目的ip地址} -p $端口 -o PasswordAuthentication=no 'date' *>&1
		Write-Verbose $private:temp011
		if ( $private:temp011.ToLower().Contains('Permission denied'.ToLower()) )
		{
			$秘钥1连接成功 = $false
			Write-Verbose "使用ssh秘钥1，在${目的ip地址}上连接l2l失败"
		}
		else
		{
			$秘钥1连接成功 = $true
		}

		if ( $private:temp011.ToLower().Contains('timed out'.ToLower()) )
		{
			Write-Error	 "使用ssh秘钥1，在${目的ip地址}上连接l2l超时"
			exit 13
		}

		if ($秘钥1连接成功 -eq $true)
		{
			$private:连接5 = New-PSSession -HostName ${目的ip地址} -Port $端口 -UserName root -KeyFilePath '/root/.ssh/id_rsa'
			Write-Verbose '【连接5】连接成功。现在开始复制文件：'
			if ($LiteralPath -eq '')
			{
				if ($Recurse -eq $True)
				{
					$env:LANG = 'en_US.UTF-8'
					Copy-Item -ToSession $private:连接5 -Path $Path -Destination $Destination -Force -Recurse
				}
				else
				{
					$env:LANG = 'en_US.UTF-8'
					Copy-Item -ToSession $private:连接5 -Path $Path -Destination $Destination -Force
				}
			}
			else
			{
				if ($Recurse -eq $True)
				{
					$env:LANG = 'en_US.UTF-8'
					Copy-Item -ToSession $private:连接5 -LiteralPath $LiteralPath -Destination $Destination -Force -Recurse
				}
				else
				{
					$env:LANG = 'en_US.UTF-8'
					Copy-Item -ToSession $private:连接5 -LiteralPath $LiteralPath -Destination $Destination -Force
				}
			}
			Write-Verbose '【连接5】复制文件完成，即将断开连接。'
			Remove-PSSession -Session $private:连接5
		}
		else
		{
			Write-Verbose '使用ssh秘钥2，连接l2l开始'
			[string]$private:temp012 = ssh -l root -i "${global:kasini3000目录}/ssh_key_files_old1/id_rsa"  ${目的ip地址} -p $端口 -o PasswordAuthentication=no 'date' *>&1
			Write-Verbose $private:temp012
			if ( $private:temp012.ToLower().Contains('Permission denied'.ToLower()) )
			{
				Write-Error "使用ssh秘钥2，在${目的ip地址}上连接l2l失败"
				exit 15
			}

			$private:连接6 = New-PSSession -HostName ${目的ip地址} -Port $端口 -UserName root -KeyFilePath  "${global:kasini3000目录}/ssh_key_files_old1/id_rsa"
			Write-Verbose '【连接6】连接成功。现在开始复制文件：'
			if ($LiteralPath -eq '')
			{
				if ($Recurse -eq $True)
				{
					$env:LANG = 'en_US.UTF-8'
					Copy-Item -ToSession $private:连接6 -Path $Path -Destination $Destination -Force -Recurse
				}
				else
				{
					$env:LANG = 'en_US.UTF-8'
					Copy-Item -ToSession $private:连接6 -Path $Path -Destination $Destination -Force
				}
			}
			else
			{
				if ($Recurse -eq $True)
				{
					$env:LANG = 'en_US.UTF-8'
					Copy-Item -ToSession $private:连接6 -LiteralPath $LiteralPath -Destination $Destination -Force -Recurse
				}
				else
				{
					$env:LANG = 'en_US.UTF-8'
					Copy-Item -ToSession $private:连接6 -LiteralPath $LiteralPath -Destination $Destination -Force
				}
			}
			Write-Verbose '【连接6】复制文件完成，即将断开连接。'
			Remove-PSSession -Session $private:连接6
		}
	}

}

Write-Verbose '从主控机到被控机，复制文件完成'

exit 0
