﻿#建议保存编码为：bom头 + utf8


$a = @{Expression = { $_.'ip' };Label = 'ip' ;width = 16 },
#@{Expression = { $_.'端口' };Label = '端口' ;width = 5 },
@{Expression = { $_.'被控机os类型' };Label = '被控机os类型' ;width = 25 },
@{Expression = { $_.'被控机显示名' };Label = '被控机显示名' ;width = 25 },
@{Expression = { $_.'被控机分组名' };Label = '被控机分组名' ;width = 13 }
#@{Expression = { $_.被控机uuid };width = 15 }

if ($IsLinux -eq $True)
{
	Write-Host '被控机列表文件存储位置：【/etc/kasini3000/nodelist.csv】' -ForegroundColor green
}

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	Write-Host '被控机列表文件存储位置：【c:\ProgramData\kasini3000\nodelist.csv】' -ForegroundColor green
}

Write-Host '====================【所有被控机列表】===================='
$global:所有被控机 | Sort-Object -Property ip | Format-Table  $a

exit 0
