﻿#建议保存编码为：bom头 + utf8

param
(
	[Alias("ipaddress")][String]$目的ip地址,
	[Alias("port")][uint16]$端口,
	[String]$LiteralPath,
	[String]$Path,
	[String]$Destination,
	[Switch]$Recurse
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$msg =
@'
卡死你3000 Copy files/folders To 被控机，简称kct。
'@

Write-Host $msg -ForegroundColor Green

$script:在k_commit中 = $false

if ($global:当前被控机_ip -ne $null)
{
	& 'k_copyto_ip.ps1' -目的ip地址 $global:当前被控机_ip.ip -端口 $端口 -LiteralPath $LiteralPath -Path $Path -Destination $Destination -Recurse $Recurse
}
else
{
	if ($global:当前被控机_uuid -ne $null)
	{
		& 'k_copyto_uuid.ps1' -目的ip地址 $global:当前被控机_uuid.ip -端口 $端口 -LiteralPath $LiteralPath -Path $Path -Destination $Destination -Recurse $Recurse
	}
	else
	{
		if ($global:当前被控机_组 -ne $null)
		{
			& 'k_copyto_g.ps1' -目的ip地址 $目的ip地址 -端口 $端口 -LiteralPath $LiteralPath -Path $Path -Destination $Destination -Recurse $Recurse
		}
		else
		{
			if ($global:当前被控机_os -ne $null)
			{
				& 'k_copyto_os.ps1' -目的ip地址 $目的ip地址 -端口 $端口 -LiteralPath $LiteralPath -Path $Path -Destination $Destination -Recurse $Recurse
			}
			else
			{
				& 'k_copyto_all.ps1' -目的ip地址 $目的ip地址 -端口 $端口 -LiteralPath $LiteralPath -Path $Path -Destination $Destination -Recurse $Recurse
			}
		}
	}
}

Write-Host '卡死你3000 Copy files/folders To 被控机，文件复制完毕' -ForegroundColor Green

exit 0
