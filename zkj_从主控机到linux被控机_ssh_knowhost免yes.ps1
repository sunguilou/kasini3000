﻿#建议保存编码为：bom头 + utf8

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	if (Test-Path -LiteralPath "$env:USERPROFILE\.ssh\config")
	{
	}
	else
	{
		New-Item -Path "$env:USERPROFILE\.ssh\config"
	}

	if (Select-String -Pattern 'StrictHostKeyChecking' -LiteralPath "$env:USERPROFILE\.ssh\config" -SimpleMatch -Quiet)
	{
	}
	else
	{
		Add-Content -LiteralPath "$env:USERPROFILE\.ssh\config" -Value "StrictHostKeyChecking no" -Encoding Ascii
	}
}


if ($IsLinux -eq $True)
{
	if (Test-Path -LiteralPath '/root/.ssh/config')
	{
	}
	else
	{
		New-Item -Path '/root/.ssh/config'
	}

	if (Select-String -Pattern 'StrictHostKeyChecking' -LiteralPath '/root/.ssh/config' -SimpleMatch -Quiet)
	{
	}
	else
	{
		Add-Content -LiteralPath '/root/.ssh/config' -Value "StrictHostKeyChecking no" -Encoding Ascii
	}
}

exit 0
