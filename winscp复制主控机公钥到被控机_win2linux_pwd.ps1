﻿#Requires -Modules winscp

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$被控机ip地址,
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[string]$被控机ssh密码明文
)

if ($IsLinux -eq $True)
{
	Write-Error "错误：不支持linux"
	exit 1
}

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'gx更新主控机上的_双公钥文件authorized_keys.ps1'
	$被推送的公钥文件 = "${global:kasini3000目录}\ssh_key_files_old1\authorized_keys"
	if (Test-Path -LiteralPath $被推送的公钥文件)
	{

	}
	else
	{
		Write-Error "错误：找不到 $被推送的公钥文件"
		exit 2
	}

	$用户名 = 'root'
	$用户密码密文 = ConvertTo-SecureString $被控机ssh密码明文 -AsPlainText -Force
	$我的登陆凭据 = New-Object System.Management.Automation.PSCredential ($用户名,$用户密码密文)
	$sftp连接参数 = New-WinSCPSessionOption -Protocol Sftp -HostName $被控机ip地址  -Credential  $我的登陆凭据
	$指纹 = Get-WinSCPHostKeyFingerprint -SessionOption $sftp连接参数 -Algorithm SHA-256
	$sftp连接参数.SshHostKeyFingerprint = $指纹

	$private:sftp连接 = New-WinSCPSession -SessionOption $sftp连接参数
	if ($private:sftp连接 -eq $null)
	{
		Write-Error "使用ssh密码，在${目的ip地址}上连接失败"
		exit 3
	}
	else
	{
		Write-Verbose '使用ssh密码，连接成功。'
	}

	Write-Verbose '用winscp+密码，复制主控机ssh公钥到被控机，命令开始。'
	if (Test-WinSCPPath -Path '/root/.ssh' -WinSCPSession $sftp连接)
	{
		Remove-WinSCPItem -Path '/root/.ssh' -Confirm:$false -WinSCPSession $private:sftp连接
	}

	$权限700 = New-WinSCPTransferOption -FilePermissions (New-WinSCPItemPermission -Octal 700)
	New-WinSCPItem -Path '/root/.ssh' -ItemType Directory -TransferOptions  $权限700 -WinSCPSession $private:sftp连接

	$权限600 = New-WinSCPTransferOption -FilePermissions (New-WinSCPItemPermission -Octal 600)
	Send-WinSCPItem -LocalPath $被推送的公钥文件 -RemotePath '/root/.ssh/' -TransferOptions $权限600 -WinSCPSession $private:sftp连接

	Write-Verbose '用winscp+密码，复制主控机ssh公钥到被控机，命令完成。'
	Remove-WinSCPSession -WinSCPSession $private:sftp连接
	exit 0
}
