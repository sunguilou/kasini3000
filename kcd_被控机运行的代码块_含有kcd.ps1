﻿#建议保存编码为：bom头 + utf8

param
(
	[String]$输入脚本代码块
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$排除关键字1 = 'cdall','cdcentos','cddebian','cdg','cdip','cdos','cdubuntu','cduuid','cdwin'
$排除关键字2 = 'k-commit','k-getoutput'

$kcd排除关键字 = $排除关键字1 + $排除关键字2

foreach ($temp in $kcd排除关键字)
{
	if ( $输入脚本代码块.ToLower().Contains($temp.ToLower()) )
	{
		Write-Warning "错误：脚本块内含有排除关键字[${temp}】"
		if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
		{
			& "${global:kasini3000目录}\admin_gui\pic\jg随机警告背景图片.ps1"
		}
		return $true
	}
}
return $false

exit 0
