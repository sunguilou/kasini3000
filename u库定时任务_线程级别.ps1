﻿#建议保存编码为：bom头 + utf8
#
Start-Sleep -Seconds 1
Remove-Module -Name psreadline
if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}


if ($global:u库_线程级别_定时任务_启用 -eq $false)
{
	Write-Error "错误：没有启用线程级别，定时任务。错误码3`n请修改config.ps1"
	exit 3
}

& 'u库定时任务_线程级别_缓存进程_写pid.ps1'

$private:u库_线程级别_定时任务_的磁盘pid = & 'u库定时任务_线程级别_读pid.ps1'
if ( ($private:u库_线程级别_定时任务_的磁盘pid -eq $null) -or ($private:u库_线程级别_定时任务_的磁盘pid -eq '') )
{
	$private:无u库_线程级别_定时任务_的进程 = $True
}
else
{
	$private:u库_线程级别_定时任务_的进程 = Get-Process -id $private:u库_线程级别_定时任务_的磁盘pid
}

if ($private:u库_线程级别_定时任务_的进程 -eq $null)
{
	$private:无u库_线程级别_定时任务_的进程 = $True
}
else
{
	if ( ($private:u库_线程级别_定时任务_的进程.ProcessName -eq 'pwsh') -or ($private:u库_线程级别_定时任务_的进程.ProcessName -eq 'powershell') )
	{
		Write-Error "信息：已经有u库，进程级别，定时任务的进程，正在运行中！"
	}
	else
	{
		$private:无u库_线程级别_定时任务_的进程 = $True
	}
}
$private:开始时间 = Get-Date
if ($private:无u库_线程级别_定时任务_的进程 -eq $True)
{
	Write-Warning '信息：无u库，线程级别，定时任务的进程'
	$private:开始时间2 = $private:开始时间.AddSeconds(-$开始时间.second).AddSeconds(-3)
}
else
{
	Write-Warning '信息：有u库，线程级别，定时任务的进程'
	$private:开始时间2 = $private:开始时间.AddSeconds(-$开始时间.second).AddMinutes(1).AddSeconds(-3)
	#运行时间：从57秒，到n分钟后的48秒。
}
$private:结束时间2 = $private:开始时间2.AddMinutes($global:u库_线程级别_定时任务_建立新进程间隔_分钟).AddSeconds(-9) #应该小于48秒。

$private:下1小时0分钟 = $private:开始时间.AddSeconds(-$开始时间.second).AddHours(1).AddMinutes(-$private:开始时间.Minute)
if ($private:结束时间2 -ge $private:下1小时0分钟)
{
	$private:结束时间2 = $private:下1小时0分钟.AddSeconds(-12) #结束时间2，应该小于0分钟48秒。
}

$private:超时时间2 = $private:开始时间2.AddSeconds(($global:u库_线程级别_定时任务_超时).TotalSeconds)

#main 运行时间：从57秒，到n分钟后的48秒。
Write-Verbose "信息：u库，线程级别，定时任务进程，【$pid】开始运行。开始时间=【$private:开始时间2】，结束时间=【$private:结束时间2】，超时时间=【 $private:超时时间2】"

do
{
	Start-Sleep -Seconds 1
	$private:现在 = Get-Date
}
while ($private:开始时间2 -gt $private:现在)

& 'u库定时任务_线程级别_缓存进程_删pid文件.ps1'
& 'u库定时任务_线程级别_写pid.ps1'
Write-Error '信息：u库，线程级别，定时任务，开始运行'
& 'k_写log.ps1' -被写入的log内容 "信息：u库，线程级别，定时任务进程，【$pid】开始运行。开始时间=【$private:开始时间2】，结束时间=【$private:结束时间2】，超时时间=【 $private:超时时间2】"


$global:u库_线程级别_定时任务_循环中 = $True
while ($global:u库_线程级别_定时任务_循环中 -eq $True)
{
	if ($global:u库_线程级别_定时任务_启用 -eq $True)
	{
		& 'end_invoke_u库_任务表_rs.ps1'
	}

	# 超时11分钟则直接退出。
	if ($private:超时时间2 -lt $private:现在)
	{
		& 'k_写log.ps1' -被写入的log内容 "错误：u库，线程级别，定时任务进程，【$pid】超时退出。"
		Write-Error "错误：u库，线程级别，定时任务进程，【$pid】超时退出。"
		exit 1
	}

	# 超过结束时间，则不会新建任务，无任务则退出。
	if ($private:结束时间2 -lt $private:现在) #$private:结束时间2应该小于51秒。
	{
		if ((Get-RSJob).count -le 0)
		{
			& 'k_写log.ps1' -被写入的log内容 "信息：u库，线程级别，定时任务进程，【$pid】正常结束。"
			Write-Error "信息：u库，线程级别，定时任务进程，【$pid】正常结束。"
			$global:u库_线程级别_定时任务_循环中 = $false
		}
		else
		{
			$private:u库_线程级别_定时任务_的磁盘pid = & 'u库定时任务_线程级别_读pid.ps1'
			if ($private:u库_线程级别_定时任务_的磁盘pid -eq $pid)
			{
				& 'u库定时任务_线程级别_删pid文件.ps1'
			}

			$private:temp995 = "信息：u库，线程级别，定时任务进程，【$pid】，现有任务线程【{0}】个" -f (Get-RSJob).count
			& 'k_写log.ps1' -被写入的log内容 $private:temp995
			& 'k_写log.ps1' -被写入的log内容 ( (Get-RSJob).State | Out-String )
			Start-Sleep -Seconds 60
			$private:现在 = Get-Date
		}
	}
	else
	{
		if ($global:u库_线程级别_定时任务_启用 -eq $True)
		{
			$private:temp998 = & 'kdir-cmdb.ps1' -被查找的库文件名 'u库_计划表_2_u库_任务表.ps1'
			if ( ($private:temp998 -eq '输入的库路径有错误') -and ($private:temp998 -eq '无返回') )
			{
				Write-Error "找不到 'u库_计划表_2_u库_任务表.ps1' "
			}
			else
			{
				& $private:temp998
			}
		}

		do
		{
			Start-Sleep -Seconds 1
			$private:现在 = Get-Date
		}
		while ($private:现在.Second -notin $global:u库_定时任务_循环在_秒)

		$private:u库_定时任务_当前线程数 = (Get-PSSession).count
		if ($private:u库_定时任务_当前线程数 -gt $global:u库_线程级别_定时任务_最大并发线程数)
		{
			& 'k_写log.ps1' -被写入的log内容 "错误：u库，线程级别，定时任务进程，【$pid】，线程数太多。 $private:u库_定时任务_当前线程数 "
		}
		else
		{
			if ($global:u库_线程级别_定时任务_启用 -eq $True)
			{
				& 'begin_invoke_u库_任务表_rs.ps1'
			}
		}
	}
}

exit 0
