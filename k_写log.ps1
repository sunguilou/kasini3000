﻿#建议保存编码为：bom头 + utf8

param
(
	[string]$被写入的log内容 = ''
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$父目录 = Split-Path -LiteralPath $global:卡死你3000日志文件全路径
if (Test-Path -LiteralPath $父目录)
{
	if (Test-Path -LiteralPath $global:卡死你3000日志文件全路径)
	{
		$private:temp = Get-Item -LiteralPath $global:卡死你3000日志文件全路径
		if ($private:temp.Length -gt 128mb)
		{
			$日期 = Get-Date -format 'yyyyMMddHHmm'
			$新文件名 = "${父目录}/${日期}"
			Move-Item -LiteralPath $global:卡死你3000日志文件全路径 -Destination $新文件名 -Force
			Start-Sleep -Seconds 2
		}
	}
	$时间 = Get-Date -Format F
	$输出信息 = "$时间	$被写入的log内容"
	Add-Content -LiteralPath $global:卡死你3000日志文件全路径 -Value $输出信息 -Encoding UTF8
}
else
{
	Write-Error '找不到日志文件目录，请运行【sc首次运行卡死你3000主控机.ps1】'
	exit 1
}

exit 0
