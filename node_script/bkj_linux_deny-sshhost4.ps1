﻿# sshd deny host脚本，powershell版。
# 超过阀值。则进入黑名单。
# 只需要删除 /etc/hosts.deny 文件，即可解除所有阻止的ip
# 用法：用crontab每分钟运行。
# echo "*  *  *  *  *  /usr/bin/pwsh -file /你的路径/deny-sshhost4.ps1"  >>  /var/spool/cron/root
# 支持centos7，centos8，debian9，Ubuntu1604，Ubuntu1804，Ubuntu2004

$ssh连接失败次数阀值 = 4
#--------------------------------------------------------------------
if (Test-Path -LiteralPath '/etc/host_deny_old1.txt')
{
	Remove-Item -LiteralPath '/etc/host_deny_old1.txt' -Force
}

if (Test-Path -LiteralPath '/etc/hosts.deny')
{
	Move-Item -LiteralPath '/etc/hosts.deny' -Destination '/etc/host_deny_old1.txt'
	New-Item -Path '/etc/hosts.deny' -Type File
	chmod 644 /etc/hosts.deny
}

if (Test-Path -LiteralPath '/var/log/secure')
{
	$所有ssh连接失败ip = Get-Content -LiteralPath '/var/log/secure' | Where-Object { $_.split()[5] -eq 'Failed' } | Select-Object @{n = "key";e = { $_.Split()[-4] } } | Group-Object -Property key -NoElement
}

if (Test-Path -LiteralPath '/var/log/auth.log')
{
	$所有ssh连接失败ip = Get-Content -LiteralPath '/var/log/auth.log' | Where-Object { $_.split()[5] -eq 'Failed' } | Select-Object @{n = "key";e = { $_.Split()[-4] } } | Group-Object -Property key -NoElement
}

if ( ($所有ssh连接失败ip -eq $null) -or ($所有ssh连接失败ip -eq '') )
{
	Write-Error '错误：无法获取登录失败信息'
	exit 1
}

foreach ($单个ssh连接失败ip in $所有ssh连接失败ip)
{
	if ($单个ssh连接失败ip.count -gt $ssh连接失败次数阀值)
	{
		$deny字串 = 'sshd: ' + $单个ssh连接失败ip.name
		Add-Content -LiteralPath '/etc/hosts.deny' -Value $deny字串 -Encoding Ascii
	}
	#问：这个脚本谁写的？有问题找谁技术支持？
	#答：QQ群号=183173532
	#名称=powershell交流群
}

Write-Warning '只需要删除 /etc/hosts.deny 文件，即可解除所有阻止的ip'
exit 0
