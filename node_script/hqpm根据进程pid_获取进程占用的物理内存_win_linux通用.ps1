﻿#
param
(
	[uint32]$输入的进程pid
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	if (Get-Process -id $输入的进程pid)
	{
		$win物理内存关键字1 = ( (Get-Process -id $输入的进程pid).ws / 1mb ).tostring('N2') + ' MB'
		return $win物理内存关键字1
	}
	else
	{
		Write-Error '错误：进程pid不存在！'
		exit 1
	}
}

if ($IsLinux -eq $True)
{
	if (Test-Path -LiteralPath "/proc/$输入的进程pid")
	{
		$linux物理内存关键字1 = Select-String -Pattern 'VmRSS' 	-LiteralPath "/proc/$输入的进程pid/status" -SimpleMatch
		$linux物理内存关键字2 = ($linux物理内存关键字1.line.split(' ')[-2] / 1024).tostring('N2') + ' MB'
		return $linux物理内存关键字2
	}
	else
	{
		Write-Error '错误：进程pid不存在！'
		exit 1
	}
}




