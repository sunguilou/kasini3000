﻿#脚本支持非管理员运行，但只能查到自己用户的端口。不是自己这个账户启动的，就查不到。而管理员运行，则可以查到所有用户的。
param
(
	[uint16]$输入的port
)

#Write-Warning '返回【监听】端口的pid'

if ($IsLinux -eq $True)
{
	if ((which netstat) -eq $null)
	{
		Write-Error '错误：找不到netstat命令。建议使用apt-get install net-tools'
		Exit 3
	}

	$username = /usr/bin/whoami
	if ($username -ne 'root')
	{
		Write-Error '错误：非管理员，有些端口查不到'
		#Exit 2
		$private:linux返回1 = sudo netstat -antup
	}
	else
	{
		$private:linux返回1 = netstat -antup
	}

	foreach ($private:temp01 in $private:linux返回1)
	{
		$private:1,$private:2,$private:3,[string]$private:4,$private:5,[string]$private:6,[string]$private:7,$private:8,$private:9 = $private:temp01 -split "\s+|\t+"
		if ( ${private:4}.tolower().contains("$输入的port") -and (${private:6} -eq 'LISTEN') )
		{
			$private:linux返回2 = ${private:7}.split('/')[0]
			return $private:linux返回2
		}
	}
	Write-Error '错误：指定端口上，无进程！'
	exit 1
}


if ( ($PSVersionTable.psversion.major -eq 4) -or ($PSVersionTable.psversion.major -eq 5) -or ($PSVersionTable.psversion.major -eq 7))
{
	$private:win返回 = (Get-NetTCPConnection -State Listen -LocalPort $输入的port).OwningProcess
}

if ($PSVersionTable.psversion.major -eq 6)
{
	$private:win返回 = powershell.exe -command "(Get-NetTCPConnection -State Listen -LocalPort $输入的port).OwningProcess"
}

Write-Verbose ($private:win返回.tostring())
if (($private:win返回 -eq '') -or ($private:win返回 -eq $null))
{
	Write-Error '错误：指定端口上，无进程！'
	exit 1
}

if ($private:win返回 -is [array])
{
	return $private:win返回[-1]
}
else
{
	return $private:win返回
}

exit 0
