﻿#建议保存编码为：bom头 + utf8
#只能在linux主控机本地执行。只能在linux被控机本地执行。若想从主控机到被控机执行，需要用invoke-command -filepath 本脚本名 -session来运行。


param
(
	[array]$被安装的一组包
)


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	Write-Host '未实现'
	exit 1
}

if ($IsLinux -eq $True)
{
	$centos安装关键字 = "yum install -y "
	$ubuntu安装关键字 = $debian安装关键字 = "apt-get install -y "
	$debian10安装关键字 = "apt install -y "
	$amazonlinux安装关键字 = "??? "
	$alpine安装关键字 = "apk add "




	$centos校验关键字 = "rpm -q "
	$ubuntu校验关键字 = $debian校验关键字 = "dpkg --verify "
	$debian10校验关键字 = "dpkg --verify "
	$amazonlinux校验关键字 = "??? "
	$alpine校验关键字 = "apk info |grep "




	function centos7安装校验($被安装的包名)
	{
		$cmd = @"
$centos安装关键字 $被安装的包名
"@

		Invoke-Expression $cmd
		Start-Sleep -Seconds 2

		$verify = @"
$centos校验关键字 $被安装的包名
"@

		$校验结果 = Invoke-Expression $verify
		Write-Verbose "$校验结果"
		if ( $校验结果.ToLower().contains('.x86_64'.ToLower()) )
		{
			Write-Host -ForegroundColor Green "${被安装的包名} 安装成功！"
		}
		else
		{
			Write-Error "${被安装的包名} 安装失败！"
		}
	}

	function ubuntu安装校验($被安装的包名)
	{
		$cmd = @"
$ubuntu安装关键字 $被安装的包名
"@

		Invoke-Expression $cmd
		Start-Sleep -Seconds 2

		$verify = @"
$ubuntu校验关键字 $被安装的包名 *>&1
"@

		$校验结果 = Invoke-Expression $verify
		Write-Verbose "$校验结果"
		if ( $校验结果 -eq $null )
		{
			Write-Host -ForegroundColor Green "${被安装的包名} 安装成功！"
		}
		else
		{
			Write-Error "${被安装的包名} 安装失败！"
		}
	}

	function debian安装校验($被安装的包名)
	{
		$cmd = @"
$debian安装关键字 $被安装的包名
"@

		Invoke-Expression $cmd
		Start-Sleep -Seconds 2

		$verify =
		@"
$debian校验关键字 $被安装的包名 *>&1
"@

		$校验结果 = Invoke-Expression $verify
		Write-Verbose "$校验结果"
		if ( $校验结果 -eq $null )
		{
			Write-Host -ForegroundColor Green "${被安装的包名} 安装成功！"
		}
		else
		{
			Write-Error "${被安装的包名} 安装失败！"
		}
	}

	function alpine安装校验($被安装的包名)
	{
		$cmd = @"
$alpine安装关键字 $被安装的包名
"@

		Invoke-Expression $cmd
		Start-Sleep -Seconds 2

		$verify = @"
$alpine校验关键字 $被安装的包名 *>&1
"@

		$校验结果 = Invoke-Expression $verify
		Write-Verbose "$校验结果"
		if ( $校验结果 -eq $null)
		{
			Write-Host -ForegroundColor Green "${被安装的包名} 安装成功！"
		}
		else
		{
			Write-Error "${被安装的包名} 安装失败！"
		}
	}

	function debian10安装校验($被安装的包名)
	{
		$cmd = @"
$debian10安装关键字 $被安装的包名
"@

		Invoke-Expression $cmd
		Start-Sleep -Seconds 2

		$verify = @"
$debian10校验关键字 $被安装的包名 *>&1
"@

		$校验结果 = Invoke-Expression $verify
		Write-Verbose "$校验结果"
		if ( $校验结果 -eq $null)
		{
			Write-Host -ForegroundColor Green "${被安装的包名} 安装成功！"
		}
		else
		{
			Write-Error "${被安装的包名} 安装失败！"
		}
	}


	function 判断linux发行版和版本
	{
		if (Select-String -Pattern 'centos-8' -Path '/etc/*-release' -SimpleMatch -Quiet)
		{
			return 'centos8'
		}

		if (Select-String -Pattern 'centos-7' -Path '/etc/*-release' -SimpleMatch -Quiet)
		{
			return 'centos7'
		}

		if (Select-String -Pattern 'centos' -Path '/etc/*-release' -SimpleMatch -Quiet)
		{
			return 'centos6'
		}

		if (Select-String -Pattern 'jessie' -Path '/etc/*-release' -SimpleMatch -Quiet)
		{
			return 'debian8'
		}

		if (Select-String -Pattern 'stretch' -Path '/etc/*-release' -SimpleMatch -Quiet)
		{
			return 'debian9'
		}

		if (Select-String -Pattern 'buster' -Path '/etc/*-release' -SimpleMatch -Quiet)
		{
			return 'debian10'
		}

		if (Select-String -Pattern 'Trusty Tahr' -Path '/etc/*-release' -SimpleMatch -Quiet)
		{
			return 'ubuntu1404'
		}

		if (Select-String -Pattern 'Xenial Xerus' -Path '/etc/*-release' -SimpleMatch -Quiet)
		{
			return 'ubuntu1604'
		}

		if (Select-String -Pattern 'Bionic Beaver' -Path '/etc/*-release' -SimpleMatch -Quiet)
		{
			return 'ubuntu1804'
		}

		if (Select-String -Pattern 'Focal Fossa' -Path '/etc/*-release' -SimpleMatch -Quiet)
		{
			return 'ubuntu2004'
		}

		if (Select-String -Pattern 'Alpine' -Path '/etc/*-release' -SimpleMatch -Quiet)
		{
			return 'Alpine'
		}

		return '未知'
	}

	$private:temp999 = 判断linux发行版和版本
	Write-Verbose $private:temp999
	switch ($private:temp999)
	{

		'未知'
		{
			Write-Error "判断os类型失败！"
			exit 2
		}

		'centos7'
		{
			foreach ($被安装的单个包 in $被安装的一组包)
			{
				centos7安装校验 -被安装的包名 $被安装的单个包
			}
			break
		}

		'centos8'
		{
			foreach ($被安装的单个包 in $被安装的一组包)
			{
				centos7安装校验 -被安装的包名 $被安装的单个包
			}
			break
		}

		'ubuntu1404'
		{
			sudo rm -rf /var/lib/dpkg/lock
			sudo rm -rf /var/lib/dpkg/lock-frontend
			dpkg --configure -a
			apt-get update
			foreach ($被安装的单个包 in $被安装的一组包)
			{

				ubuntu安装校验 -被安装的包名 $被安装的单个包
			}
			break
		}

		'ubuntu1604'
		{
			sudo rm -rf /var/lib/dpkg/lock
			sudo rm -rf /var/lib/dpkg/lock-frontend
			dpkg --configure -a
			apt-get update
			foreach ($被安装的单个包 in $被安装的一组包)
			{
				ubuntu安装校验 -被安装的包名 $被安装的单个包
			}
			break
		}

		'ubuntu1804'
		{
			sudo rm -rf /var/lib/dpkg/lock
			sudo rm -rf /var/lib/dpkg/lock-frontend
			dpkg --configure -a
			apt-get update
			foreach ($被安装的单个包 in $被安装的一组包)
			{
				ubuntu安装校验 -被安装的包名 $被安装的单个包
			}
			break
		}

		'ubuntu2004'
		{
			sudo rm -rf /var/lib/dpkg/lock
			sudo rm -rf /var/lib/dpkg/lock-frontend
			dpkg --configure -a
			apt-get update
			foreach ($被安装的单个包 in $被安装的一组包)
			{
				ubuntu安装校验 -被安装的包名 $被安装的单个包
			}
			break
		}

		'debian8'
		{
			/bin/rm -rf /var/lib/dpkg/lock
			/bin/rm -rf /var/lib/dpkg/lock-frontend
			apt-get update
			foreach ($被安装的单个包 in $被安装的一组包)
			{
				debian安装校验 -被安装的包名 $被安装的单个包
			}
			break
		}

		'debian9'
		{
			/bin/rm -rf /var/lib/dpkg/lock
			/bin/rm -rf /var/lib/dpkg/lock-frontend
			dpkg --configure -a
			apt-get update
			foreach ($被安装的单个包 in $被安装的一组包)
			{
				debian安装校验 -被安装的包名 $被安装的单个包
			}
			break
		}

		'debian10'
		{
			/bin/rm -rf /var/lib/dpkg/lock
			/bin/rm -rf /var/lib/dpkg/lock-frontend
			dpkg --configure -a
			apt update
			foreach ($被安装的单个包 in $被安装的一组包)
			{
				debian10安装校验 -被安装的包名 $被安装的单个包
			}
			break
		}

		'alpine'
		{
			apk update
			foreach ($被安装的单个包 in $被安装的一组包)
			{
				alpine安装校验 -被安装的包名 $被安装的单个包
			}
			break
		}

		Default
		{
			Write-Error "判断os类型失败！"
			exit 2
		}
	}

}
exit 0
