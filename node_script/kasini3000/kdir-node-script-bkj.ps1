﻿#建议保存编码为：bom头 + utf8
param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$被查找的脚本文件名
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	${被控机脚本目录} = 'C:\ProgramData\kasini3000'
}

if ($IsLinux -eq $True)
{
	${被控机脚本目录} = '/etc/kasini3000'
}

$被查找的全路径 = "${被控机脚本目录}/${被查找的脚本文件名}"
Write-Verbose $被查找的全路径
if ( $被查找的全路径.Contains('[') -or $被查找的全路径.Contains(']') )
{
	return '输入的库路径有错误'
	#	Exit 1
}

$ErrorActionPreference = 'SilentlyContinue'
$查找结果 = Get-ChildItem -path $被查找的全路径 -Recurse -File
Write-Verbose $查找结果
if ( $查找结果.Length -le 0)
{
	return '无返回'
	#	Exit 2
}

if ($查找结果.Length -gt 1)
{
	$查找结果2 = $查找结果 | Sort-Object -Descending | Select-Object -First 1
	return $查找结果2.fullname
}
else
{
	return $查找结果.fullname
}


exit 0
