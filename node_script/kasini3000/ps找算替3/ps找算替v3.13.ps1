﻿<#
ps找算替，简易版的sed。
#>

[CmdletBinding()]
Param
(
	# v3.13

	[Parameter(Mandatory = $true)][ValidateNotNullOrEmpty()][string]$输入文件全路径,

	[int32]$输入文件编码,

	[string]$输出文件全路径,

	[ValidateSet('utf8无bom','utf8有bom','utf16le有bom')][string]$输出文件编码linux = 'utf8无bom',

	[ValidateSet('utf8无bom','utf8有bom','utf16le有bom')][string]$输出文件编码win = 'utf16le有bom',

	[string]$找什么,

	[string]$找什么_块首,

	[string]$找什么_块尾,

	$用正则查找 = $false,#bool

	$区分大小写 = $true,#bool

	$查找返回的结果必须唯一 = $true, #bool

	[ValidateSet('全文字符串','行','匹配头尾')][string]$查找模式 = '行',

	[string]$替换成啥 = '',

	$不用替换成啥_而用ps代码替换 = $false, #bool

	[ValidateSet('只替换第一个','只替换最后一个','全部替换')][string]$返回结果不唯一时 = '只替换最后一个',

	$找不到则在文件末尾追加 = $false, #bool

	[string]$脚本pid
)

function yg严格测试布尔型变量($被测试的布尔形变量)
{
	if (($被测试的布尔形变量 -eq 1) -or ($被测试的布尔形变量 -eq $true) -or ($被测试的布尔形变量 -eq 'true'))
	{
		return $true
	}
	elseif (($被测试的布尔形变量 -eq 0) -or ($被测试的布尔形变量 -eq $false) -or ($被测试的布尔形变量 -eq 'false'))
	{
		return $false
	}
	else
	{
		Write-Error '错误：不合法的布尔型值，错误码999'
		exit 999
	}
}

[bool]$找不到则在文件末尾追加 = yg严格测试布尔型变量 $找不到则在文件末尾追加
[bool]$不用替换成啥_而用ps代码替换 = yg严格测试布尔型变量 $不用替换成啥_而用ps代码替换
[bool]$查找返回的结果必须唯一 = yg严格测试布尔型变量 $查找返回的结果必须唯一
[bool]$区分大小写 = yg严格测试布尔型变量 $区分大小写
[bool]$用正则查找 = yg严格测试布尔型变量 $用正则查找


function 有bom头吗 ($输入文件全路径2)
{
	if ( ($IsLinux -eq $True) -or ($PSVersionTable.psversion.major -ge 6) )
	{
		$头 = Get-Content -LiteralPath $输入文件全路径2 -AsByteStream -TotalCount 4
	}
	else
	{
		$头 = Get-Content -LiteralPath $输入文件全路径2 -Encoding Byte -TotalCount 4
	}

	[string]$utf8_BOM = "{0:X}{1:X}{2:X}" -f $头
	[string]$utf16_BOM = "{0:X}{1:X}" -f $头
	[string]$utf32_BOM = "{0:X}{1:X}{2:X}{3:X}" -f $头

	if ($utf8_BOM -eq "EFBBBF")
	{
		$script:输入文件编码2 = 65001
		$script:输出文件编码2 = 65001
		Write-Verbose "UTF-8"
		return $true
	}

	if ($utf16_BOM -eq "FFFE")
	{
		$script:输入文件编码2 = 1200
		$script:输出文件编码2 = 1200
		Write-Verbose "Unicode"
		return $true
	}

	if ($utf16_BOM -eq "FEFF")
	{
		$script:输入文件编码2 = 1201
		$script:输出文件编码2 = 1201
		Write-Verbose "Big Endian Unicode"
		return $true
	}

	if ($utf32_BOM -eq "0000FEFF")
	{
		$script:输入文件编码2 = 12001
		$script:输出文件编码2 = 12001
		Write-Verbose "UTF-32, big-endian"
		return $true
	}

	if ($utf32_BOM -eq "FFFE0000")
	{
		$script:输入文件编码2 = 12000
		$script:输出文件编码2 = 12000
		Write-Verbose "UTF-32, little-endian"
		return $true
	}

	return $False
}

$ErrorActionPreference = 'Continue'

if ($脚本pid)
{
	[uint32]$脚本pid2 = $脚本pid -as [uint32]
	if ($pid -ne $脚本pid2)
	{
		Write-Error "错误：不应跨进程传值！ 源脚本进程pid： $脚本pid ，当前进程pid： $pid"
		exit 111
	}
}

if (-not (Test-Path  -LiteralPath $输入文件全路径 -PathType Leaf ))
{
	Write-Error '错误：找不到输入文件'
	exit 1
}

if ((Get-Item -LiteralPath $输入文件全路径).IsReadOnly -eq $true)
{
	Write-Error '错误：发现只读输入文件'
	exit 2
}

if ((Get-ChildItem -LiteralPath $输入文件全路径).Length -gt 64mb )
{
	Write-Error '输入文件太大'
	exit 3
}

if ($输出文件全路径 -eq '' -or $输出文件全路径 -eq $null)
{
	#备份源文件
	$temp001 = Get-Date -Format 'yyyy-MM-dd-HH-mm-ss'
	$temp002 = '.' + $temp001 + '.powershell'
	$输入文件全路径bak = (Get-ChildItem -LiteralPath $输入文件全路径).fullname + $temp002
	Copy-Item -LiteralPath $输入文件全路径 -Destination $输入文件全路径bak
	Start-Sleep -Seconds 1
	$输出文件全路径 = $输入文件全路径
}

if ($输出文件全路径 -eq '直接写输入文件，不需要备份')
{
	$输出文件全路径 = $输入文件全路径
}

if (有bom头吗 ($输入文件全路径))
{

}
else
{
	if ($输入文件编码 -eq '' -or $输入文件编码 -eq $null)
	{
		Write-Error '应输入源文件编码： -输入文件编码'
		exit 5
	}
	else
	{
		$编码表 = 936,950,65001,20127,1200,1201,12000,12001,54936
		if ($输入文件编码 -notin $编码表)
		{
			Write-Error '-输入文件编码 参数值错误'
			exit 6
		}
	}
}

if ($script:输入文件编码2 -ne $null -and $script:输入文件编码2 -ne '')
{
	$script:编码对象 = [System.Text.Encoding]::GetEncoding($script:输入文件编码2)
	#有bom头,编码类型指定错误也无妨
}
else
{
	$script:编码对象 = [System.Text.Encoding]::GetEncoding($输入文件编码)
	#无bom头
}

$源文件内容 = [System.IO.File]::ReadAllText($输入文件全路径,$script:编码对象)
$匹配关键字数量 = 0
$找什么偏移量 = 0
$eof = $false
if ($查找模式 -eq '行' -or $查找模式 -eq '全文字符串')
{
	Remove-Variable '找什么_块首'
	Remove-Variable '找什么_块尾'
	if ($找什么 -eq '' -or $找什么 -eq $null)
	{
		Write-Error '-找什么 参数值错误'
		exit 7
	}

	while ($eof -eq $false)
	{
		$找什么偏移量 = $源文件内容.IndexOf($找什么,$找什么偏移量)
		if ($找什么偏移量 -ne -1)
		{
			$匹配关键字数量++
			$找什么偏移量++
		}
		else
		{
			$eof = $true
		}
	}
}

if ($查找模式 -eq '匹配头尾')
{
	Remove-Variable '找什么'
	if ($找什么_块首 -eq '' -or $找什么_块首 -eq $null)
	{
		Write-Error '-找什么_块首 参数值错误'
		exit 8
	}

	if ($找什么_块尾 -eq '' -or $找什么_块尾 -eq $null)
	{
		Write-Error '-找什么_块尾 参数值错误'
		exit 9
	}

	while ($eof -eq $false)
	{
		$找什么偏移量 = $源文件内容.IndexOf($找什么_块首,$找什么偏移量)
		if ($找什么偏移量 -ne -1)
		{
			$找什么偏移量++
			$找什么偏移量 = $源文件内容.IndexOf($找什么_块尾,$找什么偏移量)
			if ($找什么偏移量 -ne -1)
			{
				$找什么偏移量++
				$匹配关键字数量++
			}
			else
			{
				$eof = $true
			}
		}
		else
		{
			$eof = $true
		}
	}
}


if ($匹配关键字数量 -le 0 -and $找不到则在文件末尾追加 -eq $false)
{
	Write-Error '找不到关键字'
	exit 11
}


if ($PSVersionTable.PSVersion.Major -le 4)
{
	$script:IsWindows = $True
}

if ($查找返回的结果必须唯一 -eq $true)
{
	if ($匹配关键字数量 -gt 1)
	{
		Write-Error '查找返回的关键字，超过1个'
		exit 12
	}
}

if ($用正则查找 -eq $true)
{
	Write-Error '暂时不支持正则查找'
	exit 111
}

if ($区分大小写 -eq $false)
{
	Write-Error '暂不支持忽略大小写'
	exit 112
}


#region main
if ($查找模式 -eq '行')
{
	$global:当前找算替文件_行模式 = [System.IO.File]::ReadAllLines($输入文件全路径,$script:编码对象)
	if ($匹配关键字数量 -le 0 -and $找不到则在文件末尾追加 -eq $true)
	{
		if ($不用替换成啥_而用ps代码替换 -eq $true)
		{
			[string]$替换成啥 = global:计算代码
		}
		[string]$最后一行 = $替换成啥 + "`n"
		$global:当前找算替文件_行模式 = $global:当前找算替文件_行模式 + $最后一行
	}
	else
	{
		$script:已经找到的个数 = 0
		:aaaaa for ($i = 0;$i -lt $global:当前找算替文件_行模式.length ; $i++)
		{
			$行 = $global:当前找算替文件_行模式[$i]
			switch ($返回结果不唯一时)
			{
				'只替换第一个'
				{
					if ($行.contains($找什么))
					{
						if ($不用替换成啥_而用ps代码替换 -eq $true)
						{
							$global:行号 = $i
							[string]$替换成啥 = global:计算代码 $global:当前找算替文件_行模式[$i]
							$global:当前找算替文件_行模式[$i] = $替换成啥
							break aaaaa
						}
						else
						{
							$global:当前找算替文件_行模式[$i] = $替换成啥
							break aaaaa
						}
					}
				}

				'只替换最后一个'
				{
					if ($行.contains($找什么))
					{
						$script:已经找到的个数++
						if ($script:已经找到的个数 -eq $匹配关键字数量)
						{
							if ($不用替换成啥_而用ps代码替换 -eq $true)
							{
								$global:行号 = $i
								[string]$替换成啥 = global:计算代码 $global:当前找算替文件_行模式[$i]
								$global:当前找算替文件_行模式[$i] = $替换成啥
								break aaaaa
							}
							else
							{
								$global:当前找算替文件_行模式[$i] = $替换成啥
								break aaaaa
							}
						}
					}
				}

				'全部替换'
				{
					if ($行.contains($找什么))
					{
						if ($不用替换成啥_而用ps代码替换 -eq $true)
						{
							$global:行号 = $i
							[string]$替换成啥 = global:计算代码 $global:当前找算替文件_行模式[$i]
							$global:当前找算替文件_行模式[$i] = $替换成啥
						}
						else
						{
							$global:当前找算替文件_行模式[$i] = $替换成啥
						}
					}
				}

				default
				{
					Write-Error '查找模式=行，内部错误'
					exit 22
				}
			}
		}
	}

	if ($script:输出文件编码2 -ne $null -and $script:输出文件编码2 -ne '')
	{
		#有bom头
		[System.IO.File]::WriteAllLines($输出文件全路径,$global:当前找算替文件_行模式,$script:编码对象)
		exit 0
	}
	else
	{
		#无bom头
		if ($IsLinux -eq $True) #判断linux
		{
			$输出文件编码3 = $输出文件编码linux
		}

		if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) )  #判断win
		{
			$输出文件编码3 = $输出文件编码win
		}

		if ($输出文件编码3 -eq 'utf8无bom')
		{
			$utf8无bom编码对象 = New-Object System.Text.UTF8Encoding($False)
			[System.IO.File]::WriteAlllines($输出文件全路径,$global:当前找算替文件_行模式,$utf8无bom编码对象) #暗含编码转换，把源编码转换成目标。
			exit 0
		}

		if ($script:输出文件编码3 -eq 'utf8有bom')
		{
			$utf8有bom编码对象 = [System.Text.Encoding]::UTF8
			[System.IO.File]::WriteAlllines($输出文件全路径,$global:当前找算替文件_行模式,$utf8有bom编码对象) #暗含编码转换，把源编码转换成目标。
			exit 0
		}

		if ($script:输出文件编码3 -eq 'utf16le有bom')
		{
			$utf16le有bom编码对象 = [System.Text.Encoding]::Unicode
			[System.IO.File]::WriteAlllines($输出文件全路径,$global:当前找算替文件_行模式,$utf16le有bom编码对象) #暗含编码转换，把源编码转换成目标。
			exit 0
		}
	}
}


if ($查找模式 -eq '全文字符串')
{
	$global:当前找算替文件_字符串模式 = [System.IO.File]::ReadAllText($输入文件全路径,$script:编码对象)
	if ($匹配关键字数量 -le 0 -and $找不到则在文件末尾追加 -eq $true)
	{
		if ($不用替换成啥_而用ps代码替换 -eq $true)
		{
			[string]$替换成啥 = global:计算代码
		}
		[string]$最后一行 = $替换成啥 + "`n"
		$global:当前找算替文件_字符串模式 = $global:当前找算替文件_字符串模式 + $最后一行
	}
	else
	{
		switch ($返回结果不唯一时)
		{
			'只替换第一个'
			{
				$找什么偏移量 = $global:当前找算替文件_字符串模式.IndexOf($找什么,0)
				$global:全文字符串查找模式_字符串开始字符_在文本中的位置 = $找什么偏移量
				$头字符串 = $global:当前找算替文件_字符串模式.Substring(0,$找什么偏移量)
				$尾字符串开始 = $找什么偏移量 + $找什么.length
				$global:全文字符串查找模式_字符串结束字符_在文本中的位置 = $尾字符串开始
				$尾字符串结束 = $global:当前找算替文件_字符串模式.Length - $尾字符串开始
				$尾字符串 = $global:当前找算替文件_字符串模式.Substring($尾字符串开始,$尾字符串结束)
				if ($不用替换成啥_而用ps代码替换 -eq $true)
				{
					[string]$替换成啥 = global:计算代码
				}
				$global:当前找算替文件_字符串模式 = $头字符串 + $替换成啥 + $尾字符串
			}

			'只替换最后一个'
			{
				$找什么偏移量 = $global:当前找算替文件_字符串模式.lastIndexOf($找什么)
				$global:全文字符串查找模式_字符串开始字符_在文本中的位置 = $找什么偏移量
				$头字符串 = $global:当前找算替文件_字符串模式.Substring(0,$找什么偏移量)
				$尾字符串开始 = $找什么偏移量 + $找什么.length
				$global:全文字符串查找模式_字符串结束字符_在文本中的位置 = $尾字符串开始
				$尾字符串结束 = $global:当前找算替文件_字符串模式.Length - $尾字符串开始
				$尾字符串 = $global:当前找算替文件_字符串模式.Substring($尾字符串开始,$尾字符串结束)
				if ($不用替换成啥_而用ps代码替换 -eq $true)
				{
					[string]$替换成啥 = global:计算代码
				}
				$global:当前找算替文件_字符串模式 = $头字符串 + $替换成啥 + $尾字符串
			}

			'全部替换'
			{
				if ($不用替换成啥_而用ps代码替换 -eq $true)
				{
					$eof = $false
					while ($eof -eq $false)
					{
						$找什么偏移量 = $global:当前找算替文件_字符串模式.IndexOf($找什么,0)
						$global:全文字符串查找模式_字符串开始字符_在文本中的位置 = $找什么偏移量
						if ($找什么偏移量 -ne -1)
						{
							$头字符串 = $global:当前找算替文件_字符串模式.Substring(0,$找什么偏移量)
							$尾字符串开始 = $找什么偏移量 + $找什么.length
							$global:全文字符串查找模式_字符串结束字符_在文本中的位置 = $尾字符串开始
							$尾字符串结束 = $global:当前找算替文件_字符串模式.Length - $尾字符串开始
							$尾字符串 = $global:当前找算替文件_字符串模式.Substring($尾字符串开始,$尾字符串结束)
							[string]$替换成啥 = global:计算代码
							$global:当前找算替文件_字符串模式 = $头字符串 + $替换成啥 + $尾字符串
						}
						else
						{
							$eof = $true
						}
					}
				}
				$global:当前找算替文件_字符串模式 = $global:当前找算替文件_字符串模式.replace($找什么,$替换成啥) #区分大小写
			}

			default
			{
				Write-Error '查找模式=全文字符串，内部错误'
				exit 31
			}
		}
	}

	if ($script:输出文件编码2 -ne $null -and $script:输出文件编码2 -ne '')
	{
		#有bom头
		[System.IO.File]::WriteAllText($输出文件全路径,$global:当前找算替文件_字符串模式,$script:编码对象)
		exit 0
	}
	else
	{
		#无bom头
		if ($IsLinux -eq $True) #判断linux
		{
			$输出文件编码3 = $输出文件编码linux
		}

		if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) )  #判断win
		{
			$输出文件编码3 = $输出文件编码win
		}

		if ($输出文件编码3 -eq 'utf8无bom')
		{
			$utf8无bom编码对象 = New-Object System.Text.UTF8Encoding($False)
			[System.IO.File]::WriteAllText($输出文件全路径,$global:当前找算替文件_字符串模式,$utf8无bom编码对象) #暗含编码转换，把源编码转换成目标。
			exit 0
		}

		if ($script:输出文件编码3 -eq 'utf8有bom')
		{
			$utf8有bom编码对象 = [System.Text.Encoding]::UTF8
			[System.IO.File]::WriteAllText($输出文件全路径,$global:当前找算替文件_字符串模式,$utf8有bom编码对象) #暗含编码转换，把源编码转换成目标。
			exit 0
		}

		if ($script:输出文件编码3 -eq 'utf16le有bom')
		{
			$utf16le有bom编码对象 = [System.Text.Encoding]::Unicode
			[System.IO.File]::WriteAllText($输出文件全路径,$global:当前找算替文件_字符串模式,$utf16le有bom编码对象) #暗含编码转换，把源编码转换成目标。
			exit 0
		}
	}
}


if ($查找模式 -eq '匹配头尾')
{
	$global:当前找算替文件_匹配头尾模式 = [System.IO.File]::ReadAllText($输入文件全路径,$script:编码对象)
	if ($匹配关键字数量 -le 0 -and $找不到则在文件末尾追加 -eq $true)
	{
		if ($不用替换成啥_而用ps代码替换 -eq $true)
		{
			[string]$替换成啥 = global:计算代码
		}
		[string]$最后一行 = $替换成啥 + "`n"
		$global:当前找算替文件_匹配头尾模式 = $global:当前找算替文件_匹配头尾模式 + $最后一行
	}
	else
	{
		switch ($返回结果不唯一时)
		{
			'只替换第一个'
			{
				$新文件内容 = ''
				$eof = $false
				while ($eof -eq $false)
				{
					$块首偏移量 = $global:当前找算替文件_匹配头尾模式.IndexOf($找什么_块首)
					$global:匹配头尾查找模式_块首开始字符_在文本中的位置 = $块首偏移量
					if ($块首偏移量 -ne -1)
					{
						$块尾偏移量 = $global:当前找算替文件_匹配头尾模式.IndexOf($找什么_块尾,$($块首偏移量 + 1))
						$global:匹配头尾查找模式_块尾结束字符_在文本中的位置 = $块尾偏移量
						if ($块尾偏移量 -ne -1)
						{
							$头字符串 = $global:当前找算替文件_匹配头尾模式.Substring(0,$块首偏移量)
							$global:匹配头尾查找模式_找到的字符串 = $global:当前找算替文件_匹配头尾模式.Substring($($块首偏移量 + 1),$($块尾偏移量 - $块首偏移量 - 1))
							if ($不用替换成啥_而用ps代码替换 -eq $true)
							{
								[string]$替换成啥 = global:计算代码
							}
							$新文件内容 = $新文件内容 + $头字符串 + $替换成啥

							$新块首偏移量 = $块尾偏移量 + $找什么_块尾.Length
							$global:当前找算替文件_匹配头尾模式 = $global:当前找算替文件_匹配头尾模式.Substring($新块首偏移量)

							$新文件内容 = $新文件内容 + $global:当前找算替文件_匹配头尾模式
							$eof = $true #只替换第一个
						}
						else
						{
							$新文件内容 = $global:当前找算替文件_匹配头尾模式
							$eof = $true #只替换第一个
						}
					}
					else
					{
						$新文件内容 = $global:当前找算替文件_匹配头尾模式
						$eof = $true
					}
				}
			}

			'只替换最后一个'
			{
				$新文件内容 = ''
				$eof = $false
				while ($eof -eq $false)
				{
					$块尾偏移量 = $global:当前找算替文件_匹配头尾模式.lastIndexOf($找什么_块尾)
					$global:匹配头尾查找模式_块尾结束字符_在文本中的位置 = $块尾偏移量
					if ($块尾偏移量 -ne -1)
					{
						$尾字符串 = $global:当前找算替文件_匹配头尾模式.Substring($($块尾偏移量 + $找什么_块尾.Length))
						$global:当前找算替文件_匹配头尾模式 = $global:当前找算替文件_匹配头尾模式.Substring(0,$($global:当前找算替文件_匹配头尾模式.length - $尾字符串.length))
						$块首偏移量 = $global:当前找算替文件_匹配头尾模式.lastIndexOf($找什么_块首)
						$global:匹配头尾查找模式_块首开始字符_在文本中的位置 = $块首偏移量
						if ($块首偏移量 -ne -1)
						{
							$头字符串 = $global:当前找算替文件_匹配头尾模式.Substring(0,$块首偏移量)
							$global:匹配头尾查找模式_找到的字符串 = $global:当前找算替文件_匹配头尾模式.Substring($($块首偏移量 + 1),$($块尾偏移量 - $块首偏移量 - 1))
							if ($不用替换成啥_而用ps代码替换 -eq $true)
							{
								[string]$替换成啥 = global:计算代码
							}
							$新文件内容 = $头字符串 + $替换成啥 + $尾字符串
							$eof = $true
						}
						else
						{
							$新文件内容 = $global:当前找算替文件_匹配头尾模式
							$eof = $true
						}
					}
					else
					{
						$新文件内容 = $global:当前找算替文件_匹配头尾模式
						$eof = $true
					}
				}
			}

			'全部替换'
			{
				$新文件内容 = ''
				$eof = $false
				while ($eof -eq $false)
				{
					$块首偏移量 = $global:当前找算替文件_匹配头尾模式.IndexOf($找什么_块首)
					$global:匹配头尾查找模式_块首开始字符_在文本中的位置 = $块首偏移量
					if ($块首偏移量 -ne -1)
					{
						$块尾偏移量 = $global:当前找算替文件_匹配头尾模式.IndexOf($找什么_块尾,$($块首偏移量 + 1))
						$global:匹配头尾查找模式_块尾结束字符_在文本中的位置 = $块尾偏移量
						if ($块尾偏移量 -ne -1)
						{
							$头字符串 = $global:当前找算替文件_匹配头尾模式.Substring(0,$块首偏移量)
							$global:匹配头尾查找模式_找到的字符串 = $global:当前找算替文件_匹配头尾模式.Substring($($块首偏移量 + 1),$($块尾偏移量 - $块首偏移量 - 1))
							if ($不用替换成啥_而用ps代码替换 -eq $true)
							{
								[string]$替换成啥 = global:计算代码
							}
							$新文件内容 = $新文件内容 + $头字符串 + $替换成啥
							$新块首偏移量 = $块尾偏移量 + $找什么_块尾.Length
							$global:当前找算替文件_匹配头尾模式 = $global:当前找算替文件_匹配头尾模式.Substring($新块首偏移量)
						}
						else
						{
							$新文件内容 = $新文件内容 + $global:当前找算替文件_匹配头尾模式
							$eof = $true
						}
					}
					else
					{
						$新文件内容 = $新文件内容 + $global:当前找算替文件_匹配头尾模式
						$eof = $true
					}
				}
			}

			default
			{
				Write-Error '查找模式=匹配头尾，内部错误'
				exit 41
			}
		}
	}

	if ($script:输出文件编码2 -ne $null -and $script:输出文件编码2 -ne '')
	{
		#有bom头
		[System.IO.File]::WriteAllText($输出文件全路径,$新文件内容,$script:编码对象)
		exit 0
	}
	else
	{
		#无bom头
		if ($IsLinux -eq $True) #判断linux
		{
			$输出文件编码3 = $输出文件编码linux
		}

		if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) )  #判断win
		{
			$输出文件编码3 = $输出文件编码win
		}

		if ($输出文件编码3 -eq 'utf8无bom')
		{
			$utf8无bom编码对象 = New-Object System.Text.UTF8Encoding($False)
			[System.IO.File]::WriteAllText($输出文件全路径,$global:当前找算替文件_匹配头尾模式,$utf8无bom编码对象) #暗含编码转换，把源编码转换成目标。
			exit 0
		}

		if ($script:输出文件编码3 -eq 'utf8有bom')
		{
			$utf8有bom编码对象 = [System.Text.Encoding]::UTF8
			[System.IO.File]::WriteAllText($输出文件全路径,$global:当前找算替文件_匹配头尾模式,$utf8有bom编码对象) #暗含编码转换，把源编码转换成目标。
			exit 0
		}

		if ($script:输出文件编码3 -eq 'utf16le有bom')
		{
			$utf16le有bom编码对象 = [System.Text.Encoding]::Unicode
			[System.IO.File]::WriteAllText($输出文件全路径,$global:当前找算替文件_匹配头尾模式,$utf16le有bom编码对象) #暗含编码转换，把源编码转换成目标。
			exit 0
		}
	}
}

#endregion main


exit 0
