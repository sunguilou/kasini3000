# win linux 测试通过。建议保存编码为：bom头 + utf8
param
(
	[string]$想要检测的关键字 = 'libexec',
	[switch]$只返回_符合条件的进程pid_的数组 #布尔型参数，输入值为真时，返回所有符合的pid
)

$msg = @'
用法：
$a =  jc检测【win-linux进程命令行中】关键字的并发数2.ps1 -想要检测的关键字  'xxx.jar'
$a
如果有两个xxx.jar进程 ,则$a返回2
'@

Write-Warning $msg

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	$private:当前用户下_所有命令的进程 = Get-CimInstance -ClassName 'Win32_Process'
	#用户权限运行则获得当前用户的。管理员权限运行，则获取所有用户的。

	$private:检测出的关键字_或_进程并发数 = 0
	$private:pid数组 = @()
	foreach ($private:temp001 in $private:当前用户下_所有命令的进程)
	{
		if ($private:temp001.CommandLine -like "*$想要检测的关键字*") #不用-match 避免正则
		{
			$private:检测出的关键字_或_进程并发数++
			$private:pid数组 += $private:temp001.ProcessId
		}
	}
	
	if ($只返回_符合条件的进程pid_的数组 -eq $true)
	{
		Write-Host "符合条件的进程pid_的数组:"
		return $private:pid数组
	}
	else
	{	
		Write-Host "检测出的脚本进程并发数:"
		return $private:检测出的关键字_或_进程并发数
	}
}

#问：这个脚本谁写的？有问题找谁技术支持？
#答：QQ群号=183173532
#名称=powershell交流群

if ($IsLinux -eq $True)
{
	$private:当前用户下_所有命令的命令行 = ps -e --format pid,cmd

	$private:检测出的关键字_或_进程并发数 = 0
	$private:pid数组 = @()
	foreach ($private:temp001 in $private:当前用户下_所有命令的命令行)
	{
		[string]$private:temp002 = $private:temp001
		$private:temp002 = [string]$private:temp002.trimstart()
		$private:temp003 = $private:temp002.indexof(' ')
		$private:linux_pid = $private:temp002.substring(0,$private:temp003) 
		$private:linux_命令行 = $temp002.substring(($private:temp003 + 1),($private:temp002.length - $private:linux_pid.length - 1)) 
		
		if ($private:linux_命令行 -like "*$想要检测的关键字*") #不用-match 避免正则
		{
			$private:检测出的关键字_或_进程并发数++
			$private:pid数组 += $private:linux_pid
		}
	}

	if ($只返回_符合条件的进程pid_的数组 -eq $true)
	{
		Write-Host "符合条件的进程pid_的数组:"
		return $private:pid数组
	}
	else
	{	
		Write-Host "检测出的脚本进程并发数:"
		return $private:检测出的关键字_或_进程并发数
	}
}