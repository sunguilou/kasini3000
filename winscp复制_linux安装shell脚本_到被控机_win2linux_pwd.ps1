﻿#Requires -Modules winscp

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$被控机ip地址,
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[string]$被控机上root的ssh密码明文
)

if ($IsLinux -eq $True)
{
	Write-Error "错误：不支持linux"
	exit 1
}

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'

	$被推送的目录 = "${global:kasini3000目录}\kasini3000_agent_linux"
	if (Test-Path -LiteralPath $被推送的目录)
	{

	}
	else
	{
		Write-Error "错误：找不到 $被推送的目录"
		exit 2
	}

	$用户名 = 'root'
	$用户密码密文 = ConvertTo-SecureString $被控机上root的ssh密码明文 -AsPlainText -Force
	$我的登陆凭据 = New-Object System.Management.Automation.PSCredential ($用户名,$用户密码密文)
	$sftp连接参数 = New-WinSCPSessionOption -Protocol Sftp -HostName $被控机ip地址  -Credential  $我的登陆凭据
	$指纹 = Get-WinSCPHostKeyFingerprint -SessionOption $sftp连接参数 -Algorithm SHA-256
	$sftp连接参数.SshHostKeyFingerprint = $指纹

	$private:sftp连接 = New-WinSCPSession -SessionOption $sftp连接参数
	if ($private:sftp连接 -eq $null)
	{
		Write-Error "使用ssh密码，在${目的ip地址}上连接失败"
		exit 3
	}
	else
	{
		Write-Verbose '使用ssh密码，连接成功。'
	}

	Write-Verbose '用winscp+密码，复制【linux安装shell脚本】到被控机，命令开始。'
	Send-WinSCPItem -LocalPath "${被推送的目录}/*.*" -RemotePath '/tmp'  -WinSCPSession $private:sftp连接
	Start-Sleep -Seconds 1
	Write-Verbose '用winscp+密码，复制【linux安装shell脚本】到被控机，命令完成。'
	Remove-WinSCPSession -WinSCPSession $private:sftp连接
	Start-Sleep -Seconds 1
	exit 0
}

