﻿#建议保存编码为：bom头 + utf8

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$目的ip地址
)


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	Invoke-Expression 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	Invoke-Expression '/etc/kasini3000/0k_source.ps1'
}

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	Write-Error "应该使用另一个脚本！"
	exit 11
}

Invoke-Expression 'v-kai开启详细信息输出.ps1'

if ($IsLinux -eq $True)
{
	Invoke-Expression 'zd只读nodelist文件.ps1'
	$当前被控机 = $global:所有被控机 | Where-Object { $_.ip -eq $目的ip地址 }
	if ($当前被控机.ip -ne $目的ip地址)
	{
		Write-Error "错误：当前被控机获取失败 ${当前被控机}"
		exit 13
	}

	#依赖sshpass
	[string]$temp2 = /usr/bin/which sshpass
	if ( $temp2.contains('no sshpass') )
	{
		Write-Error '此linux主控机上没有 sshpass程序'
		exit 14
	}

	Write-Verbose '使用ssh密码，连接开始'
	$sshpass命令 = @"
sshpass -p '$(${当前被控机}.当前密码)' ssh -o StrictHostKeyChecking=no root@${目的ip地址}
"@

	$判断pwsh命令 = $sshpass命令 + @"
 "if [ -h /usr/bin/pwsh ];then echo 'you'; else echo 'wu' ;fi"
"@

	$判断pwsh命令返回 = Invoke-Expression $判断pwsh命令
	if ($LASTEXITCODE -ne 0)
	{
		Write-Error "使用ssh密码，在${目的ip地址}上连接失败"
		exit 21
	}

	if ($判断pwsh命令返回 -eq 'you')
	{
		Write-Verbose '已安装powershell。开始检查powershell-sshd'
		$检查cmd11 = @'
 if grep -Eq 'powershell' /etc/ssh/sshd_config; then echo 'powershell_ssh_good';fi
'@

		$检查cmd12 = $sshpass命令 + @"
 "${检查cmd11}"
"@
		Write-Verbose $检查cmd12
		$检查cmd12返回 = Invoke-Expression $检查cmd12

		if ($检查cmd12返回 -ne 'powershell_ssh_good')
		{
			Write-Warning '开始更改powershell-sshd'
			$检查cmd21 = @'
 echo -e '\nSubsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile\nUseDNS no' >>  /etc/ssh/sshd_config
'@

			$检查cmd22 = $sshpass命令 + @"
 "${检查cmd21}"
"@
			Write-Verbose $检查cmd22
			Invoke-Expression $检查cmd22

			$检查cmd31 = @'
 systemctl restart sshd.service ;rc-service sshd restart
'@

			$检查cmd32 = $sshpass命令 + @"
 "${检查cmd31}"
"@
			Write-Verbose $检查cmd32
			Invoke-Expression $检查cmd32
			Write-Warning '更改powershell-sshd结束'
		}
		Write-Verbose '检查powershell-sshd结束'

	}

	if ($判断pwsh命令返回 -eq 'wu')
	{
		Write-Warning  "在${目的ip地址}上,未安装pwsh"
		#region centos7
		$判断centos1 = @'
 if grep -iEq 'CentOS-7' /etc/*-release; then echo 'CentOS7';fi
'@

		$判断centos2 = $sshpass命令 + @"
 "${判断centos1}"
"@

		$判断centos返回 = Invoke-Expression $判断centos2
		if ($判断centos返回 -eq 'CentOS7')
		{
			$判断centos61 = @'
 rpm -q centos-release
'@

			$判断centos62 = $sshpass命令 + @"
 "${判断centos61}"
"@

			$判断centos6返回 = Invoke-Expression $判断centos62
			if ( $判断centos6返回.contains('el6'.tolower()) )
			{
				Write-Error '不支持centos6'
				Exit 12
			}

			Write-Verbose "在${目的ip地址}上,在centos7系统上，开始安装pwsh"
			$安装cmd11 = @'
 curl -o /etc/yum.repos.d/microsoft.repo  https://packages.microsoft.com/config/rhel/7/prod.repo ;yum remove -y powershell ;#删除旧版
'@

			$安装cmd12 = $sshpass命令 + @"
 "${安装cmd11}"
"@

			Write-Verbose $安装cmd12
			Invoke-Expression $安装cmd12
			Write-Verbose "在${目的ip地址}上,在centos7系统上，安装pwsh，步骤1完成 ${LASTEXITCODE}"

			$安装cmd21 = @'
 yum install -y powershell
'@

			$安装cmd22 = $sshpass命令 + @"
 "${安装cmd21}"
"@

			Write-Verbose $安装cmd22
			Invoke-Expression $安装cmd22
			if ($LASTEXITCODE -ne 0)
			{
				Invoke-Expression $安装cmd22
				if ($LASTEXITCODE -ne 0)
				{
					Write-Error '安装失败，请使用离线安装包安装。'
					Exit 14
				}
			}
			Write-Verbose "在${目的ip地址}上,在centos7系统上，安装pwsh，步骤2完成 ${LASTEXITCODE}"

			Start-Sleep -Seconds 1
			$安装cmd3 = @'
 echo 'Set-PSReadlineOption -EditMode Windows' > /root/.config/powershell/Microsoft.PowerShell_profile.ps1
'@

			$安装cmd32 = $sshpass命令 + @"
 "${安装cmd3}"
"@

			Write-Verbose $安装cmd32
			Invoke-Expression $安装cmd32
			Write-Verbose "在${目的ip地址}上,在centos7系统上，安装pwsh，步骤3完成 ${LASTEXITCODE}"

			$安装cmd41 = @'
 if grep -Eq 'powershell' /etc/ssh/sshd_config; then echo 'powershell_ssh_good';fi
'@

			$安装cmd42 = $sshpass命令 + @"
 "${安装cmd41}"
"@
			Write-Verbose $安装cmd42
			$安装cmd42返回 = Invoke-Expression $安装cmd42

			if ($安装cmd42返回 -ne 'powershell_ssh_good')
			{
				$安装cmd51 = @'
 echo -e '\nSubsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile\nUseDNS no' >>  /etc/ssh/sshd_config
'@

				$安装cmd52 = $sshpass命令 + @"
 "${安装cmd51}"
"@
				Write-Verbose $安装cmd52
				Invoke-Expression $安装cmd52
				Write-Verbose "在${目的ip地址}上,在centos7系统上，安装pwsh，步骤4完成 ${LASTEXITCODE}"

				$安装cmd61 = @'
 systemctl restart sshd.service
'@

				$安装cmd62 = $sshpass命令 + @"
 "${安装cmd61}"
"@
				Write-Verbose $安装cmd62
				Invoke-Expression $安装cmd62
				Write-Verbose "在${目的ip地址}上,在centos7系统上，安装pwsh，步骤5完成 ${LASTEXITCODE}"
			}
			Write-Warning "在${目的ip地址}上,在centos7系统上，安装pwsh完成"
		}
		#endregion centos7

		#region centos8
		$判断centos1 = @'
 if grep -iEq 'CentOS-8' /etc/*-release; then echo 'CentOS8';fi
'@

		$判断centos2 = $sshpass命令 + @"
 "${判断centos1}"
"@

		$判断centos返回 = Invoke-Expression $判断centos2
		if ($判断centos返回 -eq 'CentOS8')
		{
			$判断centos61 = @'
 rpm -q centos-release
'@

			$判断centos62 = $sshpass命令 + @"
 "${判断centos61}"
"@

			$判断centos6返回 = Invoke-Expression $判断centos62
			if ( $判断centos6返回.contains('el6'.tolower()) )
			{
				Write-Error '不支持centos6'
				Exit 12
			}

			Write-Verbose "在${目的ip地址}上,在centos7系统上，开始安装pwsh"
			$安装cmd11 = @'
 curl -o /etc/yum.repos.d/microsoft.repo  https://packages.microsoft.com/config/rhel/7/prod.repo ;yum remove -y powershell ;#删除旧版
'@

			$安装cmd12 = $sshpass命令 + @"
 "${安装cmd11}"
"@

			Write-Verbose $安装cmd12
			Invoke-Expression $安装cmd12
			Write-Verbose "在${目的ip地址}上,在centos8系统上，安装pwsh，步骤1完成 ${LASTEXITCODE}"

			$安装cmd21 = @'
 yum install -y powershell
'@

			$安装cmd22 = $sshpass命令 + @"
 "${安装cmd21}"
"@

			Write-Verbose $安装cmd22
			Invoke-Expression $安装cmd22
			if ($LASTEXITCODE -ne 0)
			{
				Invoke-Expression $安装cmd22
				if ($LASTEXITCODE -ne 0)
				{
					Write-Error '安装失败，请使用离线安装包安装。'
					Exit 14
				}
			}
			Write-Verbose "在${目的ip地址}上,在centos8系统上，安装pwsh，步骤2完成 ${LASTEXITCODE}"

			Start-Sleep -Seconds 1
			$安装cmd3 = @'
 echo 'Set-PSReadlineOption -EditMode Windows' > /root/.config/powershell/Microsoft.PowerShell_profile.ps1
'@

			$安装cmd32 = $sshpass命令 + @"
 "${安装cmd3}"
"@

			Write-Verbose $安装cmd32
			Invoke-Expression $安装cmd32
			Write-Verbose "在${目的ip地址}上,在centos8系统上，安装pwsh，步骤3完成 ${LASTEXITCODE}"

			$安装cmd41 = @'
 if grep -Eq 'powershell' /etc/ssh/sshd_config; then echo 'powershell_ssh_good';fi
'@

			$安装cmd42 = $sshpass命令 + @"
 "${安装cmd41}"
"@
			Write-Verbose $安装cmd42
			$安装cmd42返回 = Invoke-Expression $安装cmd42

			if ($安装cmd42返回 -ne 'powershell_ssh_good')
			{
				$安装cmd51 = @'
 echo -e '\nSubsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile\nUseDNS no' >>  /etc/ssh/sshd_config
'@

				$安装cmd52 = $sshpass命令 + @"
 "${安装cmd51}"
"@
				Write-Verbose $安装cmd52
				Invoke-Expression $安装cmd52
				Write-Verbose "在${目的ip地址}上,在centos8系统上，安装pwsh，步骤4完成 ${LASTEXITCODE}"

				$安装cmd61 = @'
 systemctl restart sshd.service
'@

				$安装cmd62 = $sshpass命令 + @"
 "${安装cmd61}"
"@
				Write-Verbose $安装cmd62
				Invoke-Expression $安装cmd62
				Write-Verbose "在${目的ip地址}上,在centos8系统上，安装pwsh，步骤5完成 ${LASTEXITCODE}"
			}
			Write-Warning "在${目的ip地址}上,在centos8系统上，安装pwsh完成"
		}
		#endregion centos8

		#region ubuntu1604
		$判断ubuntu16041 = @'
 if grep -Eq 'Xenial Xerus' /etc/issue || grep -Eq 'Xenial Xerus' /etc/*-release; then echo 'ubuntu1604';fi
'@

		$判断ubuntu16042 = $sshpass命令 + @"
 "${判断ubuntu16041}"
"@

		$判断ubuntu1604返回 = Invoke-Expression $判断ubuntu16042
		if ($判断ubuntu1604返回 -eq 'ubuntu1604')
		{
			Write-Verbose "在${目的ip地址}上,在ubuntu1604系统上，开始安装pwsh"
			$安装cmd11 = @'
 sudo rm -rf /var/lib/dpkg/lock;rm -rf /var/lib/dpkg/lock-frontend; dpkg --configure -a;sudo curl -o /tmp/a.deb https://packages.microsoft.com/config/ubuntu/16.04/packages-microsoft-prod.deb ;sudo dpkg -i /tmp/a.deb  ;sudo apt-get update ;sudo apt-get remove -y powershell ;
'@

			$安装cmd12 = $sshpass命令 + @"
 "${安装cmd11}"
"@
			Write-Verbose $安装cmd12
			Invoke-Expression $安装cmd12
			Write-Verbose "在${目的ip地址}上,在ubuntu1604系统上，安装pwsh，步骤1完成 ${LASTEXITCODE}"

			$安装cmd21 = @'
 sudo apt-get install -y powershell
'@

			$安装cmd22 = $sshpass命令 + @"
 "${安装cmd21}"
"@

			Write-Verbose $安装cmd22
			Invoke-Expression $安装cmd22
			if ($LASTEXITCODE -ne 0)
			{
				Invoke-Expression $安装cmd22
				if ($LASTEXITCODE -ne 0)
				{
					Write-Error '安装失败，请使用离线安装包安装。'
					Exit 14
				}
			}

			Write-Verbose "在${目的ip地址}上,在ubuntu1604系统上，安装pwsh，步骤2完成 ${LASTEXITCODE}"

			Start-Sleep -Seconds 1
			$安装cmd3 = @'
 echo 'Set-PSReadlineOption -EditMode Windows' > /root/.config/powershell/Microsoft.PowerShell_profile.ps1
'@

			$安装cmd32 = $sshpass命令 + @"
 "${安装cmd3}"
"@

			Write-Verbose $安装cmd32
			Invoke-Expression $安装cmd32
			Write-Verbose "在${目的ip地址}上,在ubuntu1604系统上，安装pwsh，步骤3完成 ${LASTEXITCODE}"

			$安装cmd41 = @'
 if grep -Eq 'powershell' /etc/ssh/sshd_config; then echo 'powershell_ssh_good';fi
'@

			$安装cmd42 = $sshpass命令 + @"
 "${安装cmd41}"
"@
			Write-Verbose $安装cmd42
			$安装cmd42返回 = Invoke-Expression $安装cmd42

			if ($安装cmd42返回 -ne 'powershell_ssh_good')
			{
				$安装cmd51 = @'
 echo -e '\nSubsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile\nUseDNS no' >>  /etc/ssh/sshd_config
'@

				$安装cmd52 = $sshpass命令 + @"
 "${安装cmd51}"
"@
				Write-Verbose $安装cmd52
				Invoke-Expression $安装cmd52
				Write-Verbose "在${目的ip地址}上,在ubuntu1604系统上，安装pwsh，步骤4完成 ${LASTEXITCODE}"

				$安装cmd61 = @'
 systemctl restart sshd.service
'@

				$安装cmd62 = $sshpass命令 + @"
 "${安装cmd61}"
"@
				Write-Verbose $安装cmd62
				Invoke-Expression $安装cmd62
				Write-Verbose "在${目的ip地址}上,在ubuntu1604系统上，安装pwsh，步骤5完成 ${LASTEXITCODE}"
			}

			Write-Warning "在${目的ip地址}上,在ubuntu1604系统上，安装pwsh完成"
		}
		#endregion ubuntu1604

		#region ubuntu1804
		$判断ubuntu18041 = @'
 if grep -Eq 'Bionic Beaver' /etc/issue || grep -Eq 'Bionic Beaver' /etc/*-release; then echo 'ubuntu1804';fi
'@

		$判断ubuntu18042 = $sshpass命令 + @"
 "${判断ubuntu18041}"
"@

		$判断ubuntu1804返回 = Invoke-Expression $判断ubuntu18042
		if ($判断ubuntu1804返回 -eq 'ubuntu1804')
		{
			Write-Verbose "在${目的ip地址}上,在ubuntu1804系统上，开始安装pwsh"
			$安装cmd11 = @'
 sudo rm -rf /var/lib/dpkg/lock;rm -rf /var/lib/dpkg/lock-frontend; dpkg --configure -a;sudo snap remove powershell ; sudo curl -o /tmp/a.deb https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb ;sudo dpkg -i /tmp/a.deb ;sudo apt-get update ;sudo add-apt-repository universe;sudo apt-get remove -y powershell ;
'@

			$安装cmd12 = $sshpass命令 + @"
 "${安装cmd11}"
"@

			Write-Verbose $安装cmd12
			Invoke-Expression $安装cmd12
			Write-Verbose "在${目的ip地址}上,在ubuntu1804系统上，安装pwsh，步骤1完成 ${LASTEXITCODE}"

			$安装cmd21 = @'
 sudo apt-get install -y powershell
'@

			$安装cmd22 = $sshpass命令 + @"
 "${安装cmd21}"
"@

			Write-Verbose $安装cmd22
			Invoke-Expression $安装cmd22
			if ($LASTEXITCODE -ne 0)
			{
				Invoke-Expression $安装cmd22
				if ($LASTEXITCODE -ne 0)
				{
					Write-Error '安装失败，请使用离线安装包安装。'
					Exit 14
				}
			}

			Write-Verbose "在${目的ip地址}上,在ubuntu1804系统上，安装pwsh，步骤2完成 ${LASTEXITCODE}"

			Start-Sleep -Seconds 1
			$安装cmd3 = @'
 echo 'Set-PSReadlineOption -EditMode Windows' > /root/.config/powershell/Microsoft.PowerShell_profile.ps1
'@

			$安装cmd32 = $sshpass命令 + @"
 "${安装cmd3}"
"@

			Write-Verbose $安装cmd32
			Invoke-Expression $安装cmd32
			Write-Verbose "在${目的ip地址}上,在ubuntu1804系统上，安装pwsh，步骤3完成 ${LASTEXITCODE}"

			$安装cmd41 = @'
 if grep -Eq 'powershell' /etc/ssh/sshd_config; then echo 'powershell_ssh_good';fi
'@

			$安装cmd42 = $sshpass命令 + @"
 "${安装cmd41}"
"@
			Write-Verbose $安装cmd42
			$安装cmd42返回 = Invoke-Expression $安装cmd42

			if ($安装cmd42返回 -ne 'powershell_ssh_good')
			{
				$安装cmd51 = @'
 echo -e '\nSubsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile\nUseDNS no' >>  /etc/ssh/sshd_config
'@

				$安装cmd52 = $sshpass命令 + @"
 "${安装cmd51}"
"@
				Write-Verbose $安装cmd52
				Invoke-Expression $安装cmd52
				Write-Verbose "在${目的ip地址}上,在ubuntu1804系统上，安装pwsh，步骤4完成 ${LASTEXITCODE}"

				$安装cmd61 = @'
 systemctl restart sshd.service
'@

				$安装cmd62 = $sshpass命令 + @"
 "${安装cmd61}"
"@
				Write-Verbose $安装cmd62
				Invoke-Expression $安装cmd62
				Write-Verbose "在${目的ip地址}上,在ubuntu1804系统上，安装pwsh，步骤5完成 ${LASTEXITCODE}"
			}

			Write-Warning "在${目的ip地址}上,在ubuntu1804系统上，安装pwsh完成"
		}
		#endregion ubuntu1804

		#region ubuntu2004
		$判断ubuntu20041 = @'
 if grep -Eq 'Focal Fossa' /etc/issue || grep -Eq 'Focal Fossa' /etc/*-release; then echo 'ubuntu2004';fi
'@

		$判断ubuntu20042 = $sshpass命令 + @"
 "${判断ubuntu20041}"
"@

		$判断ubuntu2004返回 = Invoke-Expression $判断ubuntu20042
		if ($判断ubuntu2004返回 -eq 'ubuntu2004')
		{
			Write-Verbose "在${目的ip地址}上,在ubuntu2004系统上，开始安装pwsh"
			$安装cmd11 = @'
 sudo rm -rf /var/lib/dpkg/lock;rm -rf /var/lib/dpkg/lock-frontend; dpkg --configure -a;sudo snap remove powershell ; sudo curl -o /tmp/a.deb https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb ;sudo dpkg -i /tmp/a.deb ;sudo apt-get update ;sudo add-apt-repository universe;sudo apt-get remove -y powershell ;
'@

			$安装cmd12 = $sshpass命令 + @"
 "${安装cmd11}"
"@

			Write-Verbose $安装cmd12
			Invoke-Expression $安装cmd12
			Write-Verbose "在${目的ip地址}上,在ubuntu2004系统上，安装pwsh，步骤1完成 ${LASTEXITCODE}"

			$安装cmd21 = @'
 sudo apt-get install -y powershell
'@

			$安装cmd22 = $sshpass命令 + @"
 "${安装cmd21}"
"@

			Write-Verbose $安装cmd22
			Invoke-Expression $安装cmd22
			if ($LASTEXITCODE -ne 0)
			{
				Invoke-Expression $安装cmd22
				if ($LASTEXITCODE -ne 0)
				{
					Write-Error '安装失败，请使用离线安装包安装。'
					Exit 14
				}
			}

			Write-Verbose "在${目的ip地址}上,在ubuntu2004系统上，安装pwsh，步骤2完成 ${LASTEXITCODE}"

			Start-Sleep -Seconds 1
			$安装cmd3 = @'
 echo 'Set-PSReadlineOption -EditMode Windows' > /root/.config/powershell/Microsoft.PowerShell_profile.ps1
'@

			$安装cmd32 = $sshpass命令 + @"
 "${安装cmd3}"
"@

			Write-Verbose $安装cmd32
			Invoke-Expression $安装cmd32
			Write-Verbose "在${目的ip地址}上,在ubuntu2004系统上，安装pwsh，步骤3完成 ${LASTEXITCODE}"

			$安装cmd41 = @'
 if grep -Eq 'powershell' /etc/ssh/sshd_config; then echo 'powershell_ssh_good';fi
'@

			$安装cmd42 = $sshpass命令 + @"
 "${安装cmd41}"
"@
			Write-Verbose $安装cmd42
			$安装cmd42返回 = Invoke-Expression $安装cmd42

			if ($安装cmd42返回 -ne 'powershell_ssh_good')
			{
				$安装cmd51 = @'
 echo -e '\nSubsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile\nUseDNS no' >>  /etc/ssh/sshd_config
'@

				$安装cmd52 = $sshpass命令 + @"
 "${安装cmd51}"
"@
				Write-Verbose $安装cmd52
				Invoke-Expression $安装cmd52
				Write-Verbose "在${目的ip地址}上,在ubuntu2004系统上，安装pwsh，步骤4完成 ${LASTEXITCODE}"

				$安装cmd61 = @'
 systemctl restart sshd.service
'@

				$安装cmd62 = $sshpass命令 + @"
 "${安装cmd61}"
"@
				Write-Verbose $安装cmd62
				Invoke-Expression $安装cmd62
				Write-Verbose "在${目的ip地址}上,在ubuntu2004系统上，安装pwsh，步骤5完成 ${LASTEXITCODE}"
			}

			Write-Warning "在${目的ip地址}上,在ubuntu2004系统上，安装pwsh完成"
		}
		#endregion ubuntu2004



		#region debian8
		$判断debian81 = @'
 if grep -Eq 'jessie' /etc/issue || grep -Eq 'jessie' /etc/*-release; then echo 'debian9';fi
'@

		$判断debian82 = $sshpass命令 + @"
 "${判断debian81}"
"@

		$判断debian8返回 = Invoke-Expression $判断debian82
		if ($判断debian8返回 -eq 'debian8')
		{
			Write-Verbose "在${目的ip地址}上,在debian8系统上，开始安装pwsh"
			$安装cmd11 = @'
 rm -rf /var/lib/dpkg/lock;rm -rf /var/lib/dpkg/lock-frontend; dpkg --configure -a;apt-get update ;apt-get install -y curl gnupg apt-transport-https sudo;curl -o /tmp/a.asc https://packages.microsoft.com/keys/microsoft.asc ; apt-key add /tmp/a.asc ;echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-debian-jessie-prod stretch main" > /etc/apt/sources.list.d/microsoft.list ;sudo apt-get update ;sudo apt-get remove -y powershell ; #删除旧版
'@

			$安装cmd12 = $sshpass命令 + @"
 "${安装cmd11}"
"@

			Write-Verbose $安装cmd12
			Invoke-Expression $安装cmd12
			Write-Verbose "在${目的ip地址}上,在debian8系统上，安装pwsh，步骤1完成 ${LASTEXITCODE}"

			$安装cmd21 = @'
 sudo apt-get install -y powershell
'@

			$安装cmd22 = $sshpass命令 + @"
 "${安装cmd21}"
"@

			Write-Verbose $安装cmd22
			Invoke-Expression $安装cmd22
			if ($LASTEXITCODE -ne 0)
			{
				Invoke-Expression $安装cmd22
				if ($LASTEXITCODE -ne 0)
				{
					Write-Error '安装失败，请使用离线安装包安装。'
					Exit 14
				}
			}

			Write-Verbose "在${目的ip地址}上,在debian8系统上，安装pwsh，步骤2完成 ${LASTEXITCODE}"

			Start-Sleep -Seconds 1
			$安装cmd3 = @'
 echo 'Set-PSReadlineOption -EditMode Windows' > /root/.config/powershell/Microsoft.PowerShell_profile.ps1
'@

			$安装cmd32 = $sshpass命令 + @"
 "${安装cmd3}"
"@

			Write-Verbose $安装cmd32
			Invoke-Expression $安装cmd32
			Write-Verbose "在${目的ip地址}上,在debian8系统上，安装pwsh，步骤3完成 ${LASTEXITCODE}"

			$安装cmd41 = @'
 if grep -Eq 'powershell' /etc/ssh/sshd_config; then echo 'powershell_ssh_good';fi
'@

			$安装cmd42 = $sshpass命令 + @"
 "${安装cmd41}"
"@
			Write-Verbose $安装cmd42
			$安装cmd42返回 = Invoke-Expression $安装cmd42

			if ($安装cmd42返回 -ne 'powershell_ssh_good')
			{
				$安装cmd51 = @'
 echo -e '\nSubsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile\nUseDNS no' >>  /etc/ssh/sshd_config
'@

				$安装cmd52 = $sshpass命令 + @"
 "${安装cmd51}"
"@
				Write-Verbose $安装cmd52
				Invoke-Expression $安装cmd52
				Write-Verbose "在${目的ip地址}上,在debian8系统上，安装pwsh，步骤4完成 ${LASTEXITCODE}"

				$安装cmd61 = @'
 systemctl restart sshd.service
'@

				$安装cmd62 = $sshpass命令 + @"
 "${安装cmd61}"
"@
				Write-Verbose $安装cmd62
				Invoke-Expression $安装cmd62
				Write-Verbose "在${目的ip地址}上,在debian8系统上，安装pwsh，步骤5完成 ${LASTEXITCODE}"
			}

			Write-Warning "在${目的ip地址}上,在debian8系统上，安装pwsh完成"
		}
		#endregion debian8

		#region debian9
		$判断debian91 = @'
 if grep -Eq 'stretch' /etc/issue || grep -Eq 'stretch' /etc/*-release; then echo 'debian9';fi
'@

		$判断debian92 = $sshpass命令 + @"
 "${判断debian91}"
"@

		$判断debian9返回 = Invoke-Expression $判断debian92
		if ($判断debian9返回 -eq 'debian9')
		{
			Write-Verbose "在${目的ip地址}上,在debian9系统上，开始安装pwsh"
			$安装cmd11 = @'
 rm -rf /var/lib/dpkg/lock;rm -rf /var/lib/dpkg/lock-frontend; dpkg --configure -a;apt-get update ;apt-get install -y curl gnupg apt-transport-https sudo;curl -o /tmp/a.asc https://packages.microsoft.com/keys/microsoft.asc ; apt-key add /tmp/a.asc ;echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-debian-stretch-prod stretch main" > /etc/apt/sources.list.d/microsoft.list ;sudo apt-get update ;sudo apt-get remove -y powershell ; #删除旧版
'@

			$安装cmd12 = $sshpass命令 + @"
 "${安装cmd11}"
"@

			Write-Verbose $安装cmd12
			Invoke-Expression $安装cmd12
			Write-Verbose "在${目的ip地址}上,在debian9系统上，安装pwsh，步骤1完成 ${LASTEXITCODE}"

			$安装cmd21 = @'
 sudo apt-get install -y powershell
'@

			$安装cmd22 = $sshpass命令 + @"
 "${安装cmd21}"
"@

			Write-Verbose $安装cmd22
			Invoke-Expression $安装cmd22
			if ($LASTEXITCODE -ne 0)
			{
				Invoke-Expression $安装cmd22
				if ($LASTEXITCODE -ne 0)
				{
					Write-Error '安装失败，请使用离线安装包安装。'
					Exit 14
				}
			}

			Write-Verbose "在${目的ip地址}上,在debian9系统上，安装pwsh，步骤2完成 ${LASTEXITCODE}"

			Start-Sleep -Seconds 1
			$安装cmd3 = @'
 echo 'Set-PSReadlineOption -EditMode Windows' > /root/.config/powershell/Microsoft.PowerShell_profile.ps1
'@

			$安装cmd32 = $sshpass命令 + @"
 "${安装cmd3}"
"@

			Write-Verbose $安装cmd32
			Invoke-Expression $安装cmd32
			Write-Verbose "在${目的ip地址}上,在debian9系统上，安装pwsh，步骤3完成 ${LASTEXITCODE}"

			$安装cmd41 = @'
if grep -Eq 'powershell' /etc/ssh/sshd_config; then echo 'powershell_ssh_good';fi
'@

			$安装cmd42 = $sshpass命令 + @"
 "${安装cmd41}"
"@
			Write-Verbose $安装cmd42
			$安装cmd42返回 = Invoke-Expression $安装cmd42

			if ($安装cmd42返回 -ne 'powershell_ssh_good')
			{
				$安装cmd51 = @'
 echo -e '\nSubsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile\nUseDNS no' >>  /etc/ssh/sshd_config
'@

				$安装cmd52 = $sshpass命令 + @"
 "${安装cmd51}"
"@
				Write-Verbose $安装cmd52
				Invoke-Expression $安装cmd52
				Write-Verbose "在${目的ip地址}上,在debian9系统上，安装pwsh，步骤4完成 ${LASTEXITCODE}"

				$安装cmd61 = @'
 systemctl restart sshd.service
'@

				$安装cmd62 = $sshpass命令 + @"
 "${安装cmd61}"
"@
				Write-Verbose $安装cmd62
				Invoke-Expression $安装cmd62
				Write-Verbose "在${目的ip地址}上,在debian9系统上，安装pwsh，步骤5完成 ${LASTEXITCODE}"
			}

			Write-Warning "在${目的ip地址}上,在debian9系统上，安装pwsh完成"
		}
		#endregion debian9

		#region debian10
		$判断debian101 = @'
 if grep -Eq 'buster' /etc/issue || grep -Eq 'buster' /etc/*-release; then echo 'debian10';fi
'@

		$判断debian102 = $sshpass命令 + @"
 "${判断debian101}"
"@

		$判断debian10返回 = Invoke-Expression $判断debian102
		if ($判断debian10返回 -eq 'debian10')
		{
			Write-Verbose "在${目的ip地址}上,在debian10系统上，开始安装pwsh"
			$安装cmd11 = @'
 rm -rf /var/lib/dpkg/lock;rm -rf /var/lib/dpkg/lock-frontend; dpkg --configure -a;apt-get update ;apt-get install -y curl gnupg apt-transport-https sudo;cd /tmp;wget https://packages.microsoft.com/keys/microsoft.asc ; apt-key add /tmp/microsoft.asc ;echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-debian-buster-prod buster main" > /etc/apt/sources.list.d/microsoft.list ;sudo apt-get update ;sudo apt-get remove -y powershell ; #删除旧版
'@

			$安装cmd12 = $sshpass命令 + @"
 "${安装cmd11}"
"@

			Write-Verbose $安装cmd12
			Invoke-Expression $安装cmd12
			Write-Verbose "在${目的ip地址}上,在debian10系统上，安装pwsh，步骤1完成 ${LASTEXITCODE}"

			$安装cmd21 = @'
 sudo apt-get install -y powershell
'@

			$安装cmd22 = $sshpass命令 + @"
 "${安装cmd21}"
"@

			Write-Verbose $安装cmd22
			Invoke-Expression $安装cmd22
			if ($LASTEXITCODE -ne 0)
			{
				Invoke-Expression $安装cmd22
				if ($LASTEXITCODE -ne 0)
				{
					Write-Error '安装失败，请使用离线安装包安装。'
					Exit 14
				}
			}

			Write-Verbose "在${目的ip地址}上,在debian10系统上，安装pwsh，步骤2完成 ${LASTEXITCODE}"

			Start-Sleep -Seconds 1
			$安装cmd3 = @'
 echo 'Set-PSReadlineOption -EditMode Windows' > /root/.config/powershell/Microsoft.PowerShell_profile.ps1
'@

			$安装cmd32 = $sshpass命令 + @"
 "${安装cmd3}"
"@

			Write-Verbose $安装cmd32
			Invoke-Expression $安装cmd32
			Write-Verbose "在${目的ip地址}上,在debian10系统上，安装pwsh，步骤3完成 ${LASTEXITCODE}"

			$安装cmd41 = @'
 if grep -Eq 'powershell' /etc/ssh/sshd_config; then echo 'powershell_ssh_good';fi
'@

			$安装cmd42 = $sshpass命令 + @"
 "${安装cmd41}"
"@
			Write-Verbose $安装cmd42
			$安装cmd42返回 = Invoke-Expression $安装cmd42

			if ($安装cmd42返回 -ne 'powershell_ssh_good')
			{
				$安装cmd51 = @'
 echo -e '\nSubsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile\nUseDNS no' >>  /etc/ssh/sshd_config
'@

				$安装cmd52 = $sshpass命令 + @"
  "${安装cmd51}"
"@
				Write-Verbose $安装cmd52
				Invoke-Expression $安装cmd52
				Write-Verbose "在${目的ip地址}上,在debian10系统上，安装pwsh，步骤4完成 ${LASTEXITCODE}"

				$安装cmd61 = @'
 systemctl restart sshd.service
'@

				$安装cmd62 = $sshpass命令 + @"
 "${安装cmd61}"
"@
				Write-Verbose $安装cmd62
				Invoke-Expression $安装cmd62
				Write-Verbose "在${目的ip地址}上,在debian10系统上，安装pwsh，步骤5完成 ${LASTEXITCODE}"
			}

			Write-Warning "在${目的ip地址}上,在debian10系统上，安装pwsh完成"
		}
		#endregion debian10

		#region alpine
		$判断alpine = @'
 if grep -iEq 'alpine' /etc/issue || grep -iEq 'alpine' /etc/*-release; then echo 'alpine';fi
'@

		$判断alpine2 = $sshpass命令 + @"
 "${判断alpine}"
"@

		Write-Verbose $判断alpine2
		$判断alpine返回 = Invoke-Expression $判断alpine2
		if ($判断alpine返回 -eq 'alpine')
		{
			& 'k-sshfs-copy-a-file.ps1' -源文件全路径 '/etc/kasini3000/kasini3000_agent_linux/alpine_del_dropbear_install_opensshserver.ash' -目标绝对路径_必须是文件夹 '/tmp' -目的ip地址 '192.168.168.93' -ssh密码 $(${当前被控机}.当前密码)
			& 'k-sshfs-copy-a-file.ps1' -源文件全路径 '/etc/kasini3000/kasini3000_agent_linux/alpine_install_powershell.ash' -目标绝对路径_必须是文件夹 '/tmp' -目的ip地址 '192.168.168.93' -ssh密码 $(${当前被控机}.当前密码)

			Write-Verbose "在${目的ip地址}上,在alpine系统上，开始安装openssh-server"
			$安装cmd11 = @'
/bin/ash /tmp/alpine_del_dropbear_install_opensshserver.ash
'@

			$安装cmd12 = $sshpass命令 + @"
 "${安装cmd11}"
"@

			Write-Verbose $安装cmd12
			Invoke-Expression $安装cmd12
			Write-Verbose "在${目的ip地址}上,在alpine系统上，安装openssh-server，完成 ${LASTEXITCODE}"

			$安装cmd21 = @'
/bin/ash /tmp/alpine_install_powershell.ash
'@

			$安装cmd22 = $sshpass命令 + @"
 "${安装cmd21}"
"@

			Write-Verbose "在${目的ip地址}上,在alpine系统上，安装pwsh，开始"
			Invoke-Expression $安装cmd22
			Write-Warning "在${目的ip地址}上,在alpine系统上，安装pwsh，完成"

		}
		#endregion alpine
	}
}


exit 0
