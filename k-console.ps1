﻿#建议保存编码为：bom头 + utf8
#

param
(
	$互斥名字 = 'Global\卡死你3000console'
)

$建立互斥成功否 = $false
$互斥对象 = New-Object System.Threading.Mutex ($true,$互斥名字,[ref]$建立互斥成功否)
if ($建立互斥成功否)
{
	Write-Verbose '互斥成功，开始干活!'
	if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
	{
		if (Test-Path -LiteralPath 'c:\ProgramData\kasini3000\')
		{
			Set-Location 'c:\ProgramData\kasini3000\'
		}
		else
		{
			Write-Error '找不到 卡死你3000程序主目录 "c:\ProgramData\kasini3000\" 程序退出'
			exit 2
		}
		& 'c:\ProgramData\kasini3000\0k_source.ps1'
		& "${global:kasini3000目录}\admin_gui\pic\m默认ps背景图片.ps1"
	}

	if ($IsLinux -eq $True)
	{
		if (Test-Path -LiteralPath '/etc/kasini3000/')
		{
			Set-Location '/etc/kasini3000/'
		}
		else
		{
			Write-Error '找不到 卡死你3000程序主目录 "/etc/kasini3000/" 程序退出'
			exit 2
		}
		& '/etc/kasini3000/0k_source.ps1'
	}
	#main
	Write-Host '欢迎来到 KaSiNi3000 Console!' -ForegroundColor Yellow
	& 'kasini_version.ps1'

	& 'zd只读nodelist文件.ps1'
	& 'sxa刷新所有被控机对象的_os类型属性.ps1'
	& 'cdall.ps1'

	if ($PSVersionTable.psversion.major -lt 6)
	{
		Write-Error '管理linux被控机，需要安装并运行powershell版本6及以上,只支持powershell稳定版'
		Write-Error '若只有win被控机，没有linux被控机，不用理会此错误'
	}

	if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
	{
		[string]$temp001 = ssh.exe -V *>&1
		if (-not ($temp001.ToLower().Contains('ssh'.ToLower())) )
		{
			Write-Error '管理linux被控机，需要ssh.exe。请运行【zkj_install_k记饭店_win主控机.ps1】'
			Write-Error '若只有win被控机，没有linux被控机，不用理会此错误'
		}
	}
}
else
{
	Write-Error '互斥失败 !只能有一个【卡死你3000 终端】。只能有一个【卡死你3000 kaiiit】。【先】关闭【卡死你3000 终端】的pwsh.exe进程，并等待1分钟，再重新运行。'
	exit 1
}
exit 0

