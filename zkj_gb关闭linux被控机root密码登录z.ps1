﻿#建议保存编码为：bom头 + utf8

param
(
	[String]$目的ip地址 = $global:当前被控机_ip.ip,
	$端口 = $global:当前被控机_ip.端口
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}


$win = 'win7','win8','win10','win2008r2','win2012r2','win2016','win2019'
$linux = 'centos7','centos8','debian8','debian9','debian10','ubuntu1404','ubuntu1604','ubuntu1804','alpine','ubuntu2004'
$当前被控机 = $global:所有被控机 | Where-Object { $_.ip -eq $目的ip地址 }
if ($当前被控机.ip -ne $目的ip地址)
{
	Write-Error "nodelist文件中找不到这个ip地址： ${目的ip地址}"
	exit 13
}
else
{
	[System.Net.IPAddress]$ip2 = 0
	if (   [System.Net.IPAddress]::TryParse($当前被控机.ip,[ref]$ip2)      )
	{
		[scriptblock]$开启linux被控机密码登录代码块 =
		{
			$private:temp999 = & 'kdir-node-script.ps1' -被查找的脚本文件名 '*ps找算替*'
			if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
			{
				Write-Error "找不到 '*ps找算替*' "
				exit 11
			}
			else
			{
				$ps找算替脚本路径 = $private:temp999
			}

			$test001 =	@{
				输入文件全路径         = '/etc/ssh/sshd_config'
				输出文件全路径         = '直接写输入文件，不需要备份'
				输入文件编码          = 65001

				找什么             =
				@'
PermitRootLogin
'@


				替换成啥            =
				@'
PermitRootLogin prohibit-password
'@

				不用替换成啥_而用ps代码替换 = $false
				查找模式            = '行'
				找不到则在文件末尾追加     = $true
				查找返回的结果必须唯一     = $false
				返回结果不唯一时        = '全部替换'
				脚本pid           = $pid # 这行用于脚本防错，不要修改。
			}

			&  "$ps找算替脚本路径"  @test001
			systemctl restart sshd.service
		}

		if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
		{
			if ( $当前被控机.被控机os类型 -in $win)
			{
				Write-Error "此脚本不支持【win被控机】"
				exit 2
			}

			if ( $当前被控机.被控机os类型 -in $linux)
			{
				& 'run_win2linux_key_pwd.ps1'  -目的ip地址 $ip2 -powershell代码块 $开启linux被控机密码登录代码块  -端口 $端口
			}
		}

		if ($IsLinux -eq $True)
		{
			if ( $当前被控机.被控机os类型 -in $win)
			{
				Write-Error "无法从【linux主控机】控制【win被控机】"
				exit 1
			}

			if ( $当前被控机.被控机os类型 -in $linux)
			{
				& 'run_linux2linux_key_pwd.ps1'  -目的ip地址 $ip2 -powershell代码块 $开启linux被控机密码登录代码块 -端口 $端口
			}
		}
	}
	else
	{
		Write-Error "nodelist文件中找到这个ip地址： ${被控机ip地址}，但ip不合法"
		exit 12
	}
}

exit 0
