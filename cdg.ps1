﻿#建议保存编码为：bom头 + utf8

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$被控机分组名
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$global:当前被控机_ip = $null
$global:当前被控机_组 = $null
$global:当前被控机_uuid = $null
$global:当前被控机_os = $null
$global:当前被控机_esxi宿主机 = $null

& 'zd只读nodelist文件.ps1'

$global:当前被控机_组 = $global:所有被控机 | Where-Object { $_.被控机分组名 -eq $被控机分组名 }
if ($global:当前被控机_组 -eq $null)
{
	Write-Error "错误：nodelist文件中找不到这个分组名的被控机： ${被控机分组名}"

	function global:prompt
	{
		"`e[91m`e[44m分组名【{0}】`e[0m{1}> " -f '错误：nodelist文件中找不到这个分组名的被控机',$PWD
	}
	exit 11
}

function global:prompt
{
	"`e[37m`e[44m分组名【{0}】`e[0m{1}>" -f $global:当前被控机_组[0].被控机分组名,$PWD
}
Write-Host -ForegroundColor green '提示：输入  ${global:当前被控机_组}  即可显示所有组内被控机'
exit 0
