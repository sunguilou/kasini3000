﻿#建议保存编码为：bom头 + utf8

param
(
	[String]$脚本文件名,
	[scriptblock]$powershell代码块 = {},
	$端口 = 22,
	$传入参数,
	[bool]$复制主控机node_script目录到被控机 = $false
)


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

Write-Verbose '开始在 被控机os 执行'

if ($global:当前被控机_os -eq $null)
{
	Write-Error "${global:当前被控机_os} 为空"
	exit 1
}

foreach ($private:当前被控机4 in $global:当前被控机_os)
{
	$global:当前被控机_ip = $private:当前被控机4
	& 'k_run_ip.ps1' -powershell代码块 $powershell代码块  -脚本文件名 $脚本文件名  -传入参数 $传入参数  -端口 $端口 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
}

Write-Verbose '结束在 被控机os 执行'

exit 0
