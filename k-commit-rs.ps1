﻿#建议保存编码为：bom头 + utf8

param
(
	[Alias("file")][String]$脚本文件名,

	[Alias("scriptblock")][scriptblock]$powershell代码块 = { },

	[Alias("port")][uint16]$端口,

	[Alias("allparameter")][string]$全部传入参数,

	[Alias("timeout")][timespan]$任务超时时间 = [timespan]'00:10:00',

	[Alias("remark")][String]$备注 = (Get-Date -format 'yyyyMMdd HH:mm:ss'),

	$复制主控机node_script目录到被控机 = $false
)

function yg严格测试布尔型变量($被测试的布尔形变量)
{
	if (($被测试的布尔形变量 -eq 1) -or ($被测试的布尔形变量 -eq $true) -or ($被测试的布尔形变量 -eq 'true'))
	{
		return $true
	}
	elseif (($被测试的布尔形变量 -eq 0) -or ($被测试的布尔形变量 -eq $false) -or ($被测试的布尔形变量 -eq 'false'))
	{
		return $false
	}
	else
	{
		Write-Error '错误：不合法的布尔型值，错误码999'
		exit 999
	}
}

[bool]$复制主控机node_script目录到被控机 = yg严格测试布尔型变量 $复制主控机node_script目录到被控机



if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}


if ( ($脚本文件名 -eq $null) -or ($脚本文件名 -eq '') )
{
	#这段代码在run_win2linux_key_pwd.ps1,run_win2win5985_pwd.ps1,run_linux2linux_key_pwd.ps1,k-commit.ps1,k_commit_ip.ps1,k_commit_uuid.ps1,k库_计划表_添加动作231.ps1
	$有脚本文件 = $false
	if ( ($powershell代码块.Ast.Extent.Text -eq '{ }') -or ($powershell代码块.Ast.Extent.Text -eq '{}') )
	{
		Write-Error "错误：没有输入脚本文件名，同时也没有输入代码块"
		exit 15
	}
	else
	{
		if (& 'kcd_被控机运行的代码块_含有kcd.ps1' -输入脚本代码块 $powershell代码块)
		{
			Write-Error '错误：脚本内含有kcd等关键字'
			exit 19
		}
	}
}
else
{
	$有脚本文件 = $true
	if ( ($powershell代码块.Ast.Extent.Text -eq '{ }') -or ($powershell代码块.Ast.Extent.Text -eq '{}') )
	{

	}
	else
	{
		Write-Error "错误：有输入脚本文件名，同时有输入代码块"
		exit 16
	}

	if (Test-Path -LiteralPath $脚本文件名)
	{
		if (& 'kcd_被控机运行的脚本_含有kcd.ps1' -输入脚本文件名 $脚本文件名)
		{
			Write-Error '错误！脚本内含有kcd等关键字'
			exit 18
		}
	}
	else
	{
		Write-Error "找不到脚本文件"
		exit 17
	}
}

#main
$msg =
@'
欢迎来到【卡死你3000 rs commit】，这是【多线程】的命令运行器。别名=【沙漠飞蝗提交】
它采用生产者/消费者模式。生产者：延时提交任务到数据库。消费者：从数据库读取脚本，异步执行。
它只能提交脚本。无屏幕输出。有日志，运行状态，结果，保存在数据库。
它的使用场合是：采用【后台多线程】技术，运行【命令块】或【脚本】，用于调试。
'@

Write-Host $msg -ForegroundColor Green

if ($global:kcommitrs_的ps版本)
{
	if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
	{
		Start-Process -FilePath  "$global:kcommitrs_的ps版本"  -ArgumentList " -WindowStyle hidden -file ${global:kasini3000目录}/kaiiit_后台任务_rs.ps1"
	}

	if ($IsLinux -eq $True)
	{
		Start-Process -FilePath  "$global:kcommitrs_的ps版本"  -ArgumentList " -file ${global:kasini3000目录}/kaiiit_后台任务_rs.ps1"
	}
}
else
{
	Write-Error "错误：k-commit-rs.ps1中，找不到变量【$global:kcommitrs_的ps版本】，错误码1"
	exit 1
}

if ($global:当前被控机_ip -ne $null)
{
	& 'k_commit_ip_rs.ps1' -powershell代码块 $powershell代码块  -脚本文件名 $脚本文件名  -传入参数 $全部传入参数 -备注 $备注 -端口 $端口 -任务超时时间 $任务超时时间 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
}
else
{
	if ($global:当前被控机_uuid -ne $null)
	{
		& 'k_commit_uuid_rs.ps1' -powershell代码块 $powershell代码块  -脚本文件名 $脚本文件名  -传入参数 $全部传入参数 -备注 $备注 -端口 $端口 -任务超时时间 $任务超时时间 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
	}
	else
	{
		if ($global:当前被控机_组 -ne $null)
		{
			& 'k_commit_g_rs.ps1' -powershell代码块 $powershell代码块  -脚本文件名 $脚本文件名  -传入参数 $全部传入参数 -备注 $备注 -端口 $端口 -任务超时时间 $任务超时时间 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
		}
		else
		{
			if ($global:当前被控机_os -ne $null)
			{
				& 'k_commit_os_rs.ps1' -powershell代码块 $powershell代码块  -脚本文件名 $脚本文件名  -传入参数 $全部传入参数 -备注 $备注 -端口 $端口  -任务超时时间 $任务超时时间 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
			}
			else
			{
				& 'k_commit_all_rs.ps1' -powershell代码块 $powershell代码块  -脚本文件名 $脚本文件名  -传入参数 $全部传入参数 -备注 $备注 -端口 $端口 -任务超时时间 $任务超时时间 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
			}
		}
	}
}

Write-Host '卡死你3000 rs commit 任务提交完毕' -ForegroundColor Green

Write-Warning 'kl-rs.ps1	从福报rs表，获取最后n个任务结果输出，以list列表形式显示。n默认为5'
Write-Warning 'kt-rs.ps1	从福报rs表，获取最后n个任务结果输出，以table表格形式显示。n默认为10'
Write-Warning 'klg-rs.ps1	用【图形版list形式】显示【福报表】结果。只能在win版powershell中使用。'
Write-Warning 'ktg-rs.ps1	用【图形版table形式】显示【福报表】结果。只能在win版powershell中使用。'

exit 0
