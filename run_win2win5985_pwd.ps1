﻿#建议保存编码为：bom头 + utf8
# 支持 5986
param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[Alias("ipaddress")][String]$目的ip地址,

	[Alias("port")][uint16]$端口 = 5985,

	[Alias("file")][String]$脚本文件名,

	[parameter(Position = 0)][Alias("scriptblock")][scriptblock]$powershell代码块 = { },

	[Alias("allparameter")]$传入参数,

	$复制主控机node_script目录到被控机 = $false
)

function yg严格测试布尔型变量($被测试的布尔形变量)
{
	if (($被测试的布尔形变量 -eq 1) -or ($被测试的布尔形变量 -eq $true) -or ($被测试的布尔形变量 -eq 'true'))
	{
		return $true
	}
	elseif (($被测试的布尔形变量 -eq 0) -or ($被测试的布尔形变量 -eq $false) -or ($被测试的布尔形变量 -eq 'false'))
	{
		return $false
	}
	else
	{
		Write-Error '错误：不合法的布尔型值，错误码999'
		exit 999
	}
}

[bool]$复制主控机node_script目录到被控机 = yg严格测试布尔型变量 $复制主控机node_script目录到被控机

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	Write-Error "不支持从linux【主控机】，到win【被控机】"
	exit 11
}

Write-Verbose "开始在win2win被控机【${目的ip地址}】上执行"

if ( ($脚本文件名 -eq $null) -or ($脚本文件名 -eq '') )
{
	#这段代码在run_win2linux_key_pwd.ps1,run_win2win5985_pwd.ps1,run_linux2linux_key_pwd.ps1,k-commit.ps1
	$有脚本文件 = $false
	if ( ($powershell代码块.Ast.Extent.Text -eq '{ }') -or ($powershell代码块.Ast.Extent.Text -eq '{}') )
	{
		Write-Error "错误：没有输入脚本文件名，同时也没有输入代码块"
		exit 15
	}
	else
	{
		if (& 'kcd_被控机运行的代码块_含有kcd.ps1' -输入脚本代码块 $powershell代码块)
		{
			Write-Error '错误：脚本内含有kcd等关键字'
			exit 19
		}
	}
}
else
{
	$有脚本文件 = $true
	if ( ($powershell代码块.Ast.Extent.Text -eq '{ }') -or ($powershell代码块.Ast.Extent.Text -eq '{}') )
	{

	}
	else
	{
		Write-Error "错误：有输入脚本文件名，同时有输入代码块"
		exit 16
	}

	if (Test-Path -LiteralPath $脚本文件名)
	{
		if (& 'kcd_被控机运行的脚本_含有kcd.ps1' -输入脚本文件名 $脚本文件名)
		{
			Write-Error '错误！脚本内含有kcd等关键字'
			exit 18
		}
	}
	else
	{
		Write-Error "找不到脚本文件"
		exit 17
	}
}


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	try
	{
		Test-WSMan -ComputerName $目的ip地址 -Port $端口 -ErrorAction stop
	}
	catch
	{
		Write-Error "错误：目的ip地址【${目的ip地址}】端口不通"
		exit 12
	}
	finally
	{

	}

	& 'zd只读nodelist文件.ps1'
	$当前被控机 = $global:所有被控机 | Where-Object { $_.ip -eq $目的ip地址 }
	if ($当前被控机.ip -ne $目的ip地址)
	{
		Write-Error "错误：当前被控机获取失败 ${当前被控机}"
		exit 13
	}

	if ($复制主控机node_script目录到被控机 -eq $true)
	{
		& 'kctf复制主控机node_script目录脚本到当前被控机.ps1'
	}

	$PSRemoting服务器用户名 = $当前被控机.用户名
	$用户名2 = "${目的ip地址}\${PSRemoting服务器用户名}"
	$密码明文 = $当前被控机.当前密码
	$密码密文 = ConvertTo-SecureString $密码明文 -AsPlainText -Force
	$用户名和密码捆绑后的授权信息 = New-Object System.Management.Automation.PSCredential ($用户名2,$密码密文)

	Write-Verbose '使用密码，连接win2win5985开始'
	$private:连接3 = New-PSSession -ComputerName $目的ip地址 -Port $端口 -Credential $用户名和密码捆绑后的授权信息
	if ($private:连接3 -eq $null)
	{
		Write-Error "使用密码，在${目的ip地址}上连接win2win5985失败"
		exit 21
	}

	Write-Verbose '连接成功。现在开始执行命令：'
	[scriptblock]$private:cmd = `
	{
		if (Test-Path -LiteralPath 'c:\ProgramData\kasini3000\')
		{

		}
		else
		{
			mkdir 'c:\ProgramData\kasini3000\'
		}
		return $PSVersionTable
	}

	$private:返回 = Invoke-Command -Session $private:连接3 -ScriptBlock $private:cmd
	Write-Verbose ($private:返回 | Out-String )
	if ( ($private:返回.psversion.major -lt 6) -and ($private:返回.psversion.Minor -lt 1) ) #ps版本低于5.1
	{
		Write-Warning 'win被控机中的powershell版本太低'
		& 'zkj_install_k记饭店_win被控机.ps1' -目的ip地址 ${目的ip地址}
	}

	Invoke-Command -Session $private:连接3 -ScriptBlock { & 'c:\ProgramData\kasini3000\node_script\kasini3000\tj在被控机添加path.ps1' }

	if ( $有脚本文件 -eq $true)
	{
		Invoke-Command -Session $private:连接3 -FilePath $脚本文件名 -ArgumentList $传入参数
	}
	else
	{
		Invoke-Command -Session $private:连接3 -ScriptBlock $powershell代码块 -ArgumentList $传入参数
	}
	Write-Verbose '执行命令完成，即将断开连接。'
	Remove-PSSession -Session $private:连接3
}
Write-Verbose "完成在win2win被控机【${目的ip地址}】上执行"
exit 0
