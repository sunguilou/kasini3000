﻿#建议保存编码为：bom头 + utf8

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$目的ip地址,
	$端口 = 22,
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[string]$ssh密码
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	Write-Error "错误：不支持win"
	exit 1
}

if ($IsLinux -eq $True)
{
	$被推送的公钥文件 = "${global:kasini3000目录}/ssh_key_files_old1/authorized_keys"
	if (Test-Path -LiteralPath $被推送的公钥文件)
	{

	}
	else
	{
		Write-Error "错误：找不到【被推送的公钥文件】"
		exit 2
	}

	[string]$目的路径 = '/root/.ssh'
	& 'k-sshfs-copy-a-file.ps1' -源文件全路径 $被推送的公钥文件 -目标绝对路径_必须是文件夹 $目的路径 -目的ip地址 $目的ip地址 -ssh密码 $ssh密码
}
exit 0
