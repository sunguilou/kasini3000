﻿#建议保存编码为：bom头 + utf8
param
(
	[ValidateNotNullOrEmpty()][string]$源文件全路径,
	[ValidateNotNullOrEmpty()][string]$目标绝对路径_必须是文件夹,
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$目的ip地址,
	$端口 = 22 ,
	[string]$ssh密码,
	[string]$主控机ssh私钥路径
	#用ssh私钥mount
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

if (Test-Path -LiteralPath $源文件全路径)
{

}
else
{
	Write-Error "错误，找不到源文件 $源文件全路径"
	exit 1
}

if (Test-Connection -TargetName $目的ip地址 -TCPPort $端口 -Quiet)
{

}
else
{
	Write-Error "错误，目标ip【${目的ip地址}】的【${端口}】端口不通"
	exit 2
}

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	Write-Host '未实现'
	exit 3
}

$username = /usr/bin/whoami
if ($username -ne 'root')
{
	Write-Error '错误：非管理员，退出码4'
	Exit 4
}

if ($IsLinux -eq $True)
{
	$etcmnt = '/etc/kasini3000/mnt'
	if ($主控机ssh私钥路径 -ne '')
	{
		#用秘钥mount  sshfs
		sshfs -o allow_other,nonempty,IdentityFile=${ssh私钥路径} root@${目的ip地址}:/  $etcmnt
	}
	else
	{
		#用密码mount  sshfs
		$ssh密码 | sshfs -o allow_other,nonempty -o password_stdin root@${目的ip地址}:/  $etcmnt
	}
	Start-Sleep -Seconds 1

	[string]$temp01 = df -hT | Select-String "${目的ip地址}"
	if ( $temp01.ToLower().Contains($目的ip地址.ToLower()) )
	{
		$sshfs映射后的目标绝对路径 = $etcmnt + $目标绝对路径_必须是文件夹
		Write-Verbose $sshfs映射后的目标绝对路径
		if (Test-Path -LiteralPath $sshfs映射后的目标绝对路径)
		{

		}
		else
		{
			Write-Warning "错误，目标ip上的路径不存在，现在将建立它 $目标绝对路径_必须是文件夹"
			mkdir $sshfs映射后的目标绝对路径
		}
		Copy-Item -LiteralPath $源文件全路径 -Destination $sshfs映射后的目标绝对路径
		if (Test-Path -LiteralPath '/etc/kasini3000/mnt/root/.ssh/authorized_keys')
		{
			chmod 600 '/etc/kasini3000/mnt/root/.ssh/authorized_keys'
		}
		Start-Sleep -Seconds 1
		umount /etc/kasini3000/mnt
	}
	else
	{
		Write-Error "错误：sshfs映射失败！退出码5"
		exit 5
	}

}

exit 0
