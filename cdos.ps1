﻿#建议保存编码为：bom头 + utf8

param
(
	[parameter(Mandatory = $true)]
	[ValidateSet('win7','win8','win10','win2008r2','win2012r2','win2016','win2019','centos6','centos7','centos8','debian8','debian9','debian10','ubuntu1404','ubuntu1604','ubuntu1804','alpine','ubuntu2004')]
	[String]$被控机os名
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$global:当前被控机_ip = $null
$global:当前被控机_组 = $null
$global:当前被控机_uuid = $null
$global:当前被控机_os = $null
$global:当前被控机_esxi宿主机 = $null

& 'zd只读nodelist文件.ps1'

$global:当前被控机_os = $global:所有被控机 | Where-Object { $_.被控机os类型 -eq $被控机os名 }
if ($global:当前被控机_os -eq $null)
{
	Write-Error "错误：nodelist文件中找不到这种os的被控机： ${被控机os名} `n请运行【.\sx刷新单个被控机对象的_os类型属性.ps1】 -被控机 '被控机ip' "

	function global:prompt
	{
		"`e[91m`e[44m【{0}】`e[0m{1}> " -f '错误：nodelist文件中找不到这种os的被控机',$PWD
	}
	exit 11
}

function global:prompt
{
	"`e[37m`e[44m所有OS为【{0}】的被控机`e[0m{1}>" -f $global:当前被控机_os[0].被控机os类型,$PWD
}
return $global:当前被控机_os
exit 0
