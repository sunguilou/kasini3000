﻿#建议保存编码为：bom头 + utf8
#
Start-Sleep -Seconds 1
if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$private:kaiiit_rs_的磁盘pid = & 'kaiiit_读pid_rs.ps1'
if ( ($private:kaiiit_rs_的磁盘pid -eq $null) -or ($private:kaiiit_rs_的磁盘pid -eq '') )
{

}
else
{
	$private:有kaiiit_rs_进程 = Get-Process -id $private:kaiiit_rs_的磁盘pid
}

if ($private:有kaiiit_rs_进程 -eq $null)
{

}
else
{
	if ( ($private:有kaiiit_rs_进程.ProcessName -eq 'pwsh') -or ($private:有kaiiit_rs_进程.ProcessName -eq 'powershell') )
	{
		Write-Error "错误：已经有 k-commit-rs.ps1 后台任务进程正在运行中！退出码2`n请等待一会后，运行kl.ps1获取运行结果。"
		exit 2
	}
}

#main
& 'k-console.ps1' -互斥名字 'Global\卡死你3000kaiiit_rs'

if ($LASTEXITCODE -eq 1)
{
	Write-Error "错误：已经有 k-commit-rs.ps1 后台任务进程正在运行中！退出码1`n请等待一会后，运行kl.ps1获取运行结果。"
	exit 1
}

#建立pid文件
& 'kaiiit_写pid_rs.ps1'

& 'k_写log.ps1' -被写入的log内容 '【kaiiit-rs】开始运行'

Write-Error '信息：k-commit-rs.ps1 后台任务现在开始运行！'

$private:超过此时间则退出 = (Get-Date).AddSeconds(60)
$global:kaiiit_rs_循环中 = $True
while ($global:kaiiit_rs_循环中 -eq $True)
{
	& 'end_invoke_福报表_rs.ps1'

	$private:当前福报数 = (Get-PSSession).count
	if ($private:当前福报数 -gt $global:kcommitrs并发线程数)
	{
		& 'k_写log.ps1' -被写入的log内容 "错误：现有rs福报数太多。 ${private:当前福报数} "
	}
	else
	{
		& 'begin_invoke_福报表_rs.ps1'
	}

	if ($global:kaiiit后台任务_循环间隔_秒)
	{
		Start-Sleep -Seconds $global:kaiiit后台任务_循环间隔_秒
	}
	else
	{
		Start-Sleep -Seconds 10
	}

	if ($private:超过此时间则退出 -lt (Get-Date)) #每分钟判断一次是否有kaiiit-rs任务运行
	{
		if ((Get-RSJob).count -le 0)
		{
			& 'k_写log.ps1' -被写入的log内容 '【kaiiit-rs】结束运行'
			$global:kaiiit_rs_循环中 = $false
			Write-Error '信息：所有 k-commit-rs.ps1 后台任务运行完毕！'
		}
		else
		{
			$private:temp995 = "现有福报rs任务 {0} 个" -f (Get-RSJob).count
			& 'k_写log.ps1' -被写入的log内容 $private:temp995
			$private:超过此时间则退出 = (Get-Date).AddSeconds(60)
		}
	}

	if ( ($private:现在.Minute % 9 -eq 0) -and ($private:现在.Second -gt 51) )
	{
		Write-Verbose '开始运行 .net 垃圾回收'
		[System.GC]::Collect()
		[System.GC]::WaitForPendingFinalizers()
	}
}

exit 0
