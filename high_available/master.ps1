﻿#建议保存编码为：bom头 + utf8
#Requires -RunAsAdministrator
# 本脚本由任务计划，每分钟拉起运行一次。脚本内，10秒钟运行一次，共运行5次。
Start-Sleep -Seconds 1

${script:嬴扶苏_ip} = '2.2.2.2' #standby_ip
${script:启用日志} = $true

#region 函数
function 检测皇帝心跳
{
	return $(Test-Path -LiteralPath "${global:kasini3000目录}/high_available/太子篡位邪念.txt")
}

function 删除皇帝心跳
{
	Remove-Item -Force -LiteralPath "${global:kasini3000目录}/high_available/太子篡位邪念.txt"
}

function 检测太子心跳
{
	& 'cdip.ps1' -被控机ip地址 ${script:嬴扶苏_ip}
	$private:嬴扶苏心跳 = & 'k_run_ip.ps1' -powershell代码块 {
		if (Test-Path -LiteralPath '/etc/kasini3000')
		{
			Test-Path -LiteralPath  '/etc/kasini3000/high_available/太子篡位邪念.txt'
		}

		if (Test-Path -LiteralPath 'c:\ProgramData\kasini3000\')
		{
			Test-Path -LiteralPath   'c:\ProgramData\kasini3000\high_available\太子篡位邪念.txt'
		}
	}

	if ($private:嬴扶苏心跳 -is [array])
	{
		return $private:嬴扶苏心跳[-1]
	}
	else
	{
		return $private:嬴扶苏心跳
	}
}

function 删除太子心跳
{
	& 'cdip.ps1' -被控机ip地址 ${script:嬴扶苏_ip}
	& 'k_run_ip.ps1' -powershell代码块 {
		if (Test-Path -LiteralPath '/etc/kasini3000')
		{
			Remove-Item -Force -LiteralPath '/etc/kasini3000/high_available/太子篡位邪念.txt'
		}

		if (Test-Path -LiteralPath 'c:\ProgramData\kasini3000\')
		{
			Remove-Item -Force -LiteralPath 'c:\ProgramData\kasini3000\high_available\太子篡位邪念.txt'
		}
	}
}

function 检测皇帝在位否
{
	if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
	{
		return $(& 'u库定时任务_检查任务计划存在否_win.ps1')
	}

	if ($IsLinux -eq $True)
	{
		return $(& 'u库定时任务_检查任务计划存在否_linux.ps1')
	}
}

function 皇帝登基_抢地主
{
	if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
	{
		& 'u库定时任务_建立任务计划_win.ps1'
	}

	if ($IsLinux -eq $True)
	{
		& 'u库定时任务_建立任务计划_linux.ps1'
	}
}


function 推送master数据库
{
	#Add-Content -LiteralPath "${global:kasini3000目录}/cmdb/d当前库/user_crontab.sqlite3" -Value ('' + (Get-Date -format F) + '主机')
	& 'cdip.ps1' -被控机ip地址 ${script:嬴扶苏_ip}
	$private:linux版本字串 = 'centos8','centos7','centos6','debian8','debian9','debian10','ubuntu1404','ubuntu1604','ubuntu1804','centos8','alpine','ubuntu2004'
	$private:win版本字串 = 'win2008r2','win7','win8','win10','win2012r2','win2016','win2019'
	if ($global:当前被控机_ip.被控机os类型 -in $private:linux版本字串)
	{
		& 'k_copyto_ip.ps1' -LiteralPath "${global:kasini3000目录}/cmdb/d当前库/user_crontab.sqlite3" -Destination  '/etc/kasini3000/cmdb/d当前库/'
	}

	if ($global:当前被控机_ip.被控机os类型 -in $private:win版本字串)
	{
		& 'k_copyto_ip.ps1' -LiteralPath "${global:kasini3000目录}/cmdb/d当前库/user_crontab.sqlite3" -Destination  'c:\ProgramData\kasini3000\cmdb\d当前库'
	}
}

function 写日志($被写入的log内容)
{
	if (${script:启用日志} -eq $True)
	{
		$private:日志全路径 = "${global:kasini3000目录}/high_available/卡死你3000高可用日志主机.txt"
		$private:temp = Get-Item -LiteralPath $private:日志全路径
		if ($private:temp.Length -gt 128mb)
		{
			Remove-Item -Force -LiteralPath $private:日志全路径
		}
		$时间 = Get-Date -Format F
		$输出信息 = "$时间	$被写入的log内容"
		Add-Content -LiteralPath $private:日志全路径 -Value $输出信息 -Encoding UTF8
	}
}

function 写日志2($被写入的log内容)
{
	$private:日志全路径 = "${global:kasini3000目录}/high_available/卡死你3000高可用日志主机.txt"
	$private:temp = Get-Item -LiteralPath $private:日志全路径
	if ($private:temp.Length -gt 128mb)
	{
		Remove-Item -Force -LiteralPath $private:日志全路径
	}
	$时间 = Get-Date -Format F
	$输出信息 = "$时间	$被写入的log内容"
	Add-Content -LiteralPath $private:日志全路径 -Value $输出信息 -Encoding UTF8
}

#endregion 函数

#-------------------main--------------------
if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$private:win = 'win7','win8','win10','win2008r2','win2012r2','win2016','win2019'
$private:linux = 'centos7','centos8','debian8','debian9','debian10','ubuntu1404','ubuntu1604','ubuntu1804','alpine'
$private:all = $win + $linux

& 'cdip.ps1' ${script:嬴扶苏_ip}
if (($LASTEXITCODE -ne 0) -or ($global:当前被控机_ip.被控机os类型 -notin $private:all))
{
	写日志 '错误：找不到被控机【嬴扶苏】，退出码11'
	exit 11
}

$private:master磁盘文件 = "${global:kasini3000目录}/high_available/master_pid.txt"
if (Test-Path -LiteralPath $private:master磁盘文件)
{
	$private:旧master_pid = Get-Content -LiteralPath $private:master磁盘文件
}

if ( ($private:旧master_pid -eq $null) -or ($private:旧master_pid -eq '') )
{

}
else
{
	$private:旧master进程 = Get-Process -id $private:旧master_pid
}

if ($private:旧master进程 -eq $null)
{

}
else
{
	if ( ($private:旧master进程.ProcessName -eq 'pwsh') -or ($private:旧master进程.ProcessName -eq 'powershell') )
	{
		写日志 "错误：旧的master进程，还在运行中！【$private:旧master_pid】它将被替换成新的。"
		Stop-Process -Force -id $private:旧master_pid
	}
}
Set-Content -LiteralPath $private:master磁盘文件 -Value $pid -Encoding UTF8 -Force
写日志 "新pid： $pid"


while ((Get-Date).Second -lt 48)
{
	switch (${script:嬴扶苏状态})
	{
		'太子将失位'
		{
			写日志 '太子将失位开始'
			for ($i = 1;$i -lt 5;$i++) #等待皇帝心跳n秒钟。
			{
				if (检测皇帝心跳)
				{
					写日志 '太子将失位2，有皇帝心跳'
					删除皇帝心跳
					break
				}
				Start-Sleep -Seconds 1
			}

			if (检测皇帝在位否)
			{

			}
			else
			{
				写日志2 '太子将失位2，太子失位之前'
				皇帝登基_抢地主
				写日志2 '太子将失位3，太子失位之后'
			}
			${script:嬴扶苏状态} = '太子已蹲东宫'

			break
		}

		'太子疑似造反'
		{
			写日志 '太子疑似造反开始'
			if (检测皇帝心跳)
			{
				写日志 '太子疑似造反2，有皇帝心跳'
				${script:嬴扶苏状态} = '太子将失位'
			}
			else
			{
				写日志 '太子疑似造反2，无皇帝心跳'
				${script:嬴扶苏状态} = '太子已蹲东宫'
			}
			break
		}

		'太子已蹲东宫'
		{
			写日志 '太子已蹲东宫开始'

			if (检测太子心跳)
			{
				写日志 '太子已蹲东宫2，删除太子心跳'
				删除太子心跳

				写日志 '太子已蹲东宫3，推送master数据库'
				推送master数据库

				Start-Sleep -Seconds 10
			}
			else
			{
				${script:嬴扶苏状态} = '太子已薨'
			}
			break
		}

		'太子已薨'
		{
			写日志 '太子已薨'
			${script:嬴扶苏状态} = '太子已蹲东宫'
			Start-Sleep -Seconds 10
			break
		}

		default
		{
			写日志 '卡死你3000主控机高可用，主机，脚本开始'
			if (检测太子心跳)
			{
				删除太子心跳
				${script:嬴扶苏状态} = '太子疑似造反'
			}
			else
			{
				${script:嬴扶苏状态} = '太子已薨'
			}
		}
	}
	Start-Sleep -Seconds 1
}

写日志 '卡死你3000主控机高可用，主机，脚本结束'
exit 0
