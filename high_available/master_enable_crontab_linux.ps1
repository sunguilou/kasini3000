﻿#建议保存编码为：bom头 + utf8
#Requires -RunAsAdministrator
#此脚本只能在主控机上运行。

& '/etc/kasini3000/0k_source.ps1'

if ($IsLinux -eq $True)
{
	$username = /usr/bin/whoami
	if ($username -ne 'root')
	{
		Write-Error '非管理员'
		Exit 1
	}

	if ([System.Environment]::Is64BitOperatingSystem -eq $True)
	{

	}
	else
	{
		Write-Error '不支持32位操作系统！'
		Exit 2
	}

	$判断centos6 = @'
rpm -q centos-release
'@ | /usr/bin/bash

	if ( $判断centos6.contains('el6'.tolower()) )
	{
		Write-Error '不支持centos6'
		Exit 3
	}


	if (Test-Path '/etc/kasini3000')
	{

	}
	else
	{
		Write-Error '找不到 /etc/kasini3000'
		Exit 4
	}

	#安装sqlite
	Copy-Item -LiteralPath '/etc/kasini3000/cmdb/linux.SQLite.Interop.dll' -Destination '/etc/kasini3000/cmdb/SQLite.Interop.dll' -Force

	#安装 k_crontab
	Copy-Item -LiteralPath '/var/spool/cron/root' -Destination '/var/spool/' -Force
	$private:新名字 = 'root-' + (Get-Date -format 'yyyy-MM-dd-HH:mm:ss')
	Rename-Item -LiteralPath '/var/spool/root' -NewName $private:新名字
	$所有任务计划 = /usr/bin/crontab -l

	if ($所有任务计划 -match 'master')
	{
		Write-Error '卡死你3000主控机高可用，主机，已经安装！'
	}
	else
	{
		$卡死你_crontab_cmd = '* * * * * /usr/bin/pwsh -file /etc/kasini3000/high_available/master.ps1 > /dev/null 2>&1'
		Add-Content -LiteralPath '/var/spool/cron/root' -Value $卡死你_crontab_cmd -Encoding UTF8nobom
	}

	#在/usr/bin创建快捷方式。
	chmod u+x /etc/kasini3000/kc.sh
	ln -s /etc/kasini3000/kc.sh /usr/bin/kc
	chmod u+x /etc/kasini3000/kcp.sh
	ln -s /etc/kasini3000/kcp.sh /usr/bin/kcp

}

exit 0
