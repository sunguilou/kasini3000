﻿#建议保存编码为：bom头 + utf8
#Requires -RunAsAdministrator

$任务计划模块 = (Get-Command -module ScheduledTasks).name
if ($任务计划模块.Length -lt 3)
{
	Write-Error '错误：此版本win，缺少任务计划模块！'
	exit 3
}
else
{
	if (Test-Path -LiteralPath 'c:\Program Files\PowerShell\7\pwsh.exe')
	{
		$操作 = New-ScheduledTaskAction -Execute 'C:\Program Files\PowerShell\7\pwsh.exe' -Argument " -WindowStyle hidden -File c:\ProgramData\kasini3000\high_available\standby.ps1" -WorkingDirectory "c:\ProgramData\kasini3000"
	}
	elseif (Test-Path -LiteralPath 'c:\Program Files\PowerShell\7-preview\pwsh.exe')
	{
		$操作 = New-ScheduledTaskAction -Execute 'c:\Program Files\PowerShell\7-preview\pwsh.exe' -Argument " -WindowStyle hidden  -File c:\ProgramData\kasini3000\high_available\standby.ps1" -WorkingDirectory "c:\ProgramData\kasini3000"
	}
	elseif (Test-Path -LiteralPath 'c:\Program Files\PowerShell\6\pwsh.exe')
	{
		$操作 = New-ScheduledTaskAction -Execute 'C:\Program Files\PowerShell\6\pwsh.exe' -Argument " -WindowStyle hidden -File c:\ProgramData\kasini3000\high_available\standby.ps1" -WorkingDirectory "c:\ProgramData\kasini3000"
	}
	else
	{
		$操作 = New-ScheduledTaskAction -Execute 'C:\windows\System32\WindowsPowerShell\v1.0\powershell.exe' -Argument " -WindowStyle hidden -File c:\ProgramData\kasini3000\high_available\standby.ps1" -WorkingDirectory "c:\ProgramData\kasini3000"
	}

	$触发器 = New-ScheduledTaskTrigger -daily -At 00:00:01
	$设置 = New-ScheduledTaskSettingsSet -DontStopIfGoingOnBatteries -AllowStartIfOnBatteries -MultipleInstances 'Parallel'
	$cred = Get-Credential  -Message '为了把卡死你3000，添加到win任务计划，请输入账户和密码'
	Register-ScheduledTask `
		-TaskName  "卡死你3000主控机高可用_备机" `
		-User "${env:COMPUTERNAME}\${env:USERNAME}" `
		-Password $cred.GetNetworkCredential().Password `
		-RunLevel Highest `
		-Action $操作  `
		-Trigger $触发器 `
		-Settings $设置

	$任务2 = Get-ScheduledTask -TaskName "卡死你3000主控机高可用_备机"
	$任务2.Triggers.Repetition.Duration = "P1D"
	$任务2.Triggers.Repetition.Interval = "PT1M"
	$任务2 | Set-ScheduledTask -User "${env:COMPUTERNAME}\${env:USERNAME}" -Password $cred.GetNetworkCredential().Password
}

