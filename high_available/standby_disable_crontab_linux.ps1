﻿#建议保存编码为：bom头 + utf8

if ($IsLinux -eq $True)
{
	$username = /usr/bin/whoami
	if ($username -ne 'root')
	{
		Write-Error '非管理员'
		Exit 1
	}

	#卸载 k_crontab
	Copy-Item -LiteralPath '/var/spool/cron/root' -Destination '/var/spool/' -Force
	$private:新名字 = 'root-' + (Get-Date -format 'yyyy-MM-dd-HH:mm:ss')
	Rename-Item -LiteralPath '/var/spool/root' -NewName $private:新名字
	Write-Warning '已经备份任务计划文件，到/var/spool/'
	$所有任务计划 = /usr/bin/crontab -l

	if ($所有任务计划 -match 'standby')
	{
		$crontab = Get-Content -LiteralPath '/var/spool/cron/root'
		$crontab2 = @()
		foreach ($行 in $crontab)
		{
			if ($行.contains('standby') )
			{
			}
			else
			{
				$crontab2 += $行
			}
		}
		Set-Content -LiteralPath '/var/spool/cron/root' -Value $crontab2 -Encoding UTF8nobom
	}
	else
	{
		Write-Error '任务计划已经卸载！'
	}
}





exit 0
