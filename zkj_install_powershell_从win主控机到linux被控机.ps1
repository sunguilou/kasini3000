﻿#建议保存编码为：bom头 + utf8
#依赖winscp，并且只能执行单行脚本。
param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$目的ip地址
)


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	Write-Error "应该使用另一个脚本！"
	exit 11
}

& 'v-kai开启详细信息输出.ps1'

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	Import-Module -Name WinSCP -ErrorAction Stop
	& 'zd只读nodelist文件.ps1'
	$当前被控机 = $global:所有被控机 | Where-Object { $_.ip -eq $目的ip地址 }
	if ($当前被控机.ip -ne $目的ip地址)
	{
		Write-Error "错误：当前被控机获取失败 ${当前被控机}"
		exit 13
	}

	& 'winscp复制_linux安装shell脚本_到被控机_win2linux_pwd.ps1' -被控机ip地址 $目的ip地址 -被控机上root的ssh密码明文 $当前被控机.当前密码


	$用户名 = 'root'
	$用户密码密文 = ConvertTo-SecureString $当前被控机.当前密码 -AsPlainText -Force
	$我的登陆凭据 = New-Object System.Management.Automation.PSCredential ($用户名,$用户密码密文)
	$sftp连接参数 = New-WinSCPSessionOption -Protocol Sftp -HostName $目的ip地址 -Credential $我的登陆凭据
	$指纹 = Get-WinSCPHostKeyFingerprint -SessionOption $sftp连接参数 -Algorithm SHA-256
	$sftp连接参数.SshHostKeyFingerprint = $指纹
	$private:连接51 = New-WinSCPSession -SessionOption $sftp连接参数
	if ($private:连接51 -eq $null)
	{
		Write-Error "使用ssh密码，在${目的ip地址}上连接失败"
		exit 21
	}
	else
	{
		$密码连接成功 = $true
		Write-Verbose '使用ssh密码，连接成功。'
	}

	if ($密码连接成功 -eq $true)
	{
		Write-Verbose '连接成功。现在开始执行命令：'
		if (Test-WinSCPPath -WinSCPSession $连接51 -Path '/usr/bin/pwsh')
		{
			Write-Verbose '已安装powershell。开始检查powershell-sshd'
			$检查cmd12 = @'
if grep -Eq 'powershell' /etc/ssh/sshd_config; then echo 'powershell_ssh_good';fi
'@

			$检查cmd12返回 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $检查cmd12
			if ($检查cmd12返回.output -ne 'powershell_ssh_good')
			{
				Write-Warning '开始更改powershell-sshd'
				$检查cmd22 = @'
echo -e '\nSubsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile\nUseDNS no' >>  /etc/ssh/sshd_config
'@

				$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $检查cmd22

				$检查cmd32 = @'
systemctl restart sshd.service ;  rc-service sshd restart
'@

				$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $检查cmd32
				Write-Warning '更改powershell-sshd结束'
			}
			Write-Verbose '检查powershell-sshd结束'
		}
		else
		{
			Write-Warning "在${目的ip地址}上，未安装pwsh，现在开始安装"

			#region centos7
			$判断centos = @'
 if grep -iEq 'CentOS-7' /etc/*-release; then echo 'CentOS7';fi
'@

			$centos = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $判断centos
			if ($centos.output -eq 'CentOS7')
			{
				$判断centos6 = @'
rpm -q centos-release
'@

				$centos6 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $判断centos6
				if ( $centos6.output.contains('el6'.tolower()) )
				{
					Write-Error '不支持centos6'
					Exit 12
				}


				$安装cmd1 = @'
curl -o /etc/yum.repos.d/microsoft.repo  https://packages.microsoft.com/config/rhel/7/prod.repo ;yum remove -y powershell
'@

				Write-Verbose $安装cmd1
				$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd1
				Write-Verbose "在${目的ip地址}上,在centos7系统上，安装pwsh，步骤1完成 ${LASTEXITCODE}"

				$安装cmd2 = @'
yum install -y powershell
'@

				Write-Verbose $安装cmd2
				$安装返回2 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd2
				if ($安装返回2.IsSuccess -ne $true)
				{
					$安装返回3 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd2
					if ($安装返回3.IsSuccess -ne $true)
					{
						Write-Error '安装失败，请使用离线安装包安装。'
						Exit 14
					}
				}
				Write-Verbose "在${目的ip地址}上,在centos7系统上，安装pwsh，步骤2完成 ${LASTEXITCODE}"

				Start-Sleep -Seconds 1
				$安装cmd32 = @'
echo 'Set-PSReadlineOption -EditMode Windows' > /root/.config/powershell/Microsoft.PowerShell_profile.ps1
'@

				Write-Verbose $安装cmd32
				$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd32
				Write-Verbose "在${目的ip地址}上,在centos7系统上，安装pwsh，步骤3完成 ${LASTEXITCODE}"

				$安装cmd42 = @'
if grep -Eq 'powershell' /etc/ssh/sshd_config; then echo 'powershell_ssh_good';fi
'@

				Write-Verbose $安装cmd42
				$安装cmd42返回 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd42

				if ($安装cmd42返回.output -ne 'powershell_ssh_good')
				{
					$安装cmd52 = @'
echo -e '\nSubsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile\nUseDNS no' >>  /etc/ssh/sshd_config
'@

					Write-Verbose $安装cmd52
					$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command  $安装cmd52
					Write-Verbose "在${目的ip地址}上,在centos7系统上，安装pwsh，步骤4完成 ${LASTEXITCODE}"

					$安装cmd62 = @'
systemctl restart sshd.service
'@

					Write-Verbose $安装cmd62
					$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command  $安装cmd62
					Write-Verbose "在${目的ip地址}上,在centos7系统上，安装pwsh，步骤5完成 ${LASTEXITCODE}"
				}
				Write-Warning "在${目的ip地址}上,在centos7系统上，安装pwsh完成"
			}
			#endregion centos7

			#region centos8
			$判断centos = @'
 if grep -iEq 'CentOS-8' /etc/*-release; then echo 'CentOS8';fi
'@

			$centos = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $判断centos
			if ($centos.output -eq 'CentOS8')
			{
				$判断centos6 = @'
rpm -q centos-release
'@

				$centos6 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $判断centos6
				if ( $centos6.output.contains('el6'.tolower()) )
				{
					Write-Error '不支持centos6'
					Exit 12
				}


				$安装cmd1 = @'
curl -o /etc/yum.repos.d/microsoft.repo  https://packages.microsoft.com/config/rhel/8/prod.repo ;yum remove -y powershell
'@

				Write-Verbose $安装cmd1
				$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd1
				Write-Verbose "在${目的ip地址}上,在centos8系统上，安装pwsh，步骤1完成 ${LASTEXITCODE}"

				$安装cmd2 = @'
yum install -y powershell
'@

				Write-Verbose $安装cmd2
				$安装返回2 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd2
				if ($安装返回2.IsSuccess -ne $true)
				{
					$安装返回3 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd2
					if ($安装返回3.IsSuccess -ne $true)
					{
						Write-Error '安装失败，请使用离线安装包安装。'
						Exit 14
					}
				}
				Write-Verbose "在${目的ip地址}上,在centos8系统上，安装pwsh，步骤2完成 ${LASTEXITCODE}"

				Start-Sleep -Seconds 1
				$安装cmd32 = @'
echo 'Set-PSReadlineOption -EditMode Windows' > /root/.config/powershell/Microsoft.PowerShell_profile.ps1
'@

				Write-Verbose $安装cmd32
				$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd32
				Write-Verbose "在${目的ip地址}上,在centos8系统上，安装pwsh，步骤3完成 ${LASTEXITCODE}"

				$安装cmd42 = @'
if grep -Eq 'powershell' /etc/ssh/sshd_config; then echo 'powershell_ssh_good';fi
'@

				Write-Verbose $安装cmd42
				$安装cmd42返回 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd42

				if ($安装cmd42返回.output -ne 'powershell_ssh_good')
				{
					$安装cmd52 = @'
echo -e '\nSubsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile\nUseDNS no' >>  /etc/ssh/sshd_config
'@

					Write-Verbose $安装cmd52
					$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command  $安装cmd52
					Write-Verbose "在${目的ip地址}上,在centos8系统上，安装pwsh，步骤4完成 ${LASTEXITCODE}"

					$安装cmd62 = @'
systemctl restart sshd.service
'@

					Write-Verbose $安装cmd62
					$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command  $安装cmd62
					Write-Verbose "在${目的ip地址}上,在centos8系统上，安装pwsh，步骤5完成 ${LASTEXITCODE}"
				}
				Write-Warning "在${目的ip地址}上,在centos8系统上，安装pwsh完成"
			}
			#endregion centos8

			#region ubuntu1604
			$判断ubuntu1604 = @'
if grep -Eq 'Xenial Xerus' /etc/issue || grep -Eq 'Xenial Xerus' /etc/*-release; then echo 'ubuntu1604';fi
'@

			$ubuntu1604 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $判断ubuntu1604
			if ($ubuntu1604.output -eq 'ubuntu1604')
			{
				$安装cmd1 = @'
sudo rm -rf /var/lib/dpkg/lock;rm -rf /var/lib/dpkg/lock-frontend; dpkg --configure -a;sudo curl -o /tmp/a.deb https://packages.microsoft.com/config/ubuntu/16.04/packages-microsoft-prod.deb ;sudo dpkg -i /tmp/a.deb  ;sudo apt-get update ;sudo apt-get remove -y powershell
'@

				Write-Verbose $安装cmd1
				$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd1
				Write-Verbose "在${目的ip地址}上,在ubuntu1604系统上，安装pwsh，步骤1完成 ${LASTEXITCODE}"

				$安装cmd2 = @'
sudo apt-get install -y powershell
'@

				Write-Verbose $安装cmd2
				$安装返回2 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd2
				if ($安装返回2.IsSuccess -ne $true)
				{
					$安装返回3 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd2
					if ($安装返回3.IsSuccess -ne $true)
					{
						Write-Error '安装失败，请使用离线安装包安装。'
						Exit 14
					}
				}

				Write-Verbose "在${目的ip地址}上,在ubuntu1604系统上，安装pwsh，步骤2完成 ${LASTEXITCODE}"
				Start-Sleep -Seconds 1

				$安装cmd32 = @'
echo 'Set-PSReadlineOption -EditMode Windows' > /root/.config/powershell/Microsoft.PowerShell_profile.ps1
'@

				Write-Verbose $安装cmd32
				$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd32
				Write-Verbose "在${目的ip地址}上,在ubuntu1604系统上，安装pwsh，步骤3完成 ${LASTEXITCODE}"

				$安装cmd42 = @'
if grep -Eq 'powershell' /etc/ssh/sshd_config; then echo 'powershell_ssh_good';fi
'@

				Write-Verbose $安装cmd42
				$安装cmd42返回 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd42

				if ($安装cmd42返回.output -ne 'powershell_ssh_good')
				{
					$安装cmd52 = @'
echo -e '\nSubsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile\nUseDNS no' >>  /etc/ssh/sshd_config
'@

					Write-Verbose $安装cmd52
					$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command  $安装cmd52
					Write-Verbose "在${目的ip地址}上,在ubuntu1604系统上，安装pwsh，步骤4完成 ${LASTEXITCODE}"

					$安装cmd62 = @'
systemctl restart sshd.service
'@
					Write-Verbose $安装cmd62
					$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command  $安装cmd62
					Write-Verbose "在${目的ip地址}上,在ubuntu1604系统上，安装pwsh，步骤5完成 ${LASTEXITCODE}"
				}
				Write-Warning "在${目的ip地址}上,在ubuntu1604系统上，安装pwsh完成"
			}
			#endregion ubuntu1604

			#region ubuntu1804
			$判断ubuntu1804 = @'
if grep -Eq 'Bionic Beaver' /etc/issue || grep -Eq 'Bionic Beaver' /etc/*-release; then echo 'ubuntu1804';fi
'@

			$ubuntu1804 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $判断ubuntu1804
			if ($ubuntu1804.output -eq 'ubuntu1804')
			{
				$安装cmd1 = @'
sudo rm -rf /var/lib/dpkg/lock;rm -rf /var/lib/dpkg/lock-frontend; dpkg --configure -a;sudo snap remove powershell ; sudo curl -o /tmp/a.deb https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb ;sudo dpkg -i /tmp/a.deb ;sudo apt-get update ;sudo add-apt-repository universe;sudo apt-get remove -y powershell
'@

				Write-Verbose $安装cmd1
				$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd1
				Write-Verbose "在${目的ip地址}上,在ubuntu1804系统上，安装pwsh，步骤1完成 ${LASTEXITCODE}"

				$安装cmd2 = @'
sudo apt-get install -y powershell
'@

				Write-Verbose $安装cmd2
				$安装返回2 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd2
				Write-Verbose "在${目的ip地址}上,在ubuntu1804系统上，安装pwsh，步骤2完成 ${LASTEXITCODE}"

				if ($安装返回2.IsSuccess -ne $true)
				{
					$安装返回3 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd2
					if ($安装返回3.IsSuccess -ne $true)
					{
						Write-Error '安装失败，请使用离线安装包安装。'
						Exit 14
					}
				}

				Write-Verbose "在${目的ip地址}上,在ubuntu1804系统上，安装pwsh，步骤2完成 ${LASTEXITCODE}"
				Start-Sleep -Seconds 1

				$安装cmd32 = @'
echo 'Set-PSReadlineOption -EditMode Windows' > /root/.config/powershell/Microsoft.PowerShell_profile.ps1
'@

				Write-Verbose $安装cmd32
				$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd32
				Write-Verbose "在${目的ip地址}上,在ubuntu1804系统上，安装pwsh，步骤3完成 ${LASTEXITCODE}"

				$安装cmd42 = @'
if grep -Eq 'powershell' /etc/ssh/sshd_config; then echo 'powershell_ssh_good';fi
'@

				Write-Verbose $安装cmd42
				$安装cmd42返回 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd42

				if ($安装cmd42返回.output -ne 'powershell_ssh_good')
				{
					$安装cmd52 = @'
echo -e '\nSubsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile\nUseDNS no' >>  /etc/ssh/sshd_config
'@

					Write-Verbose $安装cmd52
					$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command  $安装cmd52
					Write-Verbose "在${目的ip地址}上,在ubuntu1804系统上，安装pwsh，步骤4完成 ${LASTEXITCODE}"

					$安装cmd62 = @'
systemctl restart sshd.service
'@

					Write-Verbose $安装cmd62
					$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command  $安装cmd62
					Write-Verbose "在${目的ip地址}上,在ubuntu1804系统上，安装pwsh，步骤5完成 ${LASTEXITCODE}"
				}
				Write-Warning "在${目的ip地址}上,在ubuntu1804系统上，安装pwsh完成"
			}
			#endregion ubuntu1804

			#region ubuntu2004
			$判断ubuntu2004 = @'
if grep -Eq 'Focal Fossa' /etc/issue || grep -Eq 'Focal Fossa' /etc/*-release; then echo 'ubuntu2004';fi
'@

			$ubuntu2004 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $判断ubuntu2004
			if ($ubuntu2004.output -eq 'ubuntu2004')
			{
				$安装cmd1 = @'
sudo rm -rf /var/lib/dpkg/lock;rm -rf /var/lib/dpkg/lock-frontend; dpkg --configure -a;sudo snap remove powershell ; sudo curl -o /tmp/a.deb https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb ;sudo dpkg -i /tmp/a.deb ;sudo apt-get update ;sudo add-apt-repository universe;sudo apt-get remove -y powershell
'@

				Write-Verbose $安装cmd1
				$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd1
				Write-Verbose "在${目的ip地址}上,在ubuntu2004系统上，安装pwsh，步骤1完成 ${LASTEXITCODE}"

				$安装cmd2 = @'
sudo apt-get install -y powershell
'@

				Write-Verbose $安装cmd2
				$安装返回2 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd2
				Write-Verbose "在${目的ip地址}上,在ubuntu2004系统上，安装pwsh，步骤2完成 ${LASTEXITCODE}"

				if ($安装返回2.IsSuccess -ne $true)
				{
					$安装返回3 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd2
					if ($安装返回3.IsSuccess -ne $true)
					{
						Write-Error '安装失败，请使用离线安装包安装。'
						Exit 14
					}
				}

				Write-Verbose "在${目的ip地址}上,在ubuntu2004系统上，安装pwsh，步骤2完成 ${LASTEXITCODE}"
				Start-Sleep -Seconds 1

				$安装cmd32 = @'
echo 'Set-PSReadlineOption -EditMode Windows' > /root/.config/powershell/Microsoft.PowerShell_profile.ps1
'@

				Write-Verbose $安装cmd32
				$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd32
				Write-Verbose "在${目的ip地址}上,在ubuntu2004系统上，安装pwsh，步骤3完成 ${LASTEXITCODE}"

				$安装cmd42 = @'
if grep -Eq 'powershell' /etc/ssh/sshd_config; then echo 'powershell_ssh_good';fi
'@

				Write-Verbose $安装cmd42
				$安装cmd42返回 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd42

				if ($安装cmd42返回.output -ne 'powershell_ssh_good')
				{
					$安装cmd52 = @'
echo -e '\nSubsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile\nUseDNS no' >>  /etc/ssh/sshd_config
'@

					Write-Verbose $安装cmd52
					$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command  $安装cmd52
					Write-Verbose "在${目的ip地址}上,在ubuntu2004系统上，安装pwsh，步骤4完成 ${LASTEXITCODE}"

					$安装cmd62 = @'
systemctl restart sshd.service
'@

					Write-Verbose $安装cmd62
					$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command  $安装cmd62
					Write-Verbose "在${目的ip地址}上,在ubuntu2004系统上，安装pwsh，步骤5完成 ${LASTEXITCODE}"
				}
				Write-Warning "在${目的ip地址}上,在ubuntu2004系统上，安装pwsh完成"
			}
			#endregion ubuntu2004

			#region debian8
			$判断debian8 = @'
if grep -Eq 'jessie' /etc/issue || grep -Eq 'jessie' /etc/*-release; then echo 'debian8';fi
'@

			$debian8 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $判断debian8
			if ($debian8.output -eq 'debian8')
			{
				$安装cmd1 = @'
rm -rf /var/lib/dpkg/lock;rm -rf /var/lib/dpkg/lock-frontend; dpkg --configure -a;apt-get update ;apt-get install -y curl gnupg apt-transport-https sudo;curl -o /tmp/a.asc https://packages.microsoft.com/keys/microsoft.asc ; apt-key add /tmp/a.asc ;echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-debian-jessie-prod stretch main" > /etc/apt/sources.list.d/microsoft.list ;sudo apt-get update ;sudo apt-get remove -y powershell
'@

				Write-Verbose $安装cmd1
				$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd1
				Write-Verbose "在${目的ip地址}上,在debian9系统上，安装pwsh，步骤1完成 ${LASTEXITCODE}"

				$安装cmd2 = @'
sudo apt-get install -y powershell
'@

				Write-Verbose $安装cmd2
				$安装返回2 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd2
				if ($安装返回2.IsSuccess -ne $true)
				{
					$安装返回3 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd2
					if ($安装返回3.IsSuccess -ne $true)
					{
						Write-Error '安装失败，请使用离线安装包安装。'
						Exit 14
					}
				}

				Write-Verbose "在${目的ip地址}上,在debian8系统上，安装pwsh，步骤2完成 ${LASTEXITCODE}"
				Start-Sleep -Seconds 1
				$安装cmd32 = @'
echo 'Set-PSReadlineOption -EditMode Windows' > /root/.config/powershell/Microsoft.PowerShell_profile.ps1
'@

				Write-Verbose $安装cmd32
				$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd32
				Write-Verbose "在${目的ip地址}上,在debian8系统上，安装pwsh，步骤3完成 ${LASTEXITCODE}"

				$安装cmd42 = @'
if grep -Eq 'powershell' /etc/ssh/sshd_config; then echo 'powershell_ssh_good';fi
'@

				Write-Verbose $安装cmd42
				$安装cmd42返回 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd42

				if ($安装cmd42返回.output -ne 'powershell_ssh_good')
				{
					$安装cmd52 = @'
echo -e '\nSubsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile\nUseDNS no' >>  /etc/ssh/sshd_config
'@

					Write-Verbose $安装cmd52
					$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command  $安装cmd52
					Write-Verbose "在${目的ip地址}上,在debian8系统上，安装pwsh，步骤4完成 ${LASTEXITCODE}"

					$安装cmd62 = @'
systemctl restart sshd.service
'@

					Write-Verbose $安装cmd62
					$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command  $安装cmd62
					Write-Verbose "在${目的ip地址}上,在debian8系统上，安装pwsh，步骤5完成 ${LASTEXITCODE}"
				}
				Write-Warning "在${目的ip地址}上,在debian8系统上，安装pwsh完成"
			}
			#endregion debian8

			#region debian9
			$判断debian9 = @'
if grep -Eq 'stretch' /etc/issue || grep -Eq 'stretch' /etc/*-release; then echo 'debian9';fi
'@

			$debian9 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $判断debian9
			if ($debian9.output -eq 'debian9')
			{
				$安装cmd1 = @'
rm -rf /var/lib/dpkg/lock;rm -rf /var/lib/dpkg/lock-frontend;dpkg --configure -a; apt-get update ;apt-get install -y curl gnupg apt-transport-https sudo;curl -o /tmp/a.asc https://packages.microsoft.com/keys/microsoft.asc ; apt-key add /tmp/a.asc ;echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-debian-stretch-prod stretch main" > /etc/apt/sources.list.d/microsoft.list ;sudo apt-get update ;sudo apt-get remove -y powershell
'@

				Write-Verbose $安装cmd1
				$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd1
				Write-Verbose "在${目的ip地址}上,在debian9系统上，安装pwsh，步骤1完成 ${LASTEXITCODE}"

				$安装cmd2 = @'
sudo apt-get install -y powershell
'@

				Write-Verbose $安装cmd2
				$安装返回2 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd2
				if ($安装返回2.IsSuccess -ne $true)
				{
					$安装返回3 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd2
					if ($安装返回3.IsSuccess -ne $true)
					{
						Write-Error '安装失败，请使用离线安装包安装。'
						Exit 14
					}
				}

				Write-Verbose "在${目的ip地址}上,在debian9系统上，安装pwsh，步骤2完成 ${LASTEXITCODE}"
				Start-Sleep -Seconds 1
				$安装cmd32 = @'
echo 'Set-PSReadlineOption -EditMode Windows' > /root/.config/powershell/Microsoft.PowerShell_profile.ps1
'@

				Write-Verbose $安装cmd32
				$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd32
				Write-Verbose "在${目的ip地址}上,在debian9系统上，安装pwsh，步骤3完成 ${LASTEXITCODE}"

				$安装cmd42 = @'
if grep -Eq 'powershell' /etc/ssh/sshd_config; then echo 'powershell_ssh_good';fi
'@

				Write-Verbose $安装cmd42
				$安装cmd42返回 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd42

				if ($安装cmd42返回.output -ne 'powershell_ssh_good')
				{
					$安装cmd52 = @'
echo -e '\nSubsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile\nUseDNS no' >>  /etc/ssh/sshd_config
'@

					Write-Verbose $安装cmd52
					$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command  $安装cmd52
					Write-Verbose "在${目的ip地址}上,在debian9系统上，安装pwsh，步骤4完成 ${LASTEXITCODE}"

					$安装cmd62 = @'
systemctl restart sshd.service
'@

					Write-Verbose $安装cmd62
					$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command  $安装cmd62
					Write-Verbose "在${目的ip地址}上,在debian9系统上，安装pwsh，步骤5完成 ${LASTEXITCODE}"
				}
				Write-Warning "在${目的ip地址}上,在debian9系统上，安装pwsh完成"
			}
			#endregion debian9

			#region debian10

			$判断debian10 = @'
if grep -Eq 'buster' /etc/issue || grep -Eq 'buster' /etc/*-release; then echo 'debian10';fi
'@

			$debian10 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $判断debian10
			if ($debian10.output -eq 'debian10')
			{
				$安装cmd1 = @'
rm -rf /var/lib/dpkg/lock;rm -rf /var/lib/dpkg/lock-frontend;dpkg --configure -a; apt-get update ;apt-get install -y curl gnupg apt-transport-https sudo;cd /tmp;wget https://packages.microsoft.com/keys/microsoft.asc ; apt-key add /tmp/microsoft.asc ;echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-debian-buster-prod buster main" > /etc/apt/sources.list.d/microsoft.list ;sudo apt-get update ;sudo apt-get remove -y powershell
'@

				Write-Verbose $安装cmd1
				$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd1
				Write-Verbose "在${目的ip地址}上,在debian10系统上，安装pwsh，步骤1完成 ${LASTEXITCODE}"

				$安装cmd2 = @'
sudo apt-get install -y powershell
'@

				Write-Verbose $安装cmd2
				$安装返回2 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd2
				if ($安装返回2.IsSuccess -ne $true)
				{
					$安装返回3 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd2
					if ($安装返回3.IsSuccess -ne $true)
					{
						Write-Error '安装失败，请使用离线安装包安装。'
						Exit 14
					}
				}

				Write-Verbose "在${目的ip地址}上,在debian10系统上，安装pwsh，步骤2完成 ${LASTEXITCODE}"
				Start-Sleep -Seconds 1

				$安装cmd32 = @'
echo 'Set-PSReadlineOption -EditMode Windows' > /root/.config/powershell/Microsoft.PowerShell_profile.ps1
'@

				Write-Verbose $安装cmd32
				$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd32
				Write-Verbose "在${目的ip地址}上,在debian10系统上，安装pwsh，步骤3完成 ${LASTEXITCODE}"

				$安装cmd42 = @'
if grep -Eq 'powershell' /etc/ssh/sshd_config; then echo 'powershell_ssh_good';fi
'@

				Write-Verbose $安装cmd42
				$安装cmd42返回 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd42

				if ($安装cmd42返回.output -ne 'powershell_ssh_good')
				{
					$安装cmd52 = @'
echo -e '\nSubsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile\nUseDNS no' >>  /etc/ssh/sshd_config
'@
					Write-Verbose $安装cmd52
					$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command  $安装cmd52
					Write-Verbose "在${目的ip地址}上,在debian10系统上，安装pwsh，步骤4完成 ${LASTEXITCODE}"

					$安装cmd62 = @'
systemctl restart sshd.service
'@
					Write-Verbose $安装cmd62
					$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command  $安装cmd62
					Write-Verbose "在${目的ip地址}上,在debian10系统上，安装pwsh，步骤5完成 ${LASTEXITCODE}"
				}
				Write-Warning "在${目的ip地址}上,在debian10系统上，安装pwsh完成"
			}
			#endregion debian10

			#region alpine

			$判断alpine = @'
if grep -iEq 'alpine' /etc/issue || grep -iEq 'alpine' /etc/*-release; then echo 'alpine';fi
'@

			$alpine = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $判断alpine
			if ($alpine.output -eq 'alpine')
			{
				$安装cmd1 = @'
OPENSSH_INSTALLED=`apk info |grep -i "openssh-server$"` ; if [ ${OPENSSH_INSTALLED} != "openssh-server" ];then apk update ; apk add openssh-server ; rc-service dropbear stop ; sleep 1; rc-update del dropbear; rc-update add sshd; rc-service sshd start ; fi
'@

				Write-Verbose "在${目的ip地址}上,在alpine系统上，开始安装openssh-server"
				$null = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd1
				Write-Verbose "在${目的ip地址}上,在alpine系统上，安装openssh-server，步骤1完成 ${LASTEXITCODE}"

				$安装cmd2 = @'
/bin/ash -f /tmp/alpine_install_powershell.ash
'@

				Write-Verbose $安装cmd2
				$安装返回2 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd2
				if ($安装返回2.IsSuccess -ne $true)
				{
					$安装返回3 = Invoke-WinSCPCommand -WinSCPSession $连接51 -Command $安装cmd2
					if ($安装返回3.IsSuccess -ne $true)
					{
						Write-Error '安装失败，请使用离线安装包安装。'
						Exit 14
					}
				}
				Write-Warning "在${目的ip地址}上,在alpine系统上，安装pwsh完成"
			}
			#endregion alpine
		}
	}
}



exit 0
