﻿#建议保存编码为：bom头 + utf8

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$被控机显示名
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$global:当前被控机_ip = $null
$global:当前被控机_组 = $null
$global:当前被控机_uuid = $null
$global:当前被控机_os = $null
$global:当前被控机_esxi宿主机 = $null

& 'zd只读nodelist文件.ps1'

$global:当前被控机_ip = $global:所有被控机 | Where-Object { $_.被控机显示名 -eq $被控机显示名 }
if ($global:当前被控机_ip -eq $null)
{
	Write-Error "错误：nodelist文件中找不到这个被控机： ${被控机显示名} "
	if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
	{
		& "${global:kasini3000目录}\admin_gui\pic\bc随机报错背景图片.ps1"
	}

	function global:prompt
	{
		"`e[91m`e[44m【{0}】`e[0m{1}> " -f '错误：无此被控机名',$PWD
	}
	exit 11
}

& cdip.ps1 -被控机ip地址 $global:当前被控机_ip.ip
