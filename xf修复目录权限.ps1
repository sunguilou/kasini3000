﻿#建议保存编码为：bom头 + utf8
if ($global:xf修复目录权限 -eq $true)
{
	exit
}
$global:xf修复目录权限 = $true

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	Write-Verbose "错误，修复卡死你3000目录权限功能，暂不支持win"
	exit 1
}

if ($IsLinux -eq $True)
{
	if ($global:kasini3000目录)
	{
		$所有子目录 = Get-ChildItem -LiteralPath $global:kasini3000目录 -Recurse -Directory
		foreach ($temp1 in $所有子目录)
		{
			chmod 700 $temp1
		}

		$所有子文件 = Get-ChildItem -LiteralPath $global:kasini3000目录 -Recurse -File
		foreach ($temp2 in $所有子文件)
		{
			if ($temp2.Extension.tolower().contains('sh') )
			{
				chmod 700 $temp2.fullname
			}
			else
			{
				chmod 600 $temp2.fullname
			}
		}
		chmod 700 $global:kasini3000目录

		$ssh_key2 = "${global:kasini3000目录}/ssh_key_files_old1/id_rsa"
		if (Test-Path -LiteralPath $ssh_key2)
		{
			chmod 600 $ssh_key2
		}
		else
		{
			$没有sshkey = 1
		}

		$ssh_key1 = '/root/.ssh/id_rsa'
		if (Test-Path -LiteralPath $ssh_key1)
		{
			chmod 600 $ssh_key1
		}
		else
		{
			$没有sshkey++
		}

		if ($没有sshkey -ge 2)
		{
			$ErrorActionPreference = 'continue'
			Write-Error "错误：没找到任何ssh key。卡死你3000ssh主控机，管理ssh被控机，必须使用ssh key。`n请运行【jl建立主控机ssh秘钥1z.ps1】"
		}
		chmod 700 /etc/kasini3000/kc.sh
		chmod 700 /etc/kasini3000/kcp.sh
	}
	else
	{
		Write-Error '错误，找不到全局变量$global:kasini3000目录'
		exit 2
	}

}
exit 0

