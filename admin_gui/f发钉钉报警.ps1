#问：这个脚本谁写的？有问题找谁技术支持？
#答：QQ群号=183173532
#名称=powershell交流群
#华夏脚之峰。Everest of the powershell

Param
(
	[String]$token = ''	, #动态输入账户，或改写源码写入。

	[String]$报警信息 = "卡死你3000`nhttps://gitee.com/chuanjiao10/kasini3000"
)

$msg = @'
用法：
1 改写源码，填入token。或动态输入。
2 c:\ProgramData\kasini3000\admin_gui\f发钉钉报警.ps1 -报警信息 '卡死你3000'
3 报警信息，支持换行。
手册：
https://ding-doc.dingtalk.com/doc#/serverapi2/krgddi
'@

Write-Warning $msg

if ( ($token -eq $null) -or ($token -eq '') )
{
	Write-Error '错误：没输入token。退出码1'
	exit 1
}


[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

[String]$钉钉网址 = @"
https://oapi.dingtalk.com/robot/send?access_token=$token
"@

$报警信息_json = @"
{
	"msgtype" : "text",
	"text" : {
		"content" : "$报警信息"
	},
	"at": {
        "isAtAll": true
    }
}
"@
Write-Verbose $报警信息_json

$chinese = [System.Text.Encoding]::UTF8.GetBytes($报警信息_json) #这里是解决中文编码问题的即发送中文消息时候使用。

Invoke-RestMethod -uri $钉钉网址 `
	-ContentType "application/json;charset=utf-8"  `
	-Method Post  `
	-Body $chinese

