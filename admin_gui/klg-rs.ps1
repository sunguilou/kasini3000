﻿#建议保存编码为：bom头 + utf8

param
(
	[String]$备注
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

function 获取最新输出
{
	$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'fb福报库_任务表rs_读取最后n个任务结果输出l178.ps1'
	if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
	{
		Write-Error "fb福报库_任务表rs_读取最后n个任务结果输出l178.ps1' "
		exit 178
	}
	else
	{
		$脚本名 = $private:temp999
	}


	$输出 = & $脚本名 -最后n个输出 1 -备注 $备注
	#	$输出 | Get-Member
	$script:当前输出任务id = $输出['不可重任务id']
	$输出2 = [PSCustomObject]@{
		'不可重任务id'  = $输出['不可重任务id']
		'被控机ip'    = $输出['被控机ip']
		'标准输出'     = $输出['标准输出']
		'任务状态'     = $输出['任务状态']
		'任务预定开始时间' = $输出['任务预定开始时间']
		'任务实际结束时间' = $输出['任务实际结束时间']
		'任务预定超时时间' = $输出['任务预定超时时间']
		'命令行'      = $输出['命令行']
		'备注'       = $输出['备注']
	}

	return $输出2
}

function 获取上一个输出
{
	$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'fb福报库_任务表rs_读取最后n个任务结果输出l1782.ps1'
	if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
	{
		Write-Error "fb福报库_任务表rs_读取最后n个任务结果输出l1782.ps1' "
		exit 1782
	}
	else
	{
		$脚本名 = $private:temp999
	}

	$script:当前输出任务id--
	$输出 = & $脚本名 -不可重任务id $script:当前输出任务id
	#	$输出 | Get-Member
	$script:当前输出任务id = $输出['不可重任务id']
	$输出2 = [PSCustomObject]@{
		'不可重任务id'  = $输出['不可重任务id']
		'被控机ip'    = $输出['被控机ip']
		'标准输出'     = $输出['标准输出']
		'任务状态'     = $输出['任务状态']
		'任务预定开始时间' = $输出['任务预定开始时间']
		'任务实际结束时间' = $输出['任务实际结束时间']
		'任务预定超时时间' = $输出['任务预定超时时间']
		'命令行'      = $输出['命令行']
		'备注'       = $输出['备注']
	}

	return $输出2
}


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{


	Add-Type -AssemblyName system.Windows.Forms
	Add-Type -AssemblyName system.Drawing
	$Form = New-Object "System.Windows.Forms.Form"
	$Form.Size = New-Object System.Drawing.Size @(800,600)
	$Form.Text = "卡死你3000，福报rs表，list输出"
	$PropertyGrid = New-Object System.Windows.Forms.PropertyGrid
	$PropertyGrid.Dock = [System.Windows.Forms.DockStyle]::fill
	$PropertyGrid.SelectedObject = 获取最新输出

	$PropertyGrid.PropertySort = 'NoSort'

	$groupBox1 = New-Object System.Windows.Forms.GroupBox
	$groupBox1.Name = "groupBox1"
	$groupBox1.Anchor = 13
	$groupBox1.Controls.Add($PropertyGrid)
	$System_Drawing_Size = New-Object System.Drawing.Size
	$System_Drawing_Size.Width = 780
	$System_Drawing_Size.Height = 516
	$groupBox1.Size = $System_Drawing_Size
	$System_Drawing_Point = New-Object System.Drawing.Point
	$System_Drawing_Point.X = 2
	$System_Drawing_Point.Y = 0
	$groupBox1.Location = $System_Drawing_Point

	$button1 = New-Object System.Windows.Forms.Button
	$button1_OnClick =
	{
		$button1.enabled = $false
		$PropertyGrid.SelectedObject = 获取最新输出
		$button1.enabled = $True
		$button2.enabled = $True
	}
	$button1.Name = "button1"
	$System_Drawing_Size = New-Object System.Drawing.Size
	$System_Drawing_Size.Width = 160
	$System_Drawing_Size.Height = 30
	$button1.Size = $System_Drawing_Size
	$button1.Text = "最新任务输出"
	$button1.Font = New-Object System.Drawing.Font("微软雅黑",15,1,3,134)
	$System_Drawing_Point = New-Object System.Drawing.Point
	$System_Drawing_Point.X = 5
	$System_Drawing_Point.Y = 526
	$button1.Location = $System_Drawing_Point
	$button1.DataBindings.DefaultDataSourceUpdateMode = 0
	$button1.enabled = $True
	$button1.add_Click($button1_OnClick)

	$button2 = New-Object System.Windows.Forms.Button
	$button2_OnClick =
	{
		$button2.enabled = $false
		$PropertyGrid.SelectedObject = 获取上一个输出
		$button2.enabled = $True
	}
	$button2.Name = "button2"
	$System_Drawing_Size = New-Object System.Drawing.Size
	$System_Drawing_Size.Width = 160
	$System_Drawing_Size.Height = 30
	$button2.Size = $System_Drawing_Size
	$button2.Text = "上一个任务输出"
	$button2.Font = New-Object System.Drawing.Font("微软雅黑",15,1,3,134)
	$System_Drawing_Point = New-Object System.Drawing.Point
	$System_Drawing_Point.X = 185
	$System_Drawing_Point.Y = 526
	$button2.Location = $System_Drawing_Point
	$button2.DataBindings.DefaultDataSourceUpdateMode = 0
	$button2.enabled = $false
	$button2.add_Click($button2_OnClick)



	$Form.Controls.Add($button1)
	$Form.Controls.Add($button2)
	$Form.Controls.Add($groupBox1)
	$Form.TopMost = $true
	$Form.StartPosition = "CenterScreen"
	$null = $Form.ShowDialog()


}

if ($IsLinux -eq $True)
{
	Write-Verbose "错误，此功能，暂不支持linux"
	exit 1
}


exit 0
