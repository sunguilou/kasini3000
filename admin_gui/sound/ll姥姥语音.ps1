﻿if (-not $args[0])
{
	Write-Warning '用法：'
	Write-Warning 'll姥姥语音.ps1    “我要把你嫁给，黑山老妖！我要你打碎金塔，在枉死城陪我我我我”'
	$args = @(“我要把你嫁给，黑山老妖！我要你打碎金塔，在枉死城陪我我我我”)
}

Add-Type -AssemblyName System.Speech
$sy = [System.Speech.Synthesis.SpeechSynthesizer]::new()
$sy.SelectVoiceByHints([System.Speech.Synthesis.VoiceGender]::Female)
$女 = $true
$分隔符 = "`t，。,.！ "

$n段文字 = $args[0].split($分隔符)
foreach ($1段文字 in $n段文字)
{
	$sy.Speak($1段文字)

	if ($女 -eq $true)
	{
		$sy.SelectVoiceByHints([System.Speech.Synthesis.VoiceGender]::Male)
		$女 = $false
	}
	else
	{
		$sy.SelectVoiceByHints([System.Speech.Synthesis.VoiceGender]::Female)
		$女 = $true
	}
}
$sy.Dispose()

exit 0
