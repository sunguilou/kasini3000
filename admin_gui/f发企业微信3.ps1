#问：这个脚本谁写的？有问题找谁技术支持？
#答：QQ群号=183173532
#名称=powershell交流群
#华夏脚之峰。Everest of the powershell
# 感谢天闲分享。 2018-10-11

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$msg = @'
用法：
1 改写源码，填入企业id等。
2 import-module d:\保存目录\f发企业微信3.ps1
3 f-WeiXin -Content "信息内容"
'@
Write-Warning $msg

function f-WeiXin
{
	Param
	(
		[String]$corpid = "", #输入。企业唯一id。可在管理后台“我的企业”－“企业信息”下查看“企业ID”（需要有管理员权限）
		[String]$corpsecret = "", #密码。在管理后台->“应用与小程序”->“应用”->“自建”，点进某个应用，即可看到。
		[String]$Username = "", #收件人
		[String]$Content = "", #信息内容
		[String]$agentid = "" #输入。应用与小程序id。管理后台->“应用与小程序”->“应用”，点进某个应用，即可看到agentid。
	)
	# https://work.weixin.qq.com/api/doc

	$auth_string = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=$corpid&corpsecret=$corpsecret"
	$auth_values = Invoke-RestMethod $auth_string

	# Send message 下面是微信JSON内容的写法
	$token = $auth_values.access_token
	$body = "{
`"touser`":`"$username`",
`"msgtype`":`"text`",
`"agentid`":`"$agentid`",
`"text`":
{`"content`":`"$content`"},
`"safe`":`"0`"
}"
	$chinese = [System.Text.Encoding]::UTF8.GetBytes($body) #这里是解决中文编码问题的即发送中文消息时候使用。
	Invoke-RestMethod "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=$token" -ContentType "application/json"  -Method Post  -Body $chinese

}


