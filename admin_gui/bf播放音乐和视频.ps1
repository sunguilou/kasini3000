# 播放音乐和视频.ps1
# Powershell交流群整理、出品    群号=183173532

# 例子：
# 播放音乐和视频.ps1 -音乐文件  "F:\MP3\MOVBLE\月亮之上.mp3"
param
(
	$音乐文件
)

if (Test-Path $音乐文件)
{
	Add-Type -AssemblyName presentationCore
	$mediaPlayer = New-Object System.Windows.Media.MediaPlayer
	Write-Host "正在播放 ==>  $音乐文件"
	$mediaPlayer.open([uri]"$音乐文件")
	$mediaPlayer.Play()
}
else
{
	Write-Error '找不到音乐文件！'
}







