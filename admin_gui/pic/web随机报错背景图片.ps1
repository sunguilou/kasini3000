﻿#建议保存编码为：bom头 + utf8

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	Write-Error '【表情包斗图乐】功能，只支持win-18362版本，及以上。'
	exit 2
}

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	$private:pname = (Get-Process -id $pid).Parent.ProcessName
	$private:ppname = (Get-Process -id $pid).Parent.Parent.ProcessName
	if ( ($private:pname -eq 'WindowsTerminal') -or ($private:ppname -eq 'WindowsTerminal') )
	{
	}
	else
	{
		Write-Error "错误：未找到Windows Terminal。"
		exit 1
	}

	$winterm配置文件 = "${env:USERPROFILE}\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json"

	$private:temp999 = & 'kdir-node-script.ps1' -被查找的脚本文件名 '*ps找算替*'
	if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
	{
		Write-Error "找不到 '*ps找算替*' "
		exit 11
	}
	else
	{
		$ps找算替脚本路径 = $private:temp999
	}

	$所有成功图片文件 = Get-ChildItem -LiteralPath "$PSScriptRoot\web报错" -file
	$当前成功图片文件 = $所有成功图片文件 | Get-Random
	$当前成功图片文件路径 = $当前成功图片文件.fullname.replace('\','\\')

	$test001 =	@{
		输入文件全路径         = $winterm配置文件
		输出文件全路径         = '直接写输入文件，不需要备份'
		输入文件编码          = 65001

		找什么_块首          = @'
"name" : "卡死你3000",
'@

		找什么_块尾          = @'
"startingDirectory" : "c:\\ProgramData\\kasini3000",
'@


		替换成啥            = @"
"name" : "卡死你3000",
            "backgroundImage": "${当前成功图片文件路径}",
            "startingDirectory" : "c:\\ProgramData\\kasini3000",
"@


		不用替换成啥_而用ps代码替换 = $false
		查找模式            = '匹配头尾'
		找不到则在文件末尾追加     = $false
		查找返回的结果必须唯一     = $false
		返回结果不唯一时        = '只替换第一个'
		脚本pid           = $pid # 这行用于脚本防错，不要修改。
	}

	&  "$ps找算替脚本路径"  @test001
}
exit 0
