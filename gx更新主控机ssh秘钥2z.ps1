﻿#建议保存编码为：bom头 + utf8
#删除秘钥2，生成秘钥2。秘钥2不备份，但生成秘钥2的10天内禁止再次生成，避免频繁生成秘钥2导致被控机失控。

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	if (test-path -LiteralPath "$env:USERPROFILE\.ssh\id_rsa")
	{
		$秘钥1 = Get-Item "$env:USERPROFILE\.ssh\id_rsa"
		if ($秘钥1.LastWriteTime -ge (get-date).adddays(-10))
		{
			Write-Error '错误：秘钥1建立日期少于10天，禁止更新秘钥2'
			exit 2
		}
		
		if (test-path -LiteralPath "${global:kasini3000目录}\ssh_key_files_old1")
		{
			Remove-Item -Path "${global:kasini3000目录}\ssh_key_files_old1\*"
		}
		else
		{
			Write-Warning "错误，找不到密钥2路径"
			mkdir -Name "${global:kasini3000目录}\ssh_key_files_old1"
		}
	}
	else
	{
		Write-Error "错误，找不到秘钥1。应该先运行【jl建立主控机ssh秘钥1z.ps1】"
		exit 1
	}

	& 'read-host+timeout_v2.1.ps1' -变量名 '【ssh私钥密码】' -变量值 'y'
	switch (${global:【ssh私钥密码】})
	{
		'y'
		{
			$a =
@"
ssh-keygen -t rsa -b 4096 -N "" -f "${global:kasini3000目录}\ssh_key_files_old1\id_rsa"
"@

			cmd.exe /c $a
			#			ssh-keygen -t rsa -b 4096 -N '' -f '/etc/kasini3000/ssh_key_files_old1/id_rsa'
			break
		}

		'n'
		{
			$b =
@"
ssh-keygen -t rsa -b 4096  -f "${global:kasini3000目录}\ssh_key_files_old1\id_rsa"
"@

			cmd.exe /c $b
			break
		}

		default
		{
			Write-Error 'y/n 输入错误，使用默认值 "n" ！'
			$a =
@"
ssh-keygen -t rsa -b 4096 -N "" -f "${global:kasini3000目录}\ssh_key_files_old1\id_rsa"
"@

			cmd.exe /c $a
		}
	}
}


if ($IsLinux -eq $True)
{
	if (test-path -LiteralPath "$env:USERPROFILE\.ssh\id_rsa")
	{
		$秘钥1 = Get-Item "$env:USERPROFILE\.ssh\id_rsa"
		if ($秘钥1.LastWriteTime -ge (get-date).adddays(-10))
		{
			Write-Error '错误：秘钥1建立日期少于10天，禁止更新秘钥2'
			exit 2
		}
		
		if (test-path -LiteralPath "${global:kasini3000目录}\ssh_key_files_old1")
		{
			Remove-Item -Path "${global:kasini3000目录}\ssh_key_files_old1\*"
		}
		else
		{
			Write-Warning "错误，找不到密钥2路径"
			mkdir -Name "${global:kasini3000目录}\ssh_key_files_old1"
		}
	}
	else
	{
		Write-Error "错误，找不到秘钥1。应该先运行【jl建立主控机ssh秘钥1z.ps1】"
		exit 1
	}
	
	
	if (test-path -LiteralPath '/root/.ssh/id_rsa')
	{
		$秘钥1 = Get-Item '/root/.ssh/id_rsa'
		if ($秘钥1.LastWriteTime -ge (get-date).adddays(-10))
		{
			Write-Error '错误：秘钥1建立日期少于10天，禁止更新秘钥2'
			exit 2
		}
		
		if (test-path -LiteralPath "${global:kasini3000目录}/ssh_key_files_old1")
		{
			Remove-Item -Path "${global:kasini3000目录}/ssh_key_files_old1/*"
		}
		else
		{
			Write-Warning "错误，找不到密钥2路径"
			mkdir -Name "${global:kasini3000目录}/ssh_key_files_old1"
		}
	}
	else
	{
		Write-Error "错误，找不到秘钥1。应该先运行【jl建立主控机ssh秘钥1.ps1】"
		exit 1
	}

	& 'read-host+timeout_v2.1.ps1' -变量名 '【ssh私钥密码】' -变量值 'y'
	switch (${global:【ssh私钥密码】})
	{
		'y'
		{
		
@'
ssh-keygen -t rsa -b 4096 -N '' -f "${global:kasini3000目录}/ssh_key_files_old1/id_rsa"
'@ | /usr/bin/bash


			break
		}

		'n'
		{
		
@'
ssh-keygen -t rsa -b 4096  -f "${global:kasini3000目录}/ssh_key_files_old1/id_rsa"
'@ | /usr/bin/bash


			break
		}

		default
		{
			Write-Error 'y/n 输入错误，使用默认值 "n" ！'
			
@'
ssh-keygen -t rsa -b 4096 -N '' -f "${global:kasini3000目录}/ssh_key_files_old1/id_rsa"
'@ | /usr/bin/bash

		}
	}
	chmod 600 /etc/kasini3000/ssh_key_files_old1/*
	chmod 600 /etc/kasini3000/ssh_key_files_old2/*
}

& 'gx更新主控机上的_双公钥文件authorized_keys.ps1'

exit 0
