
if grep -iEq 'alpine' /etc/issue || grep -iEq 'alpine' /etc/*-release
then
  echo 'linux is alpine'
  echo 'Warning: dropbear sshd will be disabled.'

  OPENSSH_INSTALLED=`apk info |grep -i "openssh-server$"`
  if [ ${OPENSSH_INSTALLED} == "openssh-server" ]
  then
    echo 'openssh-server 已经安装！'
  else
    echo '开始安装 openssh-server'
    apk update
    apk add openssh-server
    echo 'openssh-server 安装完成！'
  fi

  OPENSSH_STATUS=`rc-status |grep "sshd.*started"`
  if [ -z "${OPENSSH_STATUS}" ]
  then
    echo '正在停止 dropbear服务！'
    rc-service dropbear stop
    sleep 1
    rc-update del dropbear
    echo '正在启动 openssh-server服务！'
    rc-update add sshd
    rc-service sshd start
    exit 0
  else
    echo 'openssh-server 已经启动！'
    exit 0
  fi

else
  echo 'linux not alpine'
  exit 3
fi




