﻿# kasini3000 
这是一个开源、免费、跨平台，批量+自动化，运维、编排工具。类似于Puppet，SaltStack，Ansible，pipeline。

跨平台意味着，主控机可以是win，linux。主控机连linux被控机，使用ssh协议。主控机连win被控机，使用winrm协议。

【主控机】控制【被控机】基于【开源 + 免费 + 跨平台的】powershell语言。

主控机采用 “推” 脚本，推脚本块，推拉文件的方式，控制被控机。

可以通过powershell调用bash，100%兼容linux上的.bash脚本；100%兼容csh脚本；100%兼容zsh脚本，py，perl等。100%兼容win上的.bat脚本；vbs脚本，py等。

旧的任何linux脚本，可以通过嵌入在主控机的ps1内，或者分发文件的方式，从主控机到达被控机执行。

powershell加卡死你3000，是这世上最好的远程+批量命令行！

开源、免费、跨平台、国产。《卡死你3000》的口号是：“别去学yaml关键字，和众多模块。卡死你3000让win，linux，命令行再次伟大”

随着中美冲突加剧，不让中国人用某些软件这种事，打压中国软件这些事，将会越来越多。但不让中国人用java，go，python，powershell，等编程语言，应该不会出现。
 
SaltStack是python开发的，c/s架构的批量运维工具。它在2020年5月份出现了，远程执行命令漏洞。详见cve-2020-11651 cve-2020-11652。 

《卡死你3000》和aisible永远没有类似的漏洞！ 卡死你3000和ansible中，开端口监听的agent是： linux的open-sshd和win的winrm。

 ![image](kaiiit.jpg)

项目中文名称=《kaiiit家的饭店》，或《k记饭店》(暂定名)

项目中文别名=《ps1屠龙刀》

项目英文名称=未知

项目中文开发代号=卡死你3000。结合3000种厉害运维脚本于一身，而且每一种脚本皆可独当一面。传说是威力惊人胜过任何武器十倍的运维利器。

项目英文开发代号=kasini3000

项目网页：   https://？？？

项目代码主站：   https://gitee.com/chuanjiao10/kasini3000

项目代码镜像：   https://？？？


------

 ![卡死你3000](kasini3000.jpg)

# 功能： features

1 基于powershell的世界上最好的远程命令行。支持多线程执行，多进程执行。

2 基于sqlite实现的跨平台cmdb，磁盘任务队列。和定时任务。

3 文件复制：win<--->win，win<--->linux，linux<--->linux。支持mac。树莓派。

win主控机支持这些版本：win8.1，win10，win2012r2，win2016，win1019

linux主控机支持这些发行版-版本：centos7，centos8，ubuntu1404（但不建议），ubuntu1604，ubuntu1804，debian8（但不建议），debian9，debian10，alpine3.8---3.11。**不支持centos6**

从win主控机，控制linux被控机。

从win主控机，控制win被控机。

从linux主控机，控制linux被控机。

【被控机清单文件】使用csv文件格式（nodelist.csv）。将来实现【被控机清单文件】使用excel文件（nodelist.xlsx）。

发企业微信脚本。发钉钉脚本。

支持跨地域，跨公有云厂家。公有云、私有云、被控机同时管理。

支持在win、linux卡死你3000主控机上，调用阿里云命令行工具，从而管控阿里云机子。

支持在win、linux卡死你3000主控机上，基于powercli管理vmware vcenter和vmhost母机。提供cdESXi.ps1命令。

【秒】级别的，任务计划（定时任务）功能。支持65分钟循环一次的触发器。在每分钟的1，11，21，31，41，51，秒运行，时间误差大概1秒。

主控机新增，主备高可用功能，alphi第一版。它基于心跳。使用场景是：

1 运行定时任务的卡死你3000主控机（主=嬴政），会每隔10秒推送【定时任务数据库】到（备）

2 主挂了，（备=嬴扶苏）主控机，会探测心跳，并启定时任务。

3 主恢复后，备会关闭定时任务，并归还【定时任务数据库】，主会继续定时任务。


## 基于事件的功能：

master_script目录内新增：

端口探通，

url探活脚本，

node_script目录新增：

jc检测【win-linux进程命令行中】关键字的并发数z3.ps1，

hq获取cpu空闲_当前实时值_win_linux通用3.ps1，

hqm获取空闲内存MB_当前实时值_win_linux通用1.ps1

hqpm根据进程pid_获取进程占用的物理内存_win_linux通用.ps1

hqpm根据端口_获取进程pid_win_linux通用.ps1

## win被控机：

win被控机支持这些版本：win8.1，win10，win2012r2，win2016，win1019，win7（需要安装ps5.1），win2008r2（需要安装ps5.1）

自动/手动 批量更改win被控机密码。即【定期自动更新被控机密码】；

以执行.ps1脚本为基础，支持使用bat脚本，100%兼容bat脚本。100%兼容vbs脚本。支持用户自定义脚本作为功能扩展（插件）；

主控机控制被控机，不需要windows域。即有无windows域都可以控制。

当主控机=win，被控机=win时，100%支持powershell-dsc。同时100%支持linux版的powershell-dsc

## linux被控机：

linux被控机支持这些发行版-版本：centos7，centos8，ubuntu1404，ubuntu1604，ubuntu1804，debian8，debian9，debian10，alpine3.8---3.11，树莓派linux，树莓派win10-iot。**不支持centos6**

类似于定期改linux密码功能：使用双ssh秘钥，管理linux被控机；自动/手动 批量更新ssh pub key到被控机。

以执行.ps1脚本为基础，支持使用bash脚本，100%兼容bash脚本。100%兼容csh脚本，100%兼容zsh脚本。支持shell管道，重定向等。支持用户自定义脚本作为功能扩展（插件）；

bkj_install_linuxpackage.ps1封装了yum和apt-get和apk add。跨linux发行版安装【名字相同的】软件包：
k-commit -powershell代码块 { bkj_install_linuxpackage.ps1 'wget','bash','发行版间包名不一样也可以放这里_只会报找不到' } 



## win声音，图形，控制台：

中英文语音报警功能。需要在主控机上，安装声卡和音箱。（不支持linux主控机）

baoj中英文声音报警.ps1 '报告司令，卡死你3000，工作正常！happy,new,year,2020'

【表情包斗图乐】功能发布。支持gif动图。你可以自己在相关目录内存图！

它的主要功能是：当脚本或命令出错，wrtie-error的同时，调用相关脚本，更改Windows Terminal背景图片，以达到醒目提醒的目的。

 ![表情包斗图乐](/admin_gui/pic/效果图.png)

增加了播放wav，mp3，视频的脚本。


------

# 特色： features

## 本项目和ansible的对比。这个项目怎么样？听我给你吹。。。

我赶脚，这个项目是比ansible强很多的。虽然这是一个人的项目，虽然这个项目的代码是ansible代码的150分之一。

* ansible是写yaml，对文本格式有要求，写中文也不容易。shell脚本对文本格式有要求。但卡死你3000是写powershell脚本，对格式没什么要求。

* 写3层循环，如foreach套while，再加上退出条件判断。yaml表达起来费劲，尤其使用rules时。

* 脚本a，调用子脚本b，b调用孙脚本c，是很常见很简单的。yaml剧本a，调用子yaml剧本b，调用孙yaml剧本c容易么？

* 卡死你3000支持【客户的数据类型】比ansible多。ansible就是折腾yaml，只有【键值对】这种数据类型。
卡死你3000除了通过yaml模块支持键值对之外，更支持csv，json，所有数据库等。

* ansible的功能，依赖其【二手库】。卡死你3000作为脚本，常用功能依赖.net方法，库功能依赖linux命令，厂家官方命令如kubectl。

* 卡死你3000的变量是.net对象。有远程传值.net对象，而不是字符串。在管道符“|”两侧传递对象。有自动传递本地脚本到远程。

* 多线程 + 线程安全型队列对象，很适合于给远程被控机【异步】+【批量】分发任务。一个脚本读取txt，写入【线程安全型队列1】，另一个脚本读取【线程安全型队列1】，分发任务。
结果收取到【线程安全型队列2】，写入txt。这种ansible能实现么？

* 本项目有多线程，后台多线程，远程后台多线程。有【多线程共用的】，【多脚本共用的】全局变量，

* 没有shell脚本的转义问题。远程传递脚本，用大花括号{}，可以括号很多层，脚本可以远程传递很多次。而单双引号最多2层。

* powershell脚本，可以用vscode+remote ssh进行远程调试。如单步、断点、鼠标选中执行等。

* 本项目没有模板，没有jinjia2这些麻烦的幺蛾子。只有call子脚本。

* 想模块化一个功能，有call子脚本，子函数，公共变量，或者写powershell module模块。没有闹人的yaml互相嵌套，没有垃圾的，规定目录的rules。

* 为了解决脚本用户使用数据库的痛点，卡死你3000提供数据库的支持。

* 卡死你3000和vmware的powercli都基于powershell。对象，属性，方法，可以无缝连接。
下一个版本的vsphere基于kubernetes打造。卡死你3000通过vmware的命令，控制k8s具有天生优势。

* arm32版powershell支持安卓手机，安卓平板电脑，苹果osx。

## 任何使用“yaml关键字定义”的用户，都必须严格遵守【对方的yaml定义】，简称【用户是奴隶】。而任何使用“基于脚本代码的”【用户是主人】。

## 请问：基于【win，linux命令】的卡死你3000，能打败基于【yaml关键字定义】的ansible吗？

------
# 围观将来要实现的功能  roadmap

* 2020-04---2021-08，本项目将英文化。脚本名：添加英文别名。脚本参数名：添加英文化别名。添加英文版报错信息。增加英文版手册。

* 2020-06---2022-06，欢迎跟本项目合作。添加web ui。

* 图形界面监控。只会开发zabbix，Prometheus没有的功能。（此功能延迟开发，延迟推出）

* 增加对异步回调任务的支持。

------

# 卡死你3000用户手册  document

* 卡死你3000架构图 https://gitee.com/chuanjiao10/kasini3000/wikis/系统架构说明

* 系统需求 https://gitee.com/chuanjiao10/kasini3000/wikis/系统需求

* 下载：  git  clone  https://gitee.com/chuanjiao10/kasini3000.git

* 安装文档 https://gitee.com/chuanjiao10/kasini3000/wikis/安装

* linux一键安装 卡死你3000的agent  https://gitee.com/chuanjiao10/kasini3000_agent_linux

* 命令列表&帮助： 下载excel文档

https://gitee.com/chuanjiao10/kasini3000/blob/master/docs/%E5%8D%A1%E6%AD%BB%E4%BD%A03000%E5%91%BD%E4%BB%A4%E6%89%8B%E5%86%8C.xlsx

* 常见问题 https://gitee.com/chuanjiao10/kasini3000/wikis/卡死你3000脚本编写要求和常见问题

* 脚本例子 https://gitee.com/chuanjiao10/kasini3000/wikis/卡死你3000脚本例子

* 卡死你3000对数据库的支持  https://gitee.com/chuanjiao10/kasini3000/wikis/用户数据库

* 卡死你3000和jenkins一同使用  https://gitee.com/chuanjiao10/kasini3000/wikis/卡死你3000的jenkins例子
 
* 使用【自建ca】和【自建证书】，给【win主控机】---》winrm---》【win被控机】增加安全性：

《走进winrm的四个安全级别》   https://www.cnblogs.com/piapia/p/11897713.html 

------

# 批量自动运维工具对比图（非精确，欢迎帮忙指出错误）

 ![对比图](批量运维工具对比图.jpg)

------
# 卡死你3000和zabbix是什么关系？

答：

* zabbix=眼睛。提供触发消息。

* 卡死你3000主控机=脑仁。提供逻辑脚本。

* 建议把【卡死你3000主控机】和【zabbix主控机】安装在同一台机子上。
【zabbix】 ---》【卡死你3000主控机】---》主控机探活脚本。活则在被控机上运行脚本a，死则在被控机上运行脚本b。

* 当卡死你3000稳定后。在一定程度上，可以减少zabbix agent的安装和使用。
即不用zabbix agent，而用卡死你3000被控机。

------

# 卡死你3000和jenkins是什么关系？

答：

* jenkins=老板或股东。在后台控制全局。

* 卡死你3000主控机=经理。提供具体操控逻辑。

* 建议把【卡死你3000主控机】和【jenkins】安装在同一台机子上。

【jenkins】---》jenkins的【powershell插件】，或【pipeline插件】等。---》【卡死你3000主控机】---》【卡死你3000被控机】



------

# 欢迎 大力推广 此项目！

## 诚征建议：
为了把阿里云ecs机子，纳入卡死你3000被控机，接受卡死你3000主控机管控，现诚征建议：

* 你希望如何简化操作你的阿里云机子？

* 你控制阿里云机子（产品），有何麻烦之处？

* 你经常用命令行，控制哪些阿里云产品，控制哪些功能？


# 项目状态：

## 【1.0雏形版之alpha1版】 日期：2019-10-06  ~~~ 2019-12-17。已经完成发布代码。

## 【1.0雏形版之alpha1版】日期：2019-12-18  ~~~ 2020-12-25。已经完成内测。

## 【1.0雏形版之alpha1版】日期：2019-12-25  ~~~ 2020-01-25。已经完成公测。

## 【1.0雏形版之beta1版】已经完成发布。** 此版本默认开启verbose。 **  日期：2020-01-25  ~~~ 2020-02-24。

## 【2.0版之alphi1版】已经完成发布。   日期：2020-02-24  ~~~ 2020-03-03。

## 【2.0版之beta1版】,【2.0版之beta2版】已经完成发布。   日期：2020-03-04  ~~~ 2020-04-15。
** 此版本默认开启verbose。 **

此版开发代号为：【穿云箭】。寓意：一支穿云箭，千万被控机，从斧头帮；）赶来相见。特色是增加了【多线程】运行任务的功能。

## 【2.1版之rc1版】。已经完成发布。 日期：2020-04-01 ~~~ 2020-04-15。

开发代号为=【winux俑】 。寓意：呼win唤linux撒node成俑。高可用的“主”主控机为【嬴政】，高可用的“备”主控机为【赢扶苏】，被控机为【俑】，被“我”驱策亿万年！



## 【3.0版之alphi1版】未发布。** 此版本默认关闭verbose。 **  日期：2021-03-01  ~~~ 2021-12-15。

此版开发代号为：【变种猫caiiit教授的，脑波强化机】。

寓意：变种猫caiiit教授，一旦使用【kasini3000牌】的脑波强化机。即可立即控制世界上的所有变种动物，继而控制动物世界 ^_* 。


## 围观详细项目进度：日期，版本，近期新增功能：  https://gitee.com/chuanjiao10/kasini3000/wikis/news

------

# 联系

## 问：技术支持，如何收费？

答：

购买qq群技术支持点卡。一次半小时，50元。

## 卡死你3000 官方技术支持群 

qq群：700816263

## 想要：

* 技术支持 ---> qq群，收费。请提供详细问题，源码，抓图。谢谢。

* 奉献代码 ---> 去gitee.com提交代码。

* 提交bug ---> 去gitee.com中的issues，提交bug。

* 提交新増功能需求 ---> 去gitee.com中的issues，提交新増功能需求。

* 帮忙翻译 ---> qq群

* 帮忙销售 ---> qq群



------

# License  本项目开源许可证=自定义

《卡死你3000》或《kaiiit家的饭店》是【powershell传教士】开发的软件作品。特在此声明软件著作权。

卡死你3000 官方技术支持群 qq群：700816263

禁止黑客使用使用本项目代码，禁止用本项目代码作恶！

It is forbidden to use project code for hackers!

As a software engineer, we must abide by certain ethical guidelines.When I learned that my code was being used for what I personally considered to be evil, I felt obligated to prevent this from happening.

Do not use code to do evil!


问：他人修改源码后，是否可以闭源？

答：

只允许交过钱的云厂家，oem厂家，odm厂家，经过我的允许后，可以闭源。

Q: After someone else modifies the source code, can it be closed source?

A: Cloud manufacturers, oem manufacturers, odm manufacturers that only allow money to be paid, can be closed after my permission.

问：是否需要署名作者？项目url？License 目录/文件？

答：

全都是。只允许交过钱的云厂家，oem厂家，odm厂家，经过我的允许后，可以移除License。

Q: Do you need a signed author? Project url? License directory/file?

A: All are. Cloud manufacturers, oem manufacturers, odm manufacturers that only allow money to be paid, can remove the license file after my permission.



## 关于软件的收费，和生存。（下为我设计的软件生存草案，欢迎拍砖，让我好修改，完善）

问：这是什么软件？

答：

这是收费共享软件。我称之为打赏软件。意为赏我仨瓜俩枣，别让我白忙活。

本软件打算接受捐助，但是捐助码、捐助方式还没弄好。

kasini3000 is Paid application.


问：软件如何收费？

答：

类似于nginx，和nginx plus，的区别。

你所看到的是【卡死你3000】，或者叫做【kasini3000 community】。本软件的主要功能（占90%左右），主要模块=开源免费。

而主要功能之外的功能，收费。

【卡死你3000 plus】。兼容老旧系统，兼容老旧ps版本，的部分脚本，模块收费。



问：如何对公司收费？

答：

company:

one kasini3000 core +

<200agent:free use

<500agent: 10Dollars /year.

<1000agent: 20Dollars /year.

Non-profit organization and edu:

free use.




问：如何对云服务商收费？

答：

Cloud computing company:

all kasini3000 core + all agent:1000＄---20000＄ /year

Donate codes to the project and get a discounted price.


问：如何对项目集成商（oem/odm）收费？

答：

跟群主联系， qq群：700816263





## 关于pr，补丁，源码等的提交。

假如你提交代码，帮助完善软件，将视为【向我免费我捐赠源码】，我有权将你捐赠的源码闭源。

If you submit the code, it will be considered a donation code to me.

All code is private to me, I have the right to hold your code closed source.



## 本软件 License 之辩。

 ![image](/licenses/license01.jpg)

 ![image](/licenses/license02.png)

* 我不懂 mit，apache，gpl，看着头大。

* 本软件的 License 不同于 mit，apache，gpl。

* 根据本软件的第一条要求，不能黑客，不能作恶。mit，apache，gpl，就没有。

* 本软件对所有云厂家，全都收费，有专门条款。 mit，apache，gpl，也没有。


某看官道：“你一行代码也没放出，却先放出严格的自定义 License，你是不是疯了？ ”

我答：

1 先要求你必须同意协议，才给你软件，这并不奇怪。大多数软件安装前，都要求同意协议。

2 软件的协议严格，甚至严苛，并不奇怪。例如：换脸软件“zao”的协议。

3 严苛的自定义License有很多，很常见，你却必须接受。比如“中介租房合同”。

4 我的协议不严格，也不严苛。

问：可以将道德条款纳入开源许可证吗？

欢迎围观：

https://www.oschina.net/news/110177/can-moral-terms-be-included-in-an-open-source-license

------

# 免责声明 Disclaimer

使用本软件前，用户应该做好充分测试。

本软件对用户造成的任何后果、损失，概不负责。

不要把你的文件保存在【kasini3000项目目录】中，卡死你3000后台更新代码可能会删除那里的文件。

卡死你会覆盖你linux上的，旧的ssh公钥。

被控机账户密码在主控机上明文保存。用户自己应该保证主控机安全。

一旦主控机被黑客控制，则所有被控机将被黑客控制。

Users should do their own tests before using them.

The consequences are not responsible!

Do not save your files in kasini3000 folder, kasini3000 will delete your files when bg update or sync by git.

kasini3000 will overwrite your old ssh pub key file on linux node. ---> /root/.ssh/authorized_keys

The node machine account password is stored in plain text on the master machine.

The user should ensure the security on the master machine by himself.

Once the master machine is controlled by the hacker, all the node machines will be controlled by the hacker.

