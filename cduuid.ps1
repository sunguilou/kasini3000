﻿#建议保存编码为：bom头 + utf8

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$被控机uuid
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$global:当前被控机_ip = $null
$global:当前被控机_组 = $null
$global:当前被控机_uuid = $null
$global:当前被控机_os = $null
$global:当前被控机_esxi宿主机 = $null

& 'zd只读nodelist文件.ps1'

$当前被控机 = $global:所有被控机 | Where-Object { $_.被控机uuid -eq $被控机uuid }
if ($当前被控机.被控机uuid -ne $被控机uuid)
{
	Write-Error "nodelist文件中找不到这个uuid： ${被控机uuid}"
	$global:当前被控机_uuid = @{被控机uuid = '错误：nodelist文件中找不到这个uuid' }
	function global:prompt
	{
		"`e[91m`e[44m【{0}】`e[0m{1}> " -f $global:当前被控机_uuid.被控机uuid,$PWD
	}
	exit 11
}

$global:当前被控机_uuid = $当前被控机
$当前被控机 | Format-List
function global:prompt
{
	"`e[37m`e[44m【{0}】`e[0m{1}>" -f ${global:当前被控机_uuid}.被控机uuid,$PWD
}

exit 0
