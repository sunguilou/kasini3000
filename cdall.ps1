﻿#建议保存编码为：bom头 + utf8

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$global:当前被控机_ip = $null
$global:当前被控机_组 = $null
$global:当前被控机_uuid = $null
$global:当前被控机_os = $null
$global:当前被控机_esxi宿主机 = $null
#没有【所有被控机】相关变量

$global:当前被控机_os_所有开放端口未知 = $global:所有被控机 | Where-Object { $_.被控机os类型 -match '端口未知' }
Write-Warning 'nodelist文件列表中，下列【被控机】，开放端口未知：'
$global:当前被控机_os_所有开放端口未知.被控机显示名


$global:当前被控机_os_所有win版本未知 = $global:所有被控机 | Where-Object { $_.被控机os类型 -eq '【被控机】win版本未知' }
Write-Warning 'nodelist文件列表中，下列【被控机】，win版本未知：'
$global:当前被控机_os_所有win版本未知.被控机显示名


$global:当前被控机_os_所有linux发行版未知 = $global:所有被控机 | Where-Object { $_.被控机os类型 -eq '【被控机】linux发行版未知' }
Write-Warning 'nodelist文件列表中，下列【被控机】，linux发行版版本未知：'
$global:当前被控机_os_所有linux发行版未知.被控机显示名


function global:prompt
{
	"`e[37m`e[44m【所有被控机】`e[0m" + $PWD + '> '
}

& 'lsnode.ps1'
exit 0
