﻿#建议保存编码为：bom头 + utf8

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	Write-Error "不支持从linux【主控机】，到win【被控机】"
	exit 11
}



& 'zd只读nodelist文件.ps1'

foreach ($private:temp1 in $global:所有被控机)
{
	[System.Net.IPAddress]$ip2 = 0
	if (-not ( [System.Net.IPAddress]::TryParse($private:temp1.ip,[ref]$ip2) ) )
	{
		Write-Error ("错误：nodelist文件中找到这个ip地址：【{0}】但ip不合法，跳过此被控机" -f ${private:temp1}.ip)
		continue
	}

	$win = 'win7','win8','win10','win2008r2','win2012r2','win2016','win2019'
	if ( $private:temp1.被控机os类型 -notin $win)
	{
		Write-Warning ("警告：此被控机：【{0}】不是win操作系统，跳过此被控机" -f ${private:temp1}.ip)
		continue
	}

	if ($private:temp1.密码写入节点时间戳 -ne '')
	{
		$最早改密码时间 = Get-Date $private:temp1.密码写入节点时间戳
		$最早改密码时间
		if ( (Get-Date) -lt $最早改密码时间)
		{
			Write-Warning ("警告：被控机【{0}】时间戳未到改密周期，跳过此被控机" -f ${private:temp1}.ip )
			continue
		}
	}

	#改密码
	if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
	{
		if ($private:temp1.端口 -eq '')
		{
			$private:temp1.端口 = 5985
		}

		Write-Verbose ("开始在win2win被控机【{0}】上执行，密码修改操作" -f $private:temp1.ip)
		try
		{
			Test-WSMan -ComputerName $private:temp1.ip -Port $private:temp1.端口 -ErrorAction stop
		}
		catch
		{
			Write-Error ("错误：被控机【{0}】端口不通" -f ${private:temp1}.ip )
			continue
		}
		finally
		{

		}

		& 'zd只读nodelist文件.ps1'
		$当前被控机 = $global:所有被控机 | Where-Object { $_.ip -eq ${private:temp1}.ip }
		if ($当前被控机.ip -ne ${private:temp1}.ip)
		{
			Write-Error "错误：当前被控机获取失败 ${当前被控机}"
			continue
		}

		$PSRemoting服务器用户名 = $当前被控机.用户名
		$用户名2 = "$(${private:temp1}.ip)\${PSRemoting服务器用户名}"
		$密码明文 = $当前被控机.当前密码
		$密码密文 = ConvertTo-SecureString $密码明文 -AsPlainText -Force
		$用户名和密码捆绑后的授权信息 = New-Object System.Management.Automation.PSCredential ($用户名2,$密码密文)

		Write-Verbose ("使用当前密码，在被控机【{0}】上连接w2w5985开始" -f ${private:temp1}.ip )
		$private:连接31 = New-PSSession -ComputerName $private:temp1.ip -Port $private:temp1.端口 -Credential $用户名和密码捆绑后的授权信息
		if ($private:连接31 -eq $null)
		{
			Write-Error ("使用当前密码，在被控机【{0}】上连接w2w5985失败" -f ${private:temp1}.ip )
			continue
		}

		Write-Verbose '使用当前密码，连接成功。现在开始执行命令：'
		[scriptblock]$private:cmd = { net user $args[0] $args[1] }
		Invoke-Command -session $private:连接31 -ScriptBlock $private:cmd -ArgumentList $当前被控机.用户名,$当前被控机.旧密码
		$private:连接31 | Remove-PSSession
		Start-Sleep -Seconds 2


		#验证
		$PSRemoting服务器用户名 = $当前被控机.用户名
		$用户名2 = "${目的ip地址}\${PSRemoting服务器用户名}"
		$密码明文 = $当前被控机.旧密码
		$密码密文 = ConvertTo-SecureString $密码明文 -AsPlainText -Force
		$用户名和密码捆绑后的授权信息 = New-Object System.Management.Automation.PSCredential ($用户名2,$密码密文)

		Write-Verbose '使用旧密码，连接w2w5985开始'
		$private:连接32 = New-PSSession -ComputerName $private:temp1.ip -Port $private:temp1.端口 -Credential $用户名和密码捆绑后的授权信息
		if ($private:连接32 -eq $null)
		{
			Write-Error "使用旧密码，在被控机上连接w2w5985失败，密码修改失败"
		}
		else
		{
			#密码对调
			$private:连接32 | Remove-PSSession
			$private:temp993 = $private:temp1.当前密码
			$private:temp1.当前密码 = $private:temp1.旧密码
			$private:temp1.旧密码 = $private:temp993

			#写入时间戳
			$private:temp1.密码写入节点时间戳 = Get-Date ((Get-Date) + $global:被控机密码_变更周期) -Format 'F'
			Write-Host -ForegroundColor green ("被控机【{0}】win2win密码修改成功。" -f $private:temp1.ip )
		}
	}
}

& 'zl整理写入nodelist文件.ps1'

exit 0
