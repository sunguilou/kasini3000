﻿#建议保存编码为：bom头 + utf8
#判断有秘钥0后，删除秘钥2，生成秘钥1。

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	if (Test-Path -LiteralPath "$env:USERPROFILE\.ssh\id_rsa")
	{
		Write-Error "错误，已有秘钥1"
		exit 1
	}

	& 'read-host+timeout_v2.1.ps1' -变量名 '【ssh私钥密码】' -变量值 'y'
	
	switch (${global:【ssh私钥密码】})
	{
		'y'
		{
			$a = @"
ssh-keygen -t rsa -b 4096 -N "" -f "$env:USERPROFILE\.ssh\id_rsa"
"@

			cmd.exe /c $a
			break
		}

		'n'
		{
			$b = @"
ssh-keygen -t rsa -b 4096  -f "$env:USERPROFILE\.ssh\id_rsa"
"@

			cmd.exe /c $b
			break
		}

		default
		{
			Write-Error 'y/n 输入错误，使用默认值 "n" ！'
			$a = @"
ssh-keygen -t rsa -b 4096 -N "" -f "$env:USERPROFILE\.ssh\id_rsa"
"@

			cmd.exe /c $a
		}
	}
}


if ($IsLinux -eq $True)
{
	if (Test-Path -LiteralPath '/root/.ssh/id_rsa')
	{
		Write-Error "错误，已有秘钥1"
		exit 1
	}

	& 'read-host+timeout_v2.1.ps1' -变量名 '【ssh私钥密码】' -变量值 'y'
	switch (${【ssh私钥密码】})
	{
		'y'
		{

@'
ssh-keygen -t rsa -b 4096 -N '' -f '/root/.ssh/id_rsa'
'@ | /usr/bin/bash

			break
		}

		'n'
		{
		
@'
ssh-keygen -t rsa -b 4096  -f '/root/.ssh/id_rsa'
'@ | /usr/bin/bash

			break
		}

		default
		{
			Write-Error 'y/n 输入错误，使用默认值 "n" ！'

@'
ssh-keygen -t rsa -b 4096 -N '' -f '/root/.ssh/id_rsa'
'@ | /usr/bin/bash

		}
	}

chmod 600 /etc/kasini3000/ssh_key_files_old1/*
}

& 'gx更新主控机上的_双公钥文件authorized_keys.ps1'

exit 0
