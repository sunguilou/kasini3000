﻿#建议保存编码为：bom头 + utf8

param
(
	[String]$目的ip地址,
	$端口,
	[String]$LiteralPath,
	[String]$Path,
	[String]$Destination,
	[Switch]$Recurse
)


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

Write-Verbose '开始在 被控机os 复制'

if ($global:当前被控机_os -eq $null)
{
	Write-Error "${global:当前被控机_os} 为空"
	exit 1
}

foreach ($当前被控机4 in $global:当前被控机_os)
{
	$global:当前被控机_ip = $当前被控机4
	& 'k_copyto_ip.ps1' -目的ip地址 $global:当前被控机_ip.ip -端口 $端口 -LiteralPath $LiteralPath -Path $Path -Destination $Destination -Recurse $Recurse
}

Write-Verbose '结束在 被控机os 复制'

exit 0
