﻿#建议保存编码为：bom头 + utf8

param
(
	[String]$目的ip地址 = $global:当前被控机_ip.ip,
	$端口 = $global:当前被控机_ip.端口
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

Write-Host -ForegroundColor green '正在从【主控机】，进入到【被控机终端】环境。。。'

if ($global:当前被控机_ip -eq $null)
{
	Write-Error "错误：找不到被控机。`n被控机 ${global:当前被控机_ip} 为空。请先运行cdip"
	exit 1
}

if ($global:当前被控机_ip.length -gt 1)
{
	Write-Error "错误：找不到被控机。`n被控机 ${global:当前被控机_ip} 数量太多。请先运行cdip"
	exit 2
}

if ($global:上次gtbkj建立_但没关闭的session.length -gt 0)
{
	$global:上次gtbkj建立_但没关闭的session | Remove-PSSession
	$global:上次gtbkj建立_但没关闭的session = @()
}
$global:上次gtbkj建立_但没关闭的session = @()

$win = 'win7','win8','win10','win2008r2','win2012r2','win2016','win2019'
$linux = 'centos7','centos8','debian8','debian9','debian10','ubuntu1404','ubuntu1604','ubuntu1804','alpine','ubuntu2004'

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	if ( $global:当前被控机_ip.被控机os类型 -in $win)
	{
		if ( ($端口 -eq '') -or ($端口 -eq $null) )
		{
			$端口 = 5985
		}
		try
		{
			Test-WSMan -ComputerName ${目的ip地址} -Port $端口 -ErrorAction stop
		}
		catch
		{
			Write-Error	"目的ip地址【${目的ip地址}】端口不通"
			exit 12
		}
		finally
		{

		}

		& 'zd只读nodelist文件.ps1'
		$当前被控机 = $global:所有被控机 | Where-Object { $_.ip -eq $目的ip地址 }
		if ($当前被控机.ip -ne $目的ip地址)
		{
			Write-Error "错误：当前被控机获取失败 ${当前被控机}"
			exit 13
		}

		$PSRemoting服务器用户名 = $当前被控机.用户名
		$用户名2 = "${目的ip地址}\${PSRemoting服务器用户名}"
		$密码明文 = $当前被控机.当前密码
		$密码密文 = ConvertTo-SecureString $密码明文 -AsPlainText -Force
		$用户名和密码捆绑后的授权信息 = New-Object System.Management.Automation.PSCredential ($用户名2,$密码密文)

		Write-Verbose '使用密码，连接win2win5985开始'
		$private:连接11 = New-PSSession -ComputerName $目的ip地址 -Port $端口 -Credential $用户名和密码捆绑后的授权信息
		if ($private:连接11 -eq $null)
		{
			Write-Error "使用密码，在${目的ip地址}上连接win2win5985失败"
			exit 21
		}
		Invoke-Command -session $private:连接11 -ScriptBlock { & 'c:\ProgramData\kasini3000\node_script\kasini3000\tj在被控机添加path.ps1' }
		Enter-PSSession -session $private:连接11
		$global:上次gtbkj建立_但没关闭的session = $global:上次gtbkj建立_但没关闭的session + $private:连接11
	}

	if ( $global:当前被控机_ip.被控机os类型 -in $linux)
	{
		if ( $PSVersionTable.psversion.major -ge 6)
		{

		}
		else
		{
			Write-Error "在win中连接linux，依赖powershell 6"
			exit 12
		}

		if ( ($端口 -eq '') -or ($端口 -eq $null) )
		{
			$端口 = 22
		}

		Write-Verbose '使用ssh秘钥1，连接win2linux开始'
		[string]$private:temp011 = ssh.exe -l root -i "$env:USERPROFILE\.ssh\id_rsa"  ${目的ip地址} -p $端口 -o PasswordAuthentication=no 'date' *>&1
		Write-Verbose $private:temp011
		if ( $private:temp011.ToLower().Contains('Permission denied'.ToLower()) )
		{
			$秘钥1连接成功 = $false
			Write-Verbose "使用ssh秘钥1，在${目的ip地址}上连接win2linux失败"
		}
		else
		{
			$秘钥1连接成功 = $true
		}

		if ( $private:temp011.ToLower().Contains('timed out'.ToLower()) )
		{
			Write-Error	 "使用ssh秘钥1，在${目的ip地址}上win2linux连接超时"
			exit 13
		}

		if ($秘钥1连接成功 -eq $true)
		{
			$private:连接21 = New-PSSession -HostName ${目的ip地址} -Port $端口 -UserName 'root' -KeyFilePath "$env:USERPROFILE\.ssh\id_rsa"
			Invoke-Command -session $private:连接21 -ScriptBlock { & '/etc/kasini3000/node_script/kasini3000/tj在被控机添加path.ps1' }
			Enter-PSSession -session $private:连接21
			$global:上次gtbkj建立_但没关闭的session = $global:上次gtbkj建立_但没关闭的session + $private:连接21
		}
		else
		{
			Write-Verbose '使用ssh秘钥2，连接win2linux开始'
			[string]$private:temp012 = ssh.exe -l root -i "${global:kasini3000目录}\ssh_key_files_old1\id_rsa"  ${目的ip地址} -p $端口 -o PasswordAuthentication=no 'date' *>&1
			Write-Verbose $private:temp012
			if ( $private:temp012.ToLower().Contains('Permission denied'.ToLower()) )
			{
				Write-Error "使用ssh秘钥2，在${目的ip地址}上连接win2linux失败"
				exit 15
			}

			$private:连接22 = New-PSSession -HostName ${目的ip地址} -Port $端口 -UserName 'root' -KeyFilePath "${global:kasini3000目录}\ssh_key_files_old1\id_rsa"
			Invoke-Command -session $private:连接22 -ScriptBlock { & '/etc/kasini3000/node_script/kasini3000/tj在被控机添加path.ps1' }
			Enter-PSSession -session $private:连接22
			$global:上次gtbkj建立_但没关闭的session = $global:上次gtbkj建立_但没关闭的session + $private:连接22
		}
	}
}

if ($IsLinux -eq $True)
{
	if ( $global:当前被控机_ip.被控机os类型 -in $win)
	{
		Write-Error "无法从【linux主控机】复制文件到【win被控机】"
		exit 1
	}

	if ( $global:当前被控机_ip.被控机os类型 -in $linux)
	{
		if ( ($端口 -eq '') -or ($端口 -eq $null) )
		{
			$端口 = 22
		}

		Write-Verbose '使用ssh秘钥1，连接linux2linux开始'
		[string]$private:temp011 = ssh -l root -i '/root/.ssh/id_rsa'  ${目的ip地址} -p $端口 -o PasswordAuthentication=no 'date' *>&1
		Write-Verbose $private:temp011
		if ( $private:temp011.ToLower().Contains('Permission denied'.ToLower()) )
		{
			$秘钥1连接成功 = $false
			Write-Verbose "使用ssh秘钥1，在${目的ip地址}上连接linux2linux失败"
		}
		else
		{
			$秘钥1连接成功 = $true
		}

		if ( $private:temp011.ToLower().Contains('timed out'.ToLower()) )
		{
			Write-Error	 "使用ssh秘钥1，在${目的ip地址}上连接linux2linux超时"
			exit 13
		}

		if ($秘钥1连接成功 -eq $true)
		{
			$private:连接21 = New-PSSession -HostName ${目的ip地址} -Port $端口 -UserName 'root' -KeyFilePath '/root/.ssh/id_rsa'
			Invoke-Command -session $private:连接21 -ScriptBlock { & '/etc/kasini3000/node_script/kasini3000/tj在被控机添加path.ps1' }
			Enter-PSSession -session $private:连接21
			$global:上次gtbkj建立_但没关闭的session = $global:上次gtbkj建立_但没关闭的session + $private:连接21
		}
		else
		{
			Write-Verbose '使用ssh秘钥2，连接linux2linux开始'
			[string]$private:temp012 = ssh -l root -i "${global:kasini3000目录}/ssh_key_files_old1/id_rsa"  ${目的ip地址} -p $端口 -o PasswordAuthentication=no 'date' *>&1
			Write-Verbose $private:temp012
			if ( $private:temp012.ToLower().Contains('Permission denied'.ToLower()) )
			{
				Write-Error "使用ssh秘钥2，在${目的ip地址}上连接linux2linux失败"
				exit 15
			}

			$private:连接22 = New-PSSession -HostName ${目的ip地址} -Port $端口 -UserName 'root' -KeyFilePath "${global:kasini3000目录}/ssh_key_files_old1/id_rsa"
			Invoke-Command -session $private:连接22 -ScriptBlock { & '/etc/kasini3000/node_script/kasini3000/tj在被控机添加path.ps1' }
			Enter-PSSession -session $private:连接22
			$global:上次gtbkj建立_但没关闭的session = $global:上次gtbkj建立_但没关闭的session + $private:连接22
		}
	}
}

Write-Host -ForegroundColor green '您已经从【主控机】，进入到【被控机终端】。。。键入exit可以退出。请注意：下面显示的，是被控机的目录路径。'

exit 0
