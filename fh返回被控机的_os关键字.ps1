﻿#建议保存编码为：bom头 + utf8
param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$目的ip地址
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$private:当前被控机 = $global:所有被控机 | Where-Object { $_.ip -eq $目的ip地址 }
if ($private:当前被控机 -eq $null)
{
	return '错误：nodelist文件中找不到这个ip地址'
	exit 1
}

[string]$private:被控机的端口信息是 = & 'fh返回被控机的_开放端口信息.ps1' -目的ip地址 $private:当前被控机.ip -目的端口 $private:当前被控机.端口
Write-Verbose ("被控机【{0}】的端口关键字是：【{1}】" -f ${private:当前被控机}.被控机显示名,${private:被控机的端口信息是} )
if ( $private:被控机的端口信息是.Contains('未知') -or $private:被控机的端口信息是.Contains('不通') )
{
	return '【被控机】开放端口未知！'
	exit 2 #这句其实没用
}

if ( $private:被控机的端口信息是.Contains('通了') )
{
	return '自定义端口，请联系开发者'
	exit 3 #这句其实没用
}

$private:linux版本字串 = 'centos8','centos7','centos6','debian8','debian9','debian10','ubuntu1404','ubuntu1604','ubuntu1804','centos8','alpine','ubuntu2004'

[scriptblock]$private:判断linux发行版 =
{
	if (Select-String -Pattern 'centos-8' -Path '/etc/*-release' -SimpleMatch -Quiet)
	{
		return 'centos8'
	}

	if (Select-String -Pattern 'centos-7' -Path '/etc/*-release' -SimpleMatch -Quiet)
	{
		return 'centos7'
	}

	if (Select-String -Pattern 'centos' -Path '/etc/*-release' -SimpleMatch -Quiet)
	{
		return 'centos6'
	}

	if (Select-String -Pattern 'jessie' -Path '/etc/*-release' -SimpleMatch -Quiet)
	{
		return 'debian8'
	}

	if (Select-String -Pattern 'stretch' -Path '/etc/*-release' -SimpleMatch -Quiet)
	{
		return 'debian9'
	}

	if (Select-String -Pattern 'buster' -Path '/etc/*-release' -SimpleMatch -Quiet)
	{
		return 'debian10'
	}

	if (Select-String -Pattern 'Trusty Tahr' -Path '/etc/*-release' -SimpleMatch -Quiet)
	{
		return 'ubuntu1404'
	}

	if (Select-String -Pattern 'Xenial Xerus' -Path '/etc/*-release' -SimpleMatch -Quiet)
	{
		return 'ubuntu1604'
	}

	if (Select-String -Pattern 'Bionic Beaver' -Path '/etc/*-release' -SimpleMatch -Quiet)
	{
		return 'ubuntu1804'
	}

	if (Select-String -Pattern 'Focal Fossa' -Path '/etc/*-release' -SimpleMatch -Quiet)
	{
		return 'ubuntu2004'
	}

	if (Select-String -Pattern 'Alpine' -Path '/etc/*-release' -SimpleMatch -Quiet)
	{
		return 'Alpine'
	}
}

$private:win版本字串 = 'win2008r2','win7','win8','win10','win2012r2','win2016','win2019'

[scriptblock]$private:判断win版本 =
{
	if ( (Get-WmiObject -Class Win32_OperatingSystem).Caption -like "*2008r2*" )
	{
		return 'win2008r2'
	}

	if ( (Get-WmiObject -Class Win32_OperatingSystem).Caption -like "*7*" )
	{
		return 'win7'
	}

	if ( (Get-WmiObject -Class Win32_OperatingSystem).Caption -like "*8*" )
	{
		return 'win8'
	}

	if ( (Get-WmiObject -Class Win32_OperatingSystem).Caption -like "*10*" )
	{
		return 'win10'
	}

	if ( (Get-WmiObject -Class Win32_OperatingSystem).Caption -like "*2012 r2*" )
	{
		return 'win2012r2'
	}

	if ( (Get-WmiObject -Class Win32_OperatingSystem).Caption -like "*2016*" )
	{
		return 'win2016'
	}

	if ( (Get-WmiObject -Class Win32_OperatingSystem).Caption -like "*2019*" )
	{
		return 'win2019'
	}
}


if ($IsLinux -eq $True)
{
	if ( ($private:被控机的端口信息是 -eq 'win5985') -or ($private:被控机的端口信息是 -eq 'win5986') )
	{
		return "从linux【主控机】中，经winrm协议，控制win【被控机】，无法实现！"
		exit 4  #这句其实没用
	}

	if ($private:被控机的端口信息是 -eq 'win135')
	{
		return "从linux【主控机】中，经wmic协议+135端口，控制win【被控机】，无法实现！"
		exit 5 #这句其实没用
	}

	if ($private:被控机的端口信息是 -eq 'linux')
	{
		$l2l = & 'run_linux2linux_key_pwd.ps1' -目的ip地址 $目的ip地址 -powershell代码块 $private:判断linux发行版
		if ($l2l -in $private:linux版本字串)
		{
			return $l2l
		}
		elseif ($l2l[-1] -in $private:linux版本字串)
		{
			return $l2l[-1]
		}
		else
		{
			return '【被控机】linux发行版未知'
		}
	}
}

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	if ($private:被控机的端口信息是 -eq 'win5985')
	{
		$w2w = & 'run_win2win5985_pwd.ps1' -目的ip地址 $目的ip地址 -powershell代码块 $private:判断win版本
		if ($w2w -in $private:win版本字串)
		{
			return $w2w
		}
		elseif ($w2w[-1] -in $private:win版本字串)
		{
			return $w2w[-1]
		}
		else
		{
			return '【被控机】win版本未知'
		}
	}

	if ($private:被控机的端口信息是 -eq 'win5986')
	{
		$w2w = & 'run_win2win5986_pwd.ps1' -目的ip地址 $目的ip地址  -端口 5986 -powershell代码块 $private:判断win版本
		if ($w2w -in $private:win版本字串)
		{
			return $w2w
		}
		elseif ($w2w[-1] -in $private:win版本字串)
		{
			return $w2w[-1]
		}
		else
		{
			return '【被控机】win版本未知'
		}
	}

	if ($private:被控机的端口信息是 -eq 'win135')
	{
		return "从win【主控机】中，经wmic协议+135端口，控制win【被控机】，无法实现！"
		exit 5 #这句其实没用
	}

	if ($private:被控机的端口信息是 -eq 'linux')
	{
		$w2l = & 'run_win2linux_key_pwd.ps1' -目的ip地址 $目的ip地址 -powershell代码块 $private:判断linux发行版
		if ($w2l -in $private:linux版本字串)
		{
			return $w2l
		}
		elseif ($w2l[-1] -in $private:linux版本字串)
		{
			return $w2l[-1]
		}
		else
		{
			return '【被控机】linux发行版未知'
		}
	}
}

exit 0

