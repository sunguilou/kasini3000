﻿#建议保存编码为：bom头 + utf8


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	$key文件1 = "$env:USERPROFILE\.ssh\id_rsa.pub"
	if (Test-Path -LiteralPath $key文件1)
	{

	}
	else
	{
		Write-Error "错误，找不到秘钥1。应该先运行【jl建立主控机ssh秘钥1.ps1】"
		exit 1
	}

	if (Test-Path -LiteralPath "${global:kasini3000目录}\ssh_key_files_old1")
	{

	}
	else
	{
		Write-Warning "错误，找不到密钥2路径"
		mkdir -Name "${global:kasini3000目录}\ssh_key_files_old1"
	}

	$win输出公钥 = "${global:kasini3000目录}\ssh_key_files_old1\authorized_keys"
	Copy-Item -LiteralPath $key文件1  -Destination $win输出公钥 -Force

	$key文件2 = "${global:kasini3000目录}\ssh_key_files_old1\id_rsa.pub"
	if (Test-Path -LiteralPath $key文件2)
	{
		[string]$key1_string = Get-Content -LiteralPath $win输出公钥 -raw
		[string]$key2_string = Get-Content -LiteralPath $key文件2 -raw
		[string]$合并后_string = $key1_string + $key2_string
		Set-Content -LiteralPath  $win输出公钥 -Value $合并后_string -Encoding ascii
		Write-Host "制作出了双key公钥文件" -ForegroundColor Green
	}
	else
	{
		Write-Warning "警告，找不到密钥2路径，只制作出了单key公钥文件"
	}
}


if ($IsLinux -eq $True)
{
	$key文件1 = "/root/.ssh/id_rsa.pub"
	if (Test-Path -LiteralPath $key文件1)
	{

	}
	else
	{
		Write-Error "错误，找不到秘钥1。应该先运行【jl建立主控机ssh秘钥1.ps1】"
		exit 1
	}

	if (Test-Path -LiteralPath "${global:kasini3000目录}/ssh_key_files_old1/")
	{

	}
	else
	{
		Write-Warning "错误，找不到密钥2路径"
		mkdir -Name "${global:kasini3000目录}/ssh_key_files_old1/"
	}

	$linux输出公钥 = "${global:kasini3000目录}/ssh_key_files_old1/authorized_keys"
	Copy-Item -LiteralPath $key文件1  -Destination $linux输出公钥 -Force

	$key文件2 = "${global:kasini3000目录}/ssh_key_files_old1/id_rsa.pub"
	if (Test-Path -LiteralPath $key文件2)
	{
		[string]$key1_string = Get-Content -LiteralPath $linux输出公钥 -raw
		[string]$key2_string = Get-Content -LiteralPath $key文件2 -raw
		[string]$合并后_string = $key1_string + $key2_string
		Set-Content -LiteralPath  $linux输出公钥 -Value $合并后_string -Encoding ascii
		Write-Host "制作出了双key公钥文件" -ForegroundColor Green
	}
	else
	{
		Write-Warning "警告，找不到密钥2路径，只制作出了单key公钥文件"
	}
	chmod 600 '/root/.ssh/id_rsa'
	chmod 600 '/root/.ssh/id_rsa.pub'
	chmod 600 "/etc/kasini3000/ssh_key_files_old1/*"
}
exit 0
