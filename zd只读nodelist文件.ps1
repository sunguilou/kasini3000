﻿#建议保存编码为：bom头 + utf8

if ($global:zd只读nodelist文件 -eq $true)
{
	exit
}
$global:zd只读nodelist文件 = $true


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
	if (Test-Path -LiteralPath $global:被控机列表文件)
	{

	}
	else
	{
		Write-Error "错误，找不到被控机列表文件 $global:被控机列表文件 。`n请按照示例文件 ‘c:\ProgramData\kasini3000\docs\examples\nodelist.csv’ `n请按照示例文件 ‘c:\ProgramData\kasini3000\docs\examples\nodelist.xlsx’ `n生成被控机列表文件‘c:\ProgramData\kasini3000\nodelist.csv’"
		Copy-Item -LiteralPath 'c:\ProgramData\kasini3000\docs\examples\nodelist.csv' -Destination 'c:\ProgramData\kasini3000\nodelist.csv'
		Start-Sleep -Seconds 1
		& C:\windows\system32\notepad.exe 'c:\ProgramData\kasini3000\nodelist.csv'
		exit 1
	}
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
	if (Test-Path -LiteralPath $global:被控机列表文件)
	{

	}
	else
	{
		Write-Error "错误，找不到被控机列表文件 $global:被控机列表文件 。`n请按照示例文件 ‘/etc/kasini3000/docs/examples/nodelist.csv’ `n请按照示例文件 ‘/etc/kasini3000/docs/examples/nodelist.xlsx’ `n生成被控机列表文件‘/etc/kasini3000/nodelist.csv’"
		Write-Host -ForegroundColor Green  'cp /etc/kasini3000/docs/examples/nodelist.csv /etc/kasini3000/nodelist.csv; vi /etc/kasini3000/nodelist.csv'
		exit 1
	}
}



function 有bom头吗 ($输入文件全路径2)
{
	if ( ($IsLinux -eq $True) -or ($PSVersionTable.psversion.major -ge 6) )
	{
		$头 = Get-Content -LiteralPath $输入文件全路径2 -AsByteStream -TotalCount 4
	}
	else
	{
		$头 = Get-Content -LiteralPath $输入文件全路径2 -Encoding Byte -TotalCount 4
	}

	[string]$utf8_BOM = "{0:X}{1:X}{2:X}" -f $头
	[string]$utf16_BOM = "{0:X}{1:X}" -f $头
	[string]$utf32_BOM = "{0:X}{1:X}{2:X}{3:X}" -f $头

	if ($utf8_BOM -eq "EFBBBF")
	{
		$script:输入文件编码2 = 65001
		$script:输出文件编码2 = 65001
		Write-Verbose "nodelist文件编码为：UTF-8"
		return $true
	}

	if ($utf16_BOM -eq "FFFE")
	{
		$script:输入文件编码2 = 1200
		$script:输出文件编码2 = 1200
		Write-Verbose "nodelist文件编码为：Unicode"
		return $true
	}

	if ($utf16_BOM -eq "FEFF")
	{
		$script:输入文件编码2 = 1201
		$script:输出文件编码2 = 1201
		Write-Verbose "nodelist文件编码为：Big Endian Unicode"
		return $true
	}

	if ($utf32_BOM -eq "0000FEFF")
	{
		$script:输入文件编码2 = 12001
		$script:输出文件编码2 = 12001
		Write-Verbose "nodelist文件编码为：UTF-32, big-endian"
		return $true
	}

	if ($utf32_BOM -eq "FFFE0000")
	{
		$script:输入文件编码2 = 12000
		$script:输出文件编码2 = 12000
		Write-Verbose "nodelist文件编码为：UTF-32, little-endian"
		return $true
	}

	return $False
}

if ( 有bom头吗 -输入文件全路径2 $global:被控机列表文件 )
{
}
else
{
	Write-Error "错误，被控机列表文件必须有bom头，文件编码格式不限，回车格式不限"
	exit 2
}

$global:所有被控机 = Import-Csv -LiteralPath $global:被控机列表文件 -ErrorAction Stop

exit 0
