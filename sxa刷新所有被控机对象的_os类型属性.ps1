﻿#建议保存编码为：bom头 + utf8


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

Write-Host -ForegroundColor Green "正在运行的脚本是： $PSCommandPath "

Write-Warning '正在刷新，所有被控机的【端口】、【操作系统】、【发行版类型】等项目，请等待...'
if ($PSVersionTable.psversion.major -ge 7)
{
	$ErrorView = 'NormalView'
	& 'sxa刷新所有被控机对象的_os类型属性psv7.ps1'
}
else
{
	foreach ($private:当前被控机2 in $global:所有被控机)
	{
		& 'sx1刷新单个被控机对象的_os类型属性.ps1' -被控机ip地址 $private:当前被控机2.ip
	}
}
Write-Warning '已完成刷新，所有被控机的【端口】、【操作系统】、【发行版类型】等项目。'
exit 0
