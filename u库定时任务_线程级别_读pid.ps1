﻿#建议保存编码为：bom头 + utf8
#

$log日志文件全目录 = "${global:主控机db目录}/d当前库"

if (Test-Path -LiteralPath $log日志文件全目录)
{
	$u库定时任务的pid文件 = "$log日志文件全目录/u库_线程级别_pid.txt"
	if (Test-Path -LiteralPath $u库定时任务的pid文件)
	{
		$u库pid = Get-Content -LiteralPath $u库定时任务的pid文件
		return $u库pid
	}

}
else
{
	Write-Error '找不到日志文件目录'
	exit 1
}

exit 0
