﻿#

param
(
	[byte]$最后n个输出 = 10,
	[String]$备注
)


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}


$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'invoke_福报库.ps1'
if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
{
	Write-Error "找不到 'invoke_福报库' "
	exit 1
}
else
{
	$invoke_福报库 = $private:temp999
}

if ( ($备注 -eq $null) -or ($备注 -eq '') )
{
	$sqlite_sql = @"
SELECT "被控机ip","不可重任务id","标准输出","任务状态","任务实际结束时间" FROM "福报任务表" ORDER BY "任务实际结束时间" DESC LIMIT ${最后n个输出}
;
"@
}
else
{
	$sqlite_sql = @"
SELECT "被控机ip","不可重任务id","标准输出","任务状态","任务实际结束时间" FROM "福报任务表" WHERE "备注"="${备注}" ORDER BY "任务实际结束时间" DESC LIMIT ${最后n个输出}
;
"@
}

try
{
	$结果 = & $invoke_福报库 -sqlite_sql语句 $sqlite_sql
}
catch
{
	Write-Error "错误：福报表,读取最后【${最后n个输出}】个结果输出,失败"
	exit 2
}
finally
{

}
Write-Host "福报表,读取最后【${最后n个输出}】个结果，倒序输出，成功" -ForegroundColor Green
$结果 | Format-Table  -Wrap

exit 0
