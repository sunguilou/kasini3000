﻿#建议保存编码为：bom头 + utf8

Write-Verbose '当前库2备份库 开始'


$当前库目录 = $PSScriptRoot + '/d当前库/'
if (test-path -LiteralPath $当前库目录)
{
	$当前库文件 = Get-ChildItem -LiteralPath $当前库目录 -File
	if ($当前库文件 -ne $null)
	{
		$备份库目录 = $PSScriptRoot + '/b备份库/'
		if (test-path -LiteralPath $备份库目录)
		{
			$今天日期 = get-date -format 'yyyyMMdd'
			$备份库目录加日期 = "$备份库目录/$今天日期"
			if (test-path -LiteralPath $备份库目录加日期)
			{
				Write-Error "错误，今天已经备份库"
				exit 4
			}
			else
			{
				mkdir $备份库目录加日期
				Move-Item -Path "${当前库目录}\*" -Destination $备份库目录加日期
				Write-Verbose '当前库2备份库 复制完毕'
			}
		}
		else
		{
			Write-Error "错误，找不到备份库目录"
			exit 3
		}
	}
	else
	{
		Write-Error "错误，当前库里无文件"
		exit 2
	}
}
else
{
	Write-Error "错误，找不到当前库目录"
	exit 1
}

Write-Verbose '当前库2备份库 结束'
exit 0
