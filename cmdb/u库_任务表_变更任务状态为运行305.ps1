﻿#

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[uint32]$u库任务表不可重任务id
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'invoke_用户库.ps1'
if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
{
	Write-Error "找不到 'invoke_用户库' "
	exit 1
}
else
{
	$invoke_用户库 = $private:temp999
}

$sqlite_sql =
@"
SELECT "任务状态" FROM "u任务表" WHERE "不可重任务id"=${u库任务表不可重任务id}
;
"@

try
{
	$任务状态查询结果 = & $invoke_用户库 -sqlite_sql语句 $sqlite_sql
}
catch
{
	Write-Error 'u库任务表,【查询】任务状态,失败'
	exit 2
}
finally
{

}
Write-Verbose 'u库任务表,【查询】任务状态,成功'

if ($任务状态查询结果.count -gt 1)
{
	Write-Error '错误：返回结果太多'
	exit 6
}

if ($任务状态查询结果.count -le 0)
{
	Write-Error '错误：无返回结果'
	exit 7
}
[uint16]$任务状态查询结果 = $任务状态查询结果['任务状态']
Write-Verbose "信息：当前任务状态 ${任务状态查询结果}"

switch ($任务状态查询结果)
{
	999
	{
		Write-Error 'u库任务表,变更任务状态为【运行】,失败。任务成功已经【完成】'
		exit 3
	}

	{ ($任务状态查询结果 % 3) -eq 0 } #超时状态
	{
		$新状态 = 1
		break
	}

	{ ($任务状态查询结果 % 3) -eq 1 } #运行状态
	{
		Write-Error 'u库任务表,变更任务状态为【运行】,失败。任务成功已经【运行】'
		exit 4
	}

	{ ($任务状态查询结果 % 3) -eq 2 } #出错状态
	{
		$新状态 = $任务状态查询结果 + 2
	}

	default
	{
		Write-Error '未知错误'
		exit 8
	}
}
Write-Verbose "新状态 $新状态"

$sqlite_sql =
@"
UPDATE "u任务表" SET "任务状态"=${新状态} WHERE "不可重任务id"=${u库任务表不可重任务id}
;
"@

try
{
	& $invoke_用户库  -sqlite_sql语句 $sqlite_sql
}
catch
{
	Write-Error 'u库任务表,变更任务状态为【运行】,失败'
	exit 5
}
finally
{

}

Write-Verbose 'u库任务表,变更任务状态为【运行】,成功'
exit 0

