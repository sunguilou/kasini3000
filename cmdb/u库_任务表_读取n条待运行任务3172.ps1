﻿# 返回状态为0,2,5,8,3,6,9的10条任务

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}


$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'invoke_用户库.ps1'
if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
{
	Write-Error "找不到 'invoke_用户库' "
	exit 1
}
else
{
	$invoke_用户库 = $private:temp999
}

$当前时间 = Get-Date -format 'yyyy-MM-dd HH:mm:ss'

if ($global:u库_进程级别_定时任务_每次执行任务数)
{
	$sqlite_sql = @"
SELECT "不可重任务id", "命令行", "被控机ip", "任务状态", "任务重试次数" FROM "u任务表" WHERE datetime("任务预定开始时间") <= "${当前时间}" AND "${当前时间}" <= datetime("任务预定超时时间") AND "使用新进程启动每个任务" = 1 AND "任务状态" NOT IN
(SELECT "任务状态" FROM "u任务表" WHERE "任务状态" = 998 OR "任务状态" = 999 OR "任务状态" = 1000 OR "任务状态" % 3 = 1)
LIMIT $global:u库_进程级别_定时任务_每次执行任务数
;
"@

}
else
{
	$sqlite_sql = @"
SELECT "不可重任务id", "命令行", "被控机ip", "任务状态", "任务重试次数" FROM "u任务表" WHERE datetime("任务预定开始时间") <= "${当前时间}" AND "${当前时间}" <= datetime("任务预定超时时间") AND  "使用新进程启动每个任务" = 1 "任务状态" NOT IN
(SELECT "任务状态" FROM "u任务表" WHERE "任务状态" = 998 OR "任务状态" = 999 OR "任务状态" = 1000 OR "任务状态" % 3 = 1)
LIMIT 10
;
"@

}


try
{
	& $invoke_用户库 -sqlite_sql语句 $sqlite_sql
}
catch
{
	Write-Error 'u库任务表,读取10个任务,失败'
	exit 2
}
finally
{

}
Write-Verbose 'u库任务表,读取10个任务,成功'


exit 0
