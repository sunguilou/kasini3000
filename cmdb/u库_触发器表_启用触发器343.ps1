﻿#

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[int]$id
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'invoke_用户库.ps1'
if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
{
	Write-Error "找不到 'invoke_用户库' "
	exit 1
}
else
{
	$invoke_用户库 = $private:temp999
}

$sqlite_sql =
@"
SELECT "id" FROM "u触发器表" WHERE "id"=${id}
;
"@

try
{
	$private:查询结果 = & $invoke_用户库  -sqlite_sql语句 $sqlite_sql
}
catch
{
	Write-Error '错误：执行u库，u触发器表，查询失败'
	exit 2
}
finally
{

}
Write-Verbose '执行u库，u触发器表，查询成功'

if ( ($private:查询结果 -eq $null) -or ($private:查询结果 -eq '') )
{
	Write-Error '错误：找不到u库，u触发器表，触发器id。错误码3'
	exit 3
}
else
{
	$sqlite_sql = @"
UPDATE "u触发器表" SET "启用状态"=1 WHERE "id"=${id}
;
"@

	try
	{
		& $invoke_用户库  -sqlite_sql语句 $sqlite_sql
	}
	catch
	{
		Write-Error '启用u库，u触发器表，触发器id，失败'
		exit 4
	}
	finally
	{

	}
	Write-Host '启用u库，u触发器表，触发器id，成功' -ForegroundColor Green
	exit 0
}
