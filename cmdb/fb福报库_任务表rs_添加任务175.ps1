﻿#

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$被控机ip,
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$福报命令行,
	[timespan]$任务超时时间 = [timespan]'00:10:00',
	[byte]$任务重试次数 = 1,
	[String]$备注
)
Write-Verbose '提交脚本开始'
Write-Verbose $福报命令行

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

if ($global:福报后台任务_延迟_秒)
{
	$private:任务预定开始时间 = (Get-Date).AddSeconds($global:福报后台任务_延迟_秒)
}
else
{
	$private:任务预定开始时间 = (Get-Date).AddSeconds(2)
}

$private:任务预定开始时间2 = Get-Date -format 'yyyy-MM-dd HH:mm:ss' $private:任务预定开始时间
$private:任务超时时间2 = $private:任务预定开始时间.add($任务超时时间)
$private:任务超时时间3 = Get-Date -format 'yyyy-MM-dd HH:mm:ss'  $private:任务超时时间2

$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'invoke_福报库.ps1'
if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
{
	Write-Error "找不到 'invoke_福报库' "
	exit 1
}
else
{
	$invoke_福报库 = $private:temp999
}

$sqlite_sql =
@"
INSERT INTO "福报任务表rs" ("任务状态", "被控机ip", "命令行", "任务预定开始时间", "任务预定超时时间", "任务重试次数", "备注") VALUES (0, "${被控机ip}", "${福报命令行}", "${private:任务预定开始时间2}", "${private:任务超时时间3}",${任务重试次数},"${备注}") ;
"@

#Write-Host 'sql提交前'
#Write-Verbose $sqlite_sql

try
{
	&  $invoke_福报库  -sqlite_sql语句  $sqlite_sql
}
catch
{
	Write-Error '福报rs表，添加任务失败'
	exit 2
}
finally
{

}
Write-Verbose '福报rs表，添加任务成功'
exit 0
