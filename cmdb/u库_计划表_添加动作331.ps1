﻿#

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[Alias("ip_address")][String]$被控机ip,
	#被控机ip地址，或'主控机'，或'master'

	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[Alias("os_type")][String]$强制提交时_被控机os类型,

	[Alias("file")][String]$脚本文件名,
	[Alias("scriptblock")][scriptblock]$powershell代码块 = { },

	[Alias("allparameter")][String]$传入参数,

	[Alias("enable")][ValidateRange(0,1)][int]$启用状态 = 1,

	[Alias("remark")][String]$备注
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$win = 'win7','win8','win10','win2008r2','win2012r2','win2016','win2019'
$linux = 'centos7','centos8','debian8','debian9','debian10','ubuntu1404','ubuntu1604','ubuntu1804','alpine','ubuntu2004'
$all = $win + $linux + '主控机' + 'master'

if ($强制提交时_被控机os类型 -notin $all)
{
	Write-Error "错误：被控机os类型不合法。错误码3"
	Write-Error $all
	exit 3
}

if ( ($脚本文件名 -eq $null) -or ($脚本文件名 -eq '') )
{
	#这段代码在run_win2linux_key_pwd.ps1,run_win2win5985_pwd.ps1,run_linux2linux_key_pwd.ps1,k-commit.ps1,k_commit_ip.ps1,k_commit_uuid.ps1,k库_计划表_添加动作231.ps1,u库_计划表_添加动作331.ps1
	$有脚本文件 = $false
	if ( ($powershell代码块.Ast.Extent.Text -eq '{ }') -or ($powershell代码块.Ast.Extent.Text -eq '{}') )
	{
		Write-Error "错误：没有输入脚本文件名，同时也没有输入代码块"
		exit 15
	}
	else
	{
		if (& 'kcd_被控机运行的代码块_含有kcd.ps1' -输入脚本代码块 $powershell代码块)
		{
			Write-Error '错误：脚本内含有kcd等关键字'
			exit 19
		}
	}
}
else
{
	$有脚本文件 = $true
	if ( ($powershell代码块.Ast.Extent.Text -eq '{ }') -or ($powershell代码块.Ast.Extent.Text -eq '{}') )
	{

	}
	else
	{
		Write-Error "错误：有输入脚本文件名，同时有输入代码块"
		exit 16
	}

	if (Test-Path -LiteralPath $脚本文件名)
	{
		if (& 'kcd_被控机运行的脚本_含有kcd.ps1' -输入脚本文件名 $脚本文件名)
		{
			Write-Error '错误！脚本内含有kcd等关键字'
			exit 18
		}
	}
	else
	{
		Write-Error "找不到脚本文件"
		exit 17
	}
}

$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'invoke_用户库.ps1'
if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
{
	Write-Error "找不到 'invoke_用户库' "
	exit 1
}
else
{
	$invoke_用户库 = $private:temp999
}

$sqlite_sql =
@"
INSERT INTO "u计划表" ("被控机ip","被控机os类型","脚本文件名","powershell代码块","传入参数","启用状态", "备注") VALUES ("${被控机ip}", "${强制提交时_被控机os类型}", "${脚本文件名}", "${powershell代码块}", "${传入参数}", ${启用状态}, "${备注}")
;
"@


try
{
	& $invoke_用户库  -sqlite_sql语句 $sqlite_sql
}
catch
{
	Write-Error '添加u库，计划任务失败'
	exit 2
}
finally
{

}
Write-Host -ForegroundColor Green '添加u库，计划任务成功'
exit 0

