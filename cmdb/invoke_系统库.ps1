﻿#
param
(
	[string]$sqlite_sql语句 = ''
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}


if ($PSVersionTable.PSVersion.Major -lt 3)
{
	$script:PSScriptRoot = Split-Path -Parent   $myinvocation.mycommand.path
	$script:PSCommandPath = $script:PSScriptRoot + '\' + $myinvocation.mycommand
}

$sqlite_dll = "$script:PSScriptRoot\System.Data.SQLite.dll"
Add-Type -path  $sqlite_dll  -ErrorAction stop


$sqlite数据库文件名 = $PSScriptRoot + '/d当前库/' + 'kasini3000_crontab.sqlite3'
if (Test-Path -LiteralPath $sqlite数据库文件名)
{

}
else
{
	Write-Error "错误，找不到sqlite数据库文件名 ${sqlite数据库文件名}"
	exit 1
}

if ( ($sqlite_sql语句 -eq $null) -or ($sqlite_sql语句 -eq ''))
{
	if ( ($sqlite_sql -eq $null) -or ($sqlite_sql -eq ''))
	{
		Write-Error "错误：找不到sql语句"
		exit 2
	}
	else
	{
		$sqlite_sql语句 = $sqlite_sql
	}
}

Write-Verbose $sqlite_sql语句

$连接1参数 = "Data Source = $sqlite数据库文件名" #可以没有这个文件。
try
{
	$连接1 = New-Object System.Data.SQLite.SQLiteConnection -ErrorAction stop
	$连接1.ConnectionString = $连接1参数
	$连接1.open()
}
catch
{
	Write-Error '错误：库错误，已经修复。请重新运行此脚本。错误码3'
	if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
	{
		Copy-Item -LiteralPath 'c:\ProgramData\kasini3000\cmdb\win.SQLite.Interop.dll' -Destination 'c:\ProgramData\kasini3000\cmdb\SQLite.Interop.dll' -Force
	}

	if ($IsLinux -eq $True)
	{
		Copy-Item -LiteralPath '/etc/kasini3000/cmdb/linux.SQLite.Interop.dll' -Destination '/etc/kasini3000/cmdb/SQLite.Interop.dll' -Force
	}
	exit 3
}

$连接1命令运行器 = New-Object System.Data.SQLite.SQLiteCommand
#$连接1命令运行器.CommandText = "select @@version;"
$连接1命令运行器.CommandText = $sqlite_sql语句
$连接1命令运行器.Connection = $连接1
#问：这个脚本谁写的？有问题找谁技术支持？
#答：QQ群号=183173532
#名称=powershell交流群
#2019-06-20
$连接1结果包装 = New-Object System.Data.SQLite.SQLiteDataAdapter
$连接1结果包装.SelectCommand = $连接1命令运行器

$ms内存数据库 = New-Object System.Data.DataSet
#我写成ds，而不用dt，为了防止有人用【多select在一条sql命令中】这种幺蛾子用法。
$null = $连接1结果包装.Fill($ms内存数据库)
$连接1.close()
$连接1.Dispose()

return $ms内存数据库.Tables[0]

exit 0

