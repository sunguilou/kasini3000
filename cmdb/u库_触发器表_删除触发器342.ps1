﻿#

param
(
	[int]$id = -1,
	[String]$备注 = '',
	[bool]$按照备注参数_强制删除_所有符合条件记录_无需二次确认 = $false
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'invoke_用户库.ps1'
if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
{
	Write-Error "找不到 'invoke_用户库' "
	exit 1
}
else
{
	$invoke_用户库 = $private:temp999
}

if ( ($id -eq -1) -and ($备注 -eq '') )
{
	Write-Error "错误：需要输入【id】或【备注】之一"
	exit 2
}

if ($id -ne -1)
{
	Write-Verbose '命中参数【id】，此时自动忽略参数【备注】'
	$备注 = ''

	$sqlite_sql = @"
SELECT * FROM "u触发器表" WHERE "id"=${id}
;
"@

	try
	{
		$private:查询结果 = & $invoke_用户库  -sqlite_sql语句 $sqlite_sql
	}
	catch
	{
		Write-Error '错误：查询u库，触发器表失败。'
		exit 13
	}
	finally
	{

	}

	Write-Verbose '查询u库，触发器表成功'
	if ( ($private:查询结果 -eq $null) -or ($private:查询结果2 -eq '') )
	{
		Write-Error '错误：找不到u库，触发器表，触发器id。错误码15'
		exit 15
	}
	else
	{

		$sqlite_sql = @"
DELETE FROM "u触发器表" WHERE "id"=${id}
;
"@
	}
}
else
{
	Write-Verbose '命中参数【备注】，此时自动忽略参数【id】'

	$sqlite_sql = @"
DELETE FROM "u触发器表" WHERE "备注"="${备注}" ;
"@

	$private:temp998 = & 'kdir-cmdb.ps1' -被查找的库文件名 'u库_触发器表_列出所有触发器*'
	if ( ($private:temp998 -eq '输入的库路径有错误') -and ($private:temp998 -eq '无返回') )
	{
		Write-Error "找不到 'u库_计划表_列出所有动作' "
		exit 23
	}
	& $private:temp998  -备注 ${备注}

	if ($按照备注参数_强制删除_所有符合条件记录_无需二次确认 -eq $true)
	{
	}
	else
	{
		$private:删除确认 = Read-Host -Prompt '确认删除以上所有条目吗？'
		if ($private:删除确认 -eq 'y')
		{
		}
		else
		{
			Write-Error '用户已经终止删除。退出码24'
			exit 24
		}
	}
}

try
{
	& $invoke_用户库  -sqlite_sql语句 $sqlite_sql
}
catch
{
	Write-Error '删除u库，触发器失败。错误码13'
	exit 3
}
finally
{

}
Write-Host -ForegroundColor Green '删除u库，触发器成功'
exit 0

