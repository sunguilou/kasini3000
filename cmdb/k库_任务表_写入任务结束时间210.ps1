﻿#

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[uint32]$k库任务表不可重任务id,

	[datetime]$任务实际结束时间
)


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}


$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'invoke_系统库.ps1'
if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
{
	Write-Error "找不到 'invoke_系统库' "
	exit 1
}
else
{
	$invoke_系统库 = $private:temp999
}

$sqlite_sql =
@"
UPDATE "k任务表" SET "任务实际结束时间"="${任务实际结束时间}" WHERE "不可重任务id"=${k库任务表不可重任务id};
"@

try
{
	& $invoke_系统库 -sqlite_sql语句 $sqlite_sql
}
catch
{
	Write-Error 'k库任务表,写入任务实际结束时间,失败'
	exit 2
}
finally
{

}
Write-Verbose 'k库任务表,写入任务实际结束时间,成功'

exit 0
