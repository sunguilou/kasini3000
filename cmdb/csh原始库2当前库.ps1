﻿#建议保存编码为：bom头 + utf8

Write-Verbose '原始库2当前库 开始'

$当前库目录 = $PSScriptRoot + '/d当前库/'
if (test-path -LiteralPath $当前库目录)
{
	$当前库文件 = Get-ChildItem -LiteralPath $当前库目录 -File
	if ($当前库文件 -eq $null)
	{
		$原始库目录 = $PSScriptRoot + '/y原始库/'
		if (test-path -LiteralPath $原始库目录)
		{
			$原始库文件 = Get-ChildItem -LiteralPath $原始库目录 -File
			if ($原始库文件 -ne $null)
			{
				Copy-Item -Path "${原始库目录}\*" -Destination $当前库目录
				Write-Verbose '原始库2当前库 复制完毕'
			}
			else
			{
				Write-Error "错误，原始库目录为空"
				exit 4
			}
		}
		else
		{
			Write-Error "错误，找不到原始库目录"
			exit 3
		}
	}
	else
	{
		Write-Error "错误，当前库里有文件"
		exit 2
	}
}
else
{
	Write-Error "错误，找不到当前库目录"
	exit 1
}

Write-Verbose '原始库2当前库 结束'

exit 0
