﻿#

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[uint32]$u库任务表不可重任务id
)


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}


$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'invoke_用户库.ps1'
if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
{
	Write-Error "找不到 'invoke_用户库' "
	exit 1
}
else
{
	$invoke_用户库 = $private:temp999
}

$sqlite_sql =
@"
UPDATE "u任务表" SET "任务状态"=998 WHERE "不可重任务id"=${u库任务表不可重任务id};
"@


try
{
	& $invoke_用户库  -sqlite_sql语句 $sqlite_sql
}
catch
{
	Write-Error 'u库任务表,变更任务状态为【完成】失败'
	exit 2
}
finally
{

}

Write-Verbose 'u库任务表,变更任务状态为【完成】成功'

exit 0
