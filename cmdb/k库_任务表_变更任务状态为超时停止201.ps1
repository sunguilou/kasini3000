﻿#

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[uint32]$k库任务表不可重任务id
)


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'invoke_系统库.ps1'
if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
{
	Write-Error "找不到 'invoke_系统库' "
	exit 1
}
else
{
	$invoke_系统库 = $private:temp999
}

$sqlite_sql =
@"
SELECT "任务状态" FROM "k任务表" WHERE "不可重任务id"=${k库任务表不可重任务id};
"@

try
{
	$结果 = & $invoke_系统库  -sqlite_sql语句 $sqlite_sql
	[uint32]$结果 = $结果['任务状态']
}
catch
{
	Write-Error 'k库任务表,【查询】任务状态,失败'
	exit 2
}
finally
{

}
Write-Verbose 'k库任务表,【查询】任务状态,成功'

switch ($结果)
{
	998
	{
		Write-Error 'k库任务表,变更任务状态为【出错停止】失败。任务成功已经【出错完成】'
		exit 3
	}

	999
	{
		Write-Error 'k库任务表,变更任务状态为【出错停止】失败。任务成功已经【超时完成】'
		exit 4
	}

	1000
	{
		Write-Error 'k库任务表,变更任务状态为【出错停止】失败。任务成功已经【完成】'
		exit 5
	}

	{ $结果 % 3 -eq 0 } #超时状态
	{
		Write-Error 'k库任务表,变更任务状态为【出错停止】,失败。任务成功已经【超时停止】'
		exit 6
	}

	{ $结果 % 3 -eq 1 } #运行状态
	{
		$新状态 = $结果 + 2
		break
	}

	{ $结果 % 3 -eq 2 } #出错完成状态
	{
		Write-Error 'k库任务表,变更任务状态为【出错停止】失败。任务成功已经【出错停止】'
		exit 7
	}

	default
	{
		Write-Error '未知错误'
		exit 8
	}
}



$sqlite_sql =
@"
UPDATE "k任务表" SET "任务状态"=${新状态} WHERE "不可重任务id"=${k库任务表不可重任务id};
"@

try
{
	& $invoke_系统库  -sqlite_sql语句 $sqlite_sql
}
catch
{
	Write-Error 'k库任务表,变更任务状态为【超时停止】失败'
	exit 9
}
finally
{

}
Write-Verbose 'k库任务表,变更任务状态为【超时停止】成功'
exit 0
