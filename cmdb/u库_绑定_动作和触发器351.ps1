﻿# 目前只支持：一个动作，绑定一个触发器。

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[int]$u库_触发器表_触发器1的id,

	[int]$u库_触发器表_触发器2的id = 0,
	[int]$u库_触发器表_触发器3的id = 0,
	[int]$u库_触发器表_触发器4的id = 0,
	[int]$u库_触发器表_触发器5的id = 0,
	[int]$u库_触发器表_触发器6的id = 0,

	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[int]$u库_计划表_动作的id
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'invoke_用户库.ps1'
if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
{
	Write-Error "找不到 'invoke_用户库' "
	exit 1
}
else
{
	$invoke_用户库 = $private:temp999
}

#去重
$private:temp998 = $u库_触发器表_触发器1的id,$u库_触发器表_触发器2的id,$u库_触发器表_触发器3的id,$u库_触发器表_触发器4的id,$u库_触发器表_触发器5的id,$u库_触发器表_触发器6的id
[System.Collections.Generic.HashSet[int]]$去重数组 = @()
foreach ($private:temp997 In $private:temp998)
{
	if ($private:temp997 -eq 0)
	{
		continue
	}
	else
	{
		if ($去重数组.add($private:temp997))
		{

		}
		else
		{
			Write-Error "错误：添加的触发器id有重复！错误码2"
			exit 2
		}
	}
}


$sqlite_sql =
@"
SELECT * FROM "u计划表" WHERE "id"=${u库_计划表_动作的id}
;
"@


try
{
	$private:查询结果 = & $invoke_用户库  -sqlite_sql语句 $sqlite_sql
}
catch
{
	Write-Error 'u库计划表，执行失败'
	exit 2
}
finally
{

}
Write-Verbose 'u库计划表，执行成功'

if ( ($private:查询结果 -eq $null) -or ($private:查询结果 -eq '') )
{
	Write-Error '错误：找不到u库，计划表，动作id。错误码3'
	exit 3
}
else
{
	Write-Verbose 'u库计划表，查询成功'
	$private:查询结果
	$private:启用状态1 = $private:查询结果['启用状态']
	if ($private:启用状态1 -eq "0")
	{
		Write-Warning '警告：这个任务计划动作，没有启用！'
	}
}




$sqlite_sql =
@"
SELECT * FROM "u触发器表" WHERE "id"=${u库_触发器表_触发器1的id}
;
"@

try
{
	$private:查询结果2 = & $invoke_用户库  -sqlite_sql语句 $sqlite_sql
}
catch
{
	Write-Error 'u库触发器表，执行失败'
	exit 4
}
finally
{

}
Write-Verbose 'u库触发器表，执行成功'

if ( ($private:查询结果2 -eq $null) -or ($private:查询结果2 -eq '') )
{
	Write-Error '错误：找不到u库，触发器表，触发器1的id。错误码5'
	exit 5
}
else
{
	Write-Verbose 'u库，触发器表，触发器1的id，查询成功'
	$private:查询结果2
	$private:启用状态2 = $private:查询结果2['启用状态']
	if ($private:启用状态2 -eq "0")
	{
		Write-Warning '警告：这个任务触发器，没有启用！'
	}

	$sqlite_sql = @"
UPDATE "u计划表" SET "触发器1id"=${u库_触发器表_触发器1的id} WHERE "id"=${u库_计划表_动作的id}
;
"@

	try
	{
		& $invoke_用户库  -sqlite_sql语句 $sqlite_sql
	}
	catch
	{
		Write-Error '错误：更新u库，计划表，执行失败'
		exit 51
	}
	finally
	{

	}
	Write-Host -ForegroundColor Green '更新u库，计划表，执行成功'
}


exit 0

