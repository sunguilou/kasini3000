﻿#建议保存编码为：bom头 + utf8
#从数据库读取所有任务，开始执行。

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

if ($global:begin_invoke_福报表 -eq $True)
{
	Write-Verbose '本【异步收取任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 1
}

if ($global:end_invoke_福报表 -eq $True)
{
	Write-Verbose '本【异步开始任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 2
}

if ($global:begin_invoke_k库_任务表 -eq $True)
{
	Write-Verbose '本【异步开始任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 3
}

if ($global:end_invoke_k库_任务表 -eq $True)
{
	Write-Verbose '本【异步开始任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 4
}

if ($global:begin_invoke_u库_任务表 -eq $True)
{
	Write-Verbose '本【异步开始任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 5
}

if ($global:end_invoke_u库_任务表 -eq $True)
{
	Write-Verbose '本【异步开始任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 6
}

if ($global:remove_free_session中 -eq $True)
{
	Write-Verbose '本【移除空闲session】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 9
}

if ($global:begin_invoke_福报表_rs -eq $True)
{
	Write-Verbose '本【异步收取任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 7
}

if ($global:end_invoke_福报表_rs -eq $True)
{
	Write-Verbose '本【异步开始任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 8
}

if ($global:begin_invoke_u库_任务表_rs -eq $True)
{
	Write-Verbose '本【异步开始任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 11
}

if ($global:end_invoke_u库_任务表_rs -eq $True)
{
	Write-Verbose '本【异步开始任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 12
}

$global:begin_invoke_k库_任务表 = $True

#region 从数据库读取任务
$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'k库_任务表_读取n条待运行任务2172.ps1'
if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
{
	Write-Error " 'k库_任务表_读取n条待运行任务2172.ps1' "
	$global:begin_invoke_k库_任务表 = $false
	exit 2172
}
else
{
	$private:脚本名 = $private:temp999
}


$所有待运行任务 = & $private:脚本名
if ( ($所有待运行任务 -eq '') -or ($所有待运行任务 -eq $null) )
{
	Write-Verbose "信息:k库，任务表，进程级别，begin invoke，没有可执行的任务，退出码21"
	$global:begin_invoke_k库_任务表 = $false
	exit 21
}
Write-Verbose ("信息:k库，任务表，进程级别，读取到【{0}】个任务" -f $所有待运行任务.count)

#endregion 从数据库读取任务

foreach ($单个待运行任务 in $所有待运行任务)
{
	#脚本217不含超时任务。含0258369
	$private:不可重任务id = $单个待运行任务['不可重任务id']
	[string]$private:命令行 = $单个待运行任务['命令行']
	$private:任务状态 = $单个待运行任务['任务状态']
	$private:任务重试次数 = $单个待运行任务['任务重试次数']

	if ($private:任务状态 -eq 998)
	{
		Write-Error "错误：k库，任务表，进程级别，任务状态不对。任务id为： ${private:不可重任务id}"
		continue
	}

	if ($private:任务状态 -eq 999)
	{
		Write-Error "错误：信息:k库，任务表，进程级别，任务状态不对。任务id为： ${private:不可重任务id}"
		continue
	}

	if ($private:任务状态 -eq 1000)
	{
		Write-Error "错误：信息:k库，任务表，进程级别，任务状态不对。任务id为： ${private:不可重任务id}"
		continue
	}

	if ($private:任务状态 -gt 1000)
	{
		Write-Error "错误：信息:k库，任务表，进程级别，任务状态太大。任务id为： ${private:不可重任务id}"
		continue
	}

	if ( ($private:任务状态 % 3) -eq 1) #脚本117不含1
	{
		Write-Verbose "信息：信息:k库，任务表，进程级别，任务正在运行。任务id为： ${private:不可重任务id}"
		continue
	}

	$private:任务状态2 = $private:任务状态
	while ($private:任务状态2 -gt 3)
	{
		$private:任务状态2 = $private:任务状态2 - 3
		$private:任务已经运行次数++
	}

	if ( ($private:任务已经运行次数 - 1) -ge $private:任务重试次数 )
	{
		if ( ($private:任务状态 % 3) -eq 2)
		{
			Write-Verbose "信息：变更任务状态:从出错结束，变为出错完成。任务id为： ${private:不可重任务id}"
			$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'k库_任务表_变更任务状态为出错完成204.ps1'
			if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
			{
				Write-Error "找不到 'k库_任务表_变更任务状态为出错完成204.ps1' "
				continue
			}
			else
			{
				$private:脚本名 = $private:temp999
			}

			Write-Verbose $脚本名
			& $private:脚本名 -k库任务表不可重任务id $private:不可重任务id
			continue
		}

		if ( ($private:任务状态 % 3) -eq 3)
		{
			Write-Verbose "信息：变更任务状态:从超时结束，变为超时完成。任务id为： ${private:不可重任务id}"
			$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'k库_任务表_变更任务状态为超时完成202.ps1'
			if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
			{
				Write-Error "找不到 'k库_任务表_变更任务状态为超时完成202.ps1' "
				continue
			}
			else
			{
				$private:脚本名 = $private:temp999
			}

			& $private:脚本名 -k库任务表不可重任务id $private:不可重任务id
			continue
		}
	}

	#main
	$private:命令行2 = [Scriptblock]::Create($private:命令行)
	#region begin_invoke
	Write-Verbose '信息:k库，任务表，进程级别，开始建立：'
	$private:job = Start-Job -ScriptBlock $private:命令行2
	Write-Verbose ("信息:k库，任务表，进程级别，建立完毕，任务id：【{0}】" -f ${private:job}.id )
	#endregion begin_invoke

	#region 写入主控机线程id
	$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'k库_任务表_写入主控机线程uuid213.ps1'
	if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
	{
		Write-Error "找不到 'k库_任务表_写入主控机线程uuid213.ps1' "
		continue
	}
	else
	{
		$private:脚本名 = $private:temp999
	}

	& $private:脚本名 -k库任务表不可重任务id $private:不可重任务id -主控机线程uuid $private:job.InstanceId
	#endregion 写入主控机线程id

	#region 变更任务状态为运行
	$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'k库_任务表_变更任务状态为运行205.ps1'
	if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
	{
		Write-Error "找不到 'k库_任务表_变更任务状态为运行205.ps1' "
		continue
	}
	else
	{
		$private:脚本名 = $private:temp999
	}

	& $private:脚本名 -k库任务表不可重任务id $private:不可重任务id
	#endregion 变更任务状态为运行
}


$global:begin_invoke_k库_任务表 = $false

exit 0
