﻿#建议保存编码为：bom头 + utf8

param
(
	[String]$脚本文件名,
	[scriptblock]$powershell代码块 = { },
	$端口,
	[String]$传入参数,
	[timespan]$任务超时时间 = [timespan]'01:00:00',
	[String]$备注 = (Get-Date -format 'yyyyMMdd HH:mm:ss'),
	$复制主控机node_script目录到被控机 = $false,
	[bool]$强制提交 = $false,
	[string]$强制提交时_被控机os类型
)

function yg严格测试布尔型变量($被测试的布尔形变量)
{
	if (($被测试的布尔形变量 -eq 1) -or ($被测试的布尔形变量 -eq $true) -or ($被测试的布尔形变量 -eq 'true'))
	{
		return $true
	}
	elseif (($被测试的布尔形变量 -eq 0) -or ($被测试的布尔形变量 -eq $false) -or ($被测试的布尔形变量 -eq 'false'))
	{
		return $false
	}
	else
	{
		Write-Error '错误：不合法的布尔型值，错误码999'
		exit 999
	}
}

[bool]$复制主控机node_script目录到被控机 = yg严格测试布尔型变量 $复制主控机node_script目录到被控机

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

Write-Verbose '开始在 被控机ip 提交'

if ($global:当前被控机_ip -eq $null)
{
	Write-Error  "错误：当前被控机 ${global:当前被控机_ip} 为空"
	exit 1
}

if ($global:当前被控机_ip.length -gt 1)
{
	Write-Error  "错误：当前被控机 ${global:当前被控机_ip} 数量太多"
	exit 2
}

$win = 'win7','win8','win10','win2008r2','win2012r2','win2016','win2019'
$linux = 'centos7','centos8','debian8','debian9','debian10','ubuntu1404','ubuntu1604','ubuntu1804','alpine','ubuntu2004'
$all = $win + $linux

if ($强制提交 -eq $true)
{
	if ( ($强制提交时_被控机os类型 -eq $null) -or ($强制提交时_被控机os类型 -eq '') )
	{
		do
		{
			Write-Host $all
			$private:temp01 = Read-Host '请输入 当前被控机ip的，被控机类型'
		}
		while ($private:temp01 -notin $all)
		$global:当前被控机_ip.被控机os类型 = $private:temp01
	}
}
else
{
	if ($global:当前被控机_ip.被控机os类型 -notin $all)
	{
		& 'sx1刷新单个被控机对象的_os类型属性.ps1'
	}

	if ($global:当前被控机_ip.被控机os类型 -notin $all)
	{
		Write-Error  ("错误：当前被控机【{0}】【端口不通】或【账户密码不对】。任务无法提交。退出码4" -f ${global:当前被控机_ip}.ip)
		exit 4
	}
}

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	if ( $global:当前被控机_ip.被控机os类型 -in $win)
	{
		$private:命令行 = "& 'run_win2win5985_pwd.ps1' -目的ip地址 "
	}

	if ( $global:当前被控机_ip.被控机os类型 -in $linux)
	{
		$private:命令行 = "& 'run_win2linux_key_pwd.ps1' -目的ip地址 "
	}
}

if ($IsLinux -eq $True)
{
	if ( $global:当前被控机_ip.被控机os类型 -in $win)
	{
		Write-Error "无法从【linux主控机】控制【win被控机】"
		exit 3
	}

	if ( $global:当前被控机_ip.被控机os类型 -in $linux)
	{
		$private:命令行 = "& 'run_linux2linux_key_pwd.ps1' -目的ip地址 "
	}
}

$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'fb福报库_任务表_添加任务115.ps1'
if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
{
	Write-Error "找不到 'fb福报库_任务表_添加任务115.ps1' "
	exit 115
}
else
{
	$脚本名 = $private:temp999
}

$private:命令行 = $private:命令行 + "'" + ${global:当前被控机_ip}.ip + "' "

if ( ($端口 -eq $null) -or ($端口 -eq '') )
{

}
else
{
	$private:命令行 = $private:命令行 + "-端口 $端口 "
}

if ($复制主控机node_script目录到被控机 -eq $true)
{
	$private:命令行 = $private:命令行 + "-复制主控机node_script目录到被控机 $true "
}
else
{

}

if ( ($powershell代码块.Ast.Extent.Text -eq '{ }') -or ($powershell代码块.Ast.Extent.Text -eq '{}') )
{
	$private:命令行 = $private:命令行 + "-脚本文件名 " + "'$脚本文件名' "
}
else
{
	$private:命令行 = $private:命令行 + '-powershell代码块 {' + $powershell代码块 + '} '
}

if ( ($传入参数 -eq $null) -or ($传入参数 -eq '') )
{

}
else
{
	$private:命令行 = $private:命令行 + "-传入参数 " + $传入参数

	$传入参数2 = ($private:命令行 -split '-传入参数 ')[1]
	if ( $传入参数2.contains(' ') )
	{
		$传入参数3 = $传入参数2.Split(' ')
		foreach ($private:temp in $传入参数3)
		{
			if ( $private:temp.contains('"') -or $private:temp.contains("'") )
			{
				continue
			}
			else
			{
				Write-Error "错误：错误代码5。【传入参数】是字符串类型。必须用单双引号包围【传入参数】`n例如：'"ab c",(get-date)' "
				exit 5
			}
		}
	}
}


$private:命令行 = $private:命令行.replace('"',"'")


& $脚本名 -被控机ip $global:当前被控机_ip.ip -任务超时时间 $任务超时时间 -备注 $备注 -福报命令行 $private:命令行

Start-Sleep -Seconds 1
Write-Verbose '结束在 被控机ip 提交'

exit 0
