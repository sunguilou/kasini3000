﻿#建议保存编码为：bom头 + utf8
$单个密码位数 = 16
$小写字母 = "a","b","c","d","e","f","g","h","i","j","k","m","n","o","p","q","r","s","t","u","v","w","x","y","z"
#小写字母没有字母 l

$数字 = "2","3","4","5","6","7","8","9"
#没有0,1

$大写字母 = "A","B","C","D","E","F","G","H","J","K","L","M","N","P","Q","R","S","T","U","V","W","X","Y","Z"
#大写字母没有字母 I,O

$大小写加数字 = $大写字母 + $小写字母 + $数字

while ($true)
{
	[System.Collections.ArrayList]$一个密码 = @()
	while ($一个密码.Count -lt $单个密码位数)
	{
		$密码字母 = Get-Random -InputObject $大小写加数字
		$null = $一个密码.Add($密码字母)
		Start-Sleep -Milliseconds 37
	}

	$洗牌 = Get-Random  -InputObject $一个密码  -Count $单个密码位数
	$结果 = [string]::Concat($洗牌)

	if ( ($结果 -match "[0-9]") -and ($结果 -match "[a-z]") -and ($结果 -match "[A-Z]") )
	{
		return $结果
	}
}


exit 0
