﻿#建议保存编码为：bom头 + utf8
#
Start-Sleep -Seconds 1
if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

& 'u库定时任务_进程级别_缓存进程_写pid.ps1'

if ((Get-Date).Minute -eq 59)
{
	Write-Error "错误：开发者逻辑设定，u库，进程级别，定时任务，不应该从59分钟开始运行。错误码2"
	exit 2
}

$private:互斥名字 = 'Global\u库_进程级别_定时任务' + (Get-Date -format 'MMddHHmm')
& 'k-console.ps1' -互斥名字 $private:互斥名字


$private:u库_进程级别_定时任务_的磁盘pid = & 'u库定时任务_进程级别_读pid.ps1'
if ( ($private:u库_进程级别_定时任务_的磁盘pid -eq $null) -or ($private:u库_进程级别_定时任务_的磁盘pid -eq '') )
{
	$private:无u库_进程级别_定时任务_的进程 = $True
}
else
{
	$private:u库_进程级别_定时任务_的进程 = Get-Process -id $private:u库_进程级别_定时任务_的磁盘pid
}

if ($private:u库_进程级别_定时任务_的进程 -eq $null)
{
	$private:无u库_进程级别_定时任务_的进程 = $True
}
else
{
	if ( ($private:u库_进程级别_定时任务_的进程.ProcessName -eq 'pwsh') -or ($private:u库_进程级别_定时任务_的进程.ProcessName -eq 'powershell') )
	{
		Write-Error "信息：已经有u库，进程级别，定时任务的进程，正在运行中！"
	}
	else
	{
		$private:无u库_进程级别_定时任务_的进程 = $True
	}
}

$private:开始时间 = Get-Date
if ($private:无u库_进程级别_定时任务_的进程 -eq $True)
{
	Write-Warning '信息：无u库，进程级别，定时任务的进程'
	$private:开始时间2 = $private:开始时间
	$private:结束时间2 = $private:开始时间2.AddHours(1).AddMinutes(-$private:开始时间.Minute).AddSeconds(-$private:开始时间.second).AddSeconds(-12) #应该小于48秒。
}
else
{
	Write-Warning '信息：有u库，进程级别，定时任务的进程'
	$private:开始时间2 = $private:开始时间.AddHours(1).AddMinutes(-$private:开始时间.Minute).AddSeconds(-$private:开始时间.second).AddSeconds(-8)
	$private:结束时间2 = $private:开始时间2.AddHours(1).AddSeconds(-4) #应该小于48秒。
}
$private:超时时间2 = $private:开始时间2.AddSeconds($global:u库_进程级别_定时任务_超时.TotalSeconds)


#main 从59分钟52秒，到59分钟48秒，运行定时任务。
Write-Verbose "信息：u库，进程级别，定时任务进程，【$pid】开始运行。开始时间=【$private:开始时间2】，结束时间=【$private:结束时间2】，超时时间=【 $private:超时时间2】"

do
{
	Start-Sleep -Seconds 1
	$private:现在 = Get-Date
}
while ($private:现在 -le $private:开始时间2)

& 'u库定时任务_进程级别_缓存进程_删pid文件.ps1'
& 'u库定时任务_进程级别_写pid.ps1'
Write-Error '信息：u库，进程级别，定时任务，开始运行'
& 'k_写log.ps1' -被写入的log内容 "信息：u库，进程级别，定时任务进程，【$pid】开始运行。开始时间=【$private:开始时间2】，结束时间=【$private:结束时间2】，超时时间=【 $private:超时时间2】"


$global:u库_进程级别_定时任务_循环中 = $True
while ($global:u库_进程级别_定时任务_循环中 -eq $True)
{
	#-----------【运行u库，进程级别，定时任务】-----------
	if ($global:u库_线程级别_定时任务_启用 -eq $false)
	{
		& 'end_invoke_u库_任务表.ps1'
	}

	if ($private:超时时间2 -lt $private:现在)
	{
		& 'k_写log.ps1' -被写入的log内容 "错误：u库，进程级别，定时任务进程，【$pid】超时退出。"
		Write-Error "错误：u库，进程级别，定时任务进程，【$pid】超时退出。"
		exit 1
	}

	# 超过结束时间，则不会新建任务，无任务则退出。
	if ($private:结束时间2 -lt $private:现在) #$private:结束时间2应该小于51秒。
	{
		if ((Get-Job).count -le 0)
		{
			& 'k_写log.ps1' -被写入的log内容 "信息：u库，进程级别，定时任务进程，【$pid】正常结束。"
			Write-Error "信息：u库，进程级别，定时任务进程，【$pid】正常结束。"
			$global:u库_进程级别_定时任务_循环中 = $false
		}
		else
		{
			$private:u库_进程级别_定时任务_的磁盘pid = & 'u库定时任务_进程级别_读pid.ps1'
			if ($private:u库_进程级别_定时任务_的磁盘pid -eq $pid)
			{
				& 'u库定时任务_进程级别_删pid文件.ps1'
			}

			$private:temp995 = "信息：u库，进程级别，定时任务进程，【$pid】，现有【{0}】个进程" -f (Get-Job).count
			& 'k_写log.ps1' -被写入的log内容 $private:temp995
			& 'k_写log.ps1' -被写入的log内容 ( (Get-Job).State | Out-String )
			Start-Sleep -Seconds 60
			$private:现在 = Get-Date
		}
	}
	else
	{
		if ($global:u库_线程级别_定时任务_启用 -eq $false)
		{
			$private:temp998 = & 'kdir-cmdb.ps1' -被查找的库文件名 'u库_计划表_2_u库_任务表.ps1'
			if ( ($private:temp998 -eq '输入的库路径有错误') -and ($private:temp998 -eq '无返回') )
			{
				Write-Error "找不到 'u库_计划表_2_u库_任务表.ps1' "
			}
			else
			{
				& $private:temp998
			}
		}

		do
		{
			Start-Sleep -Seconds 1
			$private:现在 = Get-Date
		}
		while ($private:现在.Second -notin $global:u库_定时任务_循环在_秒)

		$private:u库_定时任务_当前进程数 = (Get-PSSession).count
		if ($private:u库_定时任务_当前进程数 -gt $global:u库_进程级别_定时任务_最大并发进程数)
		{
			& 'k_写log.ps1' -被写入的log内容 "错误：u库，进程级别，定时任务进程，【$pid】，子进程数太多。 $private:u库_定时任务_当前进程数 "
		}
		else
		{
			if ($global:u库_线程级别_定时任务_启用 -eq $false)
			{
				& 'begin_invoke_u库_任务表.ps1'
			}
		}

		#-----------【运行k库定时任务】-----------
		& 'end_invoke_k库_任务表.ps1'

		$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'k库_计划表_2_k库_任务表.ps1'
		if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
		{
			Write-Error "找不到 'k库_计划表_2_k库_任务表.ps1' "
		}
		else
		{
			& $private:temp999
		}
		Start-Sleep -Seconds 1

		& 'begin_invoke_k库_任务表.ps1'
	}

	if ( ($private:现在.Minute % 9 -eq 0) -and ($private:现在.Second -gt 51) )
	{
		Write-Verbose '开始运行 .net 垃圾回收'
		[System.GC]::Collect()
		[System.GC]::WaitForPendingFinalizers()
	}
}

exit 0
