﻿#建议保存编码为：bom头 + utf8
#Requires -RunAsAdministrator
#此脚本只能在被控机上运行。

Write-Error '信息：此脚本只能在被控机上运行。'
& '/etc/kasini3000/0k_source.ps1'

if ($IsLinux -eq $True)
{
	$username = /usr/bin/whoami
	if ($username -ne 'root')
	{
		Write-Error '非管理员'
		Exit 1
	}

	if ([System.Environment]::Is64BitOperatingSystem -eq $True)
	{

	}
	else
	{
		Write-Error '不支持32位操作系统！'
		Exit 2
	}

	$判断centos6 = @'
rpm -q centos-release
'@ | /usr/bin/bash

	if ( $判断centos6.contains('el6'.tolower()) )
	{
		Write-Error '不支持centos6'
		Exit 3
	}

	if ( $PSScriptRoot -eq '/etc/kasini3000' )
	{
		Write-Error '找不到 /etc/kasini3000'
		Exit 4
	}

	#安装ps依赖库。
	Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
	& '/usr/bin/pwsh' -c 'Install-Module -Name powershell-yaml'
	& '/usr/bin/pwsh' -c 'Install-Module -Name PoshRSJob'

	#安装linux依赖库。
	& 'bkj_install_linuxpackage.ps1' epel-release
	& 'bkj_install_linuxpackage.ps1' ntpdate
	& 'bkj_install_linuxpackage.ps1' fuse-sshfs
	& 'bkj_install_linuxpackage.ps1' sshfs
	& 'bkj_install_linuxpackage.ps1' git
	& 'bkj_install_linuxpackage.ps1' sshpass
	& 'bkj_install_linuxpackage.ps1' openssh-clients

	#建立目录
	mkdir '/etc/kasini3000'
	mkdir '/etc/kasini3000/node_script'
	mkdir '/etc/kasini3000/lib'
}
exit 0
