﻿#建议保存编码为：bom头 + utf8

param
(
	$被控机ip地址 = $global:当前被控机_ip.ip
)

if ($被控机ip地址 -eq $null)
{
	Write-Error "错误：ip ${被控机ip地址} 不存在。`n请手动输入ip地址参数 `n或运行cdip -被控机ip地址 '被控机ip地址'"
	exit 1
}

$private:当前被控机 = $global:所有被控机 | Where-Object { $_.ip -eq $被控机ip地址 }
if ($private:当前被控机 -eq $null)
{
	Write-Error "错误：nodelist文件中找不到这个ip地址 ${当前被控机} "
	[string]$private:被控机os关键字 = 'nodelist文件中找不到这个ip地址'
	exit 2
}

[System.Net.IPAddress]$ip2 = 0
if (   [System.Net.IPAddress]::TryParse($当前被控机.ip,[ref]$ip2)      )
{
	[string]$private:被控机os关键字 = & 'fh返回被控机的_os关键字.ps1' -目的ip地址 ${被控机ip地址}
	Write-Verbose ("被控机【{0}】的os类型是:【{1}】" -f ${private:当前被控机}.被控机显示名,$private:被控机os关键字 )
}
else
{
	Write-Error "nodelist文件中找到这个ip地址： $($当前被控机.ip)，但ip不合法"
	$private:被控机os关键字 = '非法ip地址'
}

Add-Member -InputObject $private:当前被控机 -MemberType NoteProperty -Name '被控机os类型' -Value $private:被控机os关键字 -Force
exit 0
