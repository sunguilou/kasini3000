﻿#建议保存编码为：bom头 + utf8

param
(
	[String]$脚本文件名,
	[scriptblock]$powershell代码块 = {},
	$端口,
	$传入参数,
	[timespan]$任务超时时间 = [timespan]'01:00:00',
	[String]$备注 = (Get-Date -format 'yyyyMMdd HH:mm:ss'),
	[bool]$复制主控机node_script目录到被控机 = $false
)


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}


Write-Verbose '开始在 所有被控机 提交'

foreach ($private:当前被控机5 in $global:所有被控机)
{
	$global:当前被控机_ip = $private:当前被控机5
	& 'k_commit_ip.ps1' -powershell代码块 $powershell代码块  -脚本文件名 $脚本文件名  -传入参数 $全部传入参数 -备注 $备注 -端口 $端口 -任务超时时间 $任务超时时间 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
}

Write-Verbose '结束在 所有被控机 提交'

exit 0
