﻿#建议保存编码为：bom头 + utf8
#优先级=1winrm，2 135，3 22，4目的端口。
param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$目的ip地址,
	[String]$目的端口
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}


if ($IsLinux -eq $True)
{
	switch (${目的端口})
	{
		'22'
		{
			if (Test-Connection -TargetName $目的ip地址 -TCPPort 22 -Quiet)
			{
				& 'zkj_从主控机到linux被控机_ssh_knowhost免yes.ps1'
				$结果 = 'linux'
				return $结果
			}
			else
			{
				Write-Verbose  "错误，被控机 ${目的ip地址} 的22端口不通"
				$结果 = "被控机 ${目的ip地址} 开放端口未知"
				return $结果
			}
		}

		'135'
		{
			if (Test-Connection -TargetName $目的ip地址 -TCPPort 135 -Quiet)
			{
				$结果 = 'win135'
				return $结果
			}
			else
			{
				Write-Verbose "错误，被控机 ${目的ip地址} 的135端口不通"
				$结果 = "被控机 ${目的ip地址} 开放端口未知"
				return $结果
			}
		}

		'5985'
		{
			if (Test-Connection -TargetName $目的ip地址 -TCPPort 5985 -Quiet)
			{
				$结果 = 'win5985'
				return $结果
			}
			else
			{
				Write-Verbose "错误，被控机 ${目的ip地址} 的5985端口不通"
				$结果 = "被控机 ${目的ip地址} 开放端口未知"
				return $结果
			}
		}

		'5986'
		{
			if (Test-Connection -TargetName $目的ip地址 -TCPPort 5986 -Quiet)
			{
				$结果 = 'win5986'
				return $结果
			}
			else
			{
				Write-Verbose "错误，被控机 ${目的ip地址} 的5986端口不通"
				$结果 = "被控机 ${目的ip地址} 开放端口未知"
				return $结果
			}
		}

		Default
		{
			if ( ${目的端口} -eq '')
			{
				#扫描端口，速度较慢
				if (Test-Connection -TargetName $目的ip地址 -TCPPort 5985 -Quiet)
				{
					$结果 = 'win5985'
					return $结果
				}
				else
				{
					Write-Verbose "错误，被控机 ${目的ip地址} 的5985端口不通"
				}

				if (Test-Connection -TargetName $目的ip地址 -TCPPort 5986 -Quiet)
				{
					$结果 = 'win5986'
					return $结果
				}
				else
				{
					Write-Verbose "错误，被控机 ${目的ip地址} 的5986端口不通"
				}

				if (Test-Connection -TargetName $目的ip地址 -TCPPort 135 -Quiet)
				{
					$结果 = 'win135'
					return $结果
				}
				else
				{
					Write-Verbose "错误，被控机 ${目的ip地址} 的135端口不通"
				}

				if (Test-Connection -TargetName $目的ip地址 -TCPPort 22 -Quiet)
				{
					& 'zkj_从主控机到linux被控机_ssh_knowhost免yes.ps1'
					$结果 = 'linux'
					return $结果
				}
				else
				{
					Write-Verbose  "错误，被控机 ${目的ip地址} 的22端口不通"
				}
				Write-Verbose  "被控机 ${目的ip地址} 开放端口未知"
				$结果 = "被控机 ${目的ip地址} 开放端口未知"
				return $结果
			}
			else
			{
				if (Test-Connection -TargetName $目的ip地址 -TCPPort $目的端口 -Quiet)
				{
					Write-Verbose  "被控机 ${目的ip地址} 端口 $目的端口 通了"
					$结果 = "被控机 ${目的ip地址} 端口 $目的端口 通了"
					return $结果
				}
				else
				{
					Write-Verbose  "被控机 ${目的ip地址} 开放端口未知"
					$结果 = "被控机 ${目的ip地址} 开放端口未知"
					return $结果
				}
			}
		}
	}
}


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	switch (${目的端口})
	{
		'5985'
		{
			if (& 'tcp--ping-v2.ps1' $目的ip地址 5985 -Quiet)
			{
				$结果 = 'win5985'
				return $结果
			}
			else
			{
				Write-Verbose  "错误，被控机 ${目的ip地址} 的5985端口不通"
				$结果 = "被控机 ${目的ip地址} 开放端口未知"
				return $结果
			}
		}

		'5986'
		{
			if (& 'tcp--ping-v2.ps1' $目的ip地址 5986 -Quiet)
			{
				$结果 = 'win5986'
				return $结果
			}
			else
			{
				Write-Verbose  "错误，被控机 ${目的ip地址} 的5986端口不通"
				$结果 = "被控机 ${目的ip地址} 开放端口未知"
				return $结果
			}
		}

		'135'
		{
			if (& 'tcp--ping-v2.ps1' $目的ip地址 135 -Quiet)
			{
				$结果 = 'win135'
				return $结果
			}
			else
			{
				Write-Verbose  "错误，被控机 ${目的ip地址} 的135端口不通"
				$结果 = "被控机 ${目的ip地址} 开放端口未知"
				return $结果
			}
		}

		'22'
		{
			if (& 'tcp--ping-v2.ps1' $目的ip地址 22 -Quiet)
			{
				& 'zkj_从主控机到linux被控机_ssh_knowhost免yes.ps1'
				$结果 = 'linux'
				return $结果
			}
			else
			{
				Write-Verbose  "错误，被控机 ${目的ip地址} 的22端口不通"
				$结果 = "被控机 ${目的ip地址} 开放端口未知"
				return $结果
			}
		}

		Default
		{
			if ( ${目的端口} -eq '')
			{
				#扫描端口，速度较慢
				if (& 'tcp--ping-v2.ps1' $目的ip地址 5985 -Quiet)
				{
					$结果 = 'win5985'
					return $结果
				}
				else
				{
					Write-Verbose  "错误，被控机 ${目的ip地址} 的5985端口不通"
				}

				if (& 'tcp--ping-v2.ps1' $目的ip地址 5986 -Quiet)
				{
					$结果 = 'win5986'
					return $结果
				}
				else
				{
					Write-Verbose  "错误，被控机 ${目的ip地址} 的5986端口不通"
				}

				if (& 'tcp--ping-v2.ps1' $目的ip地址 135 -Quiet)
				{
					$结果 = 'win135'
					return $结果
				}
				else
				{
					Write-Verbose  "错误，被控机 ${目的ip地址} 的135端口不通"
				}

				if (& 'tcp--ping-v2.ps1' $目的ip地址 22 -Quiet)
				{
					& 'zkj_从主控机到linux被控机_ssh_knowhost免yes.ps1'
					$结果 = 'linux'
					return $结果
				}
				else
				{
					Write-Verbose  "错误，被控机 ${目的ip地址} 的22端口不通"
				}
				Write-Verbose  "被控机 ${目的ip地址} 开放端口未知"
				$结果 = "被控机 ${目的ip地址} 开放端口未知"
				return $结果
			}
			else
			{
				if (& 'tcp--ping-v2.ps1' $目的ip地址 $目的端口 -Quiet)
				{
					Write-Verbose  "被控机 ${目的ip地址} 端口 $目的端口 通了"
					$结果 = "被控机 ${目的ip地址} 端口 $目的端口 通了"
					return $结果
				}
				else
				{
					Write-Verbose  "被控机 ${目的ip地址} 开放端口未知"
					$结果 = "被控机 ${目的ip地址} 开放端口未知"
					return $结果
				}
			}
		}
	}
}
exit 0
