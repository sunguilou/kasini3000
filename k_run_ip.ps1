﻿#建议保存编码为：bom头 + utf8

param
(
	[Alias("file")][String]$脚本文件名,
	[parameter(Position = 0)][Alias("scriptblock")][scriptblock][scriptblock]$powershell代码块 = { },
	[Alias("allparameter")]$全部传入参数,
	$复制主控机node_script目录到被控机 = $false
)

function yg严格测试布尔型变量($被测试的布尔形变量)
{
	if (($被测试的布尔形变量 -eq 1) -or ($被测试的布尔形变量 -eq $true) -or ($被测试的布尔形变量 -eq 'true'))
	{
		return $true
	}
	elseif (($被测试的布尔形变量 -eq 0) -or ($被测试的布尔形变量 -eq $false) -or ($被测试的布尔形变量 -eq 'false'))
	{
		return $false
	}
	else
	{
		Write-Error '错误：不合法的布尔型值，错误码999'
		exit 999
	}
}

[bool]$复制主控机node_script目录到被控机 = yg严格测试布尔型变量 $复制主控机node_script目录到被控机


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

Write-Verbose '开始在【被控机ip】上执行'

if ($global:当前被控机_ip -eq $null)
{
	Write-Error  "错误：当前被控机 ${global:当前被控机_ip} 为空"
	exit 1
}

if ($global:当前被控机_ip.length -gt 1)
{
	Write-Error  "错误：当前被控机 ${global:当前被控机_ip} 数量太多"
	exit 2
}

$win = 'win7','win8','win10','win2008r2','win2012r2','win2016','win2019'
$linux = 'centos7','centos8','debian8','debian9','debian10','ubuntu1404','ubuntu1604','ubuntu1804','alpine','ubuntu2004'
$all = $win + $linux

if ($global:当前被控机_ip.被控机os类型 -notin $all)
{
	& 'sx1刷新单个被控机对象的_os类型属性.ps1'
}

if ($global:当前被控机_ip.被控机os类型 -notin $all)
{
	Write-Error  ("错误：当前被控机【{0}】【端口不通】或【账户密码不对】。任务无法运行。退出码4" -f ${global:当前被控机_ip}.ip)
	if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
	{
		& "${global:kasini3000目录}\admin_gui\pic\bc随机报错背景图片.ps1"
	}
	exit 4
}

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	if ( $global:当前被控机_ip.被控机os类型 -in $win)
	{
		if ( $global:当前被控机_ip.端口 -eq '')
		{
			& 'run_win2win5985_pwd.ps1'  -目的ip地址 $global:当前被控机_ip.ip -powershell代码块 $powershell代码块  -脚本文件名 $脚本文件名  -传入参数 $全部传入参数 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
		}
		else
		{
			& 'run_win2win5985_pwd.ps1'  -目的ip地址 $global:当前被控机_ip.ip -powershell代码块 $powershell代码块  -脚本文件名 $脚本文件名  -传入参数 $全部传入参数  -端口 $global:当前被控机_ip.端口 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
		}
	}

	if ( $global:当前被控机_ip.被控机os类型 -in $linux)
	{
		if ( $global:当前被控机_ip.端口 -eq '')
		{
			& 'run_win2linux_key_pwd.ps1'  -目的ip地址 $global:当前被控机_ip.ip -powershell代码块 $powershell代码块  -脚本文件名 $脚本文件名  -传入参数 $全部传入参数 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
		}
		else
		{
			& 'run_win2linux_key_pwd.ps1'  -目的ip地址 $global:当前被控机_ip.ip -powershell代码块 $powershell代码块  -脚本文件名 $脚本文件名  -传入参数 $全部传入参数  -端口 $global:当前被控机_ip.端口 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
		}
	}
}

if ($IsLinux -eq $True)
{
	if ( $global:当前被控机_ip.被控机os类型 -in $win)
	{
		Write-Error "无法从【linux主控机】控制【win被控机】"
		exit 3
	}

	if ( $global:当前被控机_ip.被控机os类型 -in $linux)
	{
		if ( $global:当前被控机_ip.端口 -eq '')
		{
			& 'run_linux2linux_key_pwd.ps1'  -目的ip地址 $global:当前被控机_ip.ip -powershell代码块 $powershell代码块  -脚本文件名 $脚本文件名  -传入参数 $全部传入参数 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
		}
		else
		{
			& 'run_linux2linux_key_pwd.ps1'  -目的ip地址 $global:当前被控机_ip.ip -powershell代码块 $powershell代码块  -脚本文件名 $脚本文件名  -传入参数 $全部传入参数  -端口 $global:当前被控机_ip.端口 -复制主控机node_script目录到被控机 $复制主控机node_script目录到被控机
		}
	}
}

Write-Verbose '结束在【被控机ip】上执行'

exit 0
