﻿#建议保存编码为：bom头 + utf8

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[Alias("ipaddress")][String]$目的ip地址,

	[Alias("port")][uint16]$端口 = 22,

	[Alias("file")][String]$脚本文件名,

	[Alias("allparameter")]$传入参数,

	[parameter(Position = 0)][Alias("scriptblock")][scriptblock]$powershell代码块 = { },

	$是第二次执行命令 = $false,

	$复制主控机node_script目录到被控机 = $false
)

function yg严格测试布尔型变量($被测试的布尔形变量)
{
	if (($被测试的布尔形变量 -eq 1) -or ($被测试的布尔形变量 -eq $true) -or ($被测试的布尔形变量 -eq 'true'))
	{
		return $true
	}
	elseif (($被测试的布尔形变量 -eq 0) -or ($被测试的布尔形变量 -eq $false) -or ($被测试的布尔形变量 -eq 'false'))
	{
		return $false
	}
	else
	{
		Write-Error '错误：不合法的布尔型值，错误码999'
		exit 999
	}
}

[bool]$复制主控机node_script目录到被控机 = yg严格测试布尔型变量 $复制主控机node_script目录到被控机
[bool]$是第二次执行命令 = yg严格测试布尔型变量 $是第二次执行命令

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	Write-Error "应该使用另一个脚本！"
	exit 11
}

Write-Verbose "开始在linux2linux被控机【${目的ip地址}】上执行"

if ( ($脚本文件名 -eq $null) -or ($脚本文件名 -eq '') )
{
	#这段代码在run_win2linux_key_pwd.ps1,run_win2win5985_pwd.ps1,run_linux2linux_key_pwd.ps1,k-commit.ps1
	$有脚本文件 = $false
	if ( ($powershell代码块.Ast.Extent.Text -eq '{ }') -or ($powershell代码块.Ast.Extent.Text -eq '{}') )
	{
		Write-Error "错误：没有输入脚本文件名，同时也没有输入代码块"
		exit 15
	}
	else
	{
		if (& 'kcd_被控机运行的代码块_含有kcd.ps1' -输入脚本代码块 $powershell代码块)
		{
			Write-Error '错误：脚本内含有kcd等关键字'
			exit 19
		}
	}
}
else
{
	$有脚本文件 = $true
	if ( ($powershell代码块.Ast.Extent.Text -eq '{ }') -or ($powershell代码块.Ast.Extent.Text -eq '{}') )
	{

	}
	else
	{
		Write-Error "错误：有输入脚本文件名，同时有输入代码块"
		exit 16
	}

	if (Test-Path -LiteralPath $脚本文件名)
	{
		if (& 'kcd_被控机运行的脚本_含有kcd.ps1' -输入脚本文件名 $脚本文件名)
		{
			Write-Error '错误！脚本内含有kcd等关键字'
			exit 18
		}
	}
	else
	{
		Write-Error "找不到脚本文件"
		exit 17
	}
}

if ($IsLinux -eq $True)
{
	$private:ssh_key2 = "${global:kasini3000目录}/ssh_key_files_old1/id_rsa"
	if (Test-Path -LiteralPath $private:ssh_key2)
	{
		chmod 600 $private:ssh_key2
	}
	else
	{
		$private:没有sshkey = 1
	}

	$private:ssh_key1 = '/root/.ssh/id_rsa'
	if (Test-Path -LiteralPath $private:ssh_key1)
	{
		chmod 600 $private:ssh_key1
	}
	else
	{
		$private:没有sshkey++
	}

	if ($private:没有sshkey -ge 2)
	{
		$ErrorActionPreference = 'continue'
		Write-Error "错误：没找到任何ssh key。卡死你3000ssh主控机，管理ssh被控机，必须使用ssh key。退出码3`n请运行【jl建立主控机ssh秘钥1z.ps1】"
		exit 3
	}

	if ($复制主控机node_script目录到被控机 -eq $true)
	{
		& 'kctf复制主控机node_script目录脚本到当前被控机.ps1'
	}

	Write-Verbose '使用ssh秘钥1，连接开始'
	[string]$private:temp011 = ssh -l root -i '/root/.ssh/id_rsa'  ${目的ip地址} -p $端口 -o PasswordAuthentication=no 'date' *>&1
	Write-Verbose $private:temp011
	if ( $private:temp011.ToLower().Contains('Permission denied'.ToLower()) )
	{
		$秘钥1连接成功 = $false
		Write-Verbose "使用ssh秘钥1，在${目的ip地址}上连接失败"
	}
	else
	{
		$秘钥1连接成功 = $true
	}

	if ( $private:temp011.ToLower().Contains('timed out'.ToLower()) )
	{
		Write-Error	 "使用ssh秘钥1，在${目的ip地址}上连接超时"
		exit 1
	}

	if ($秘钥1连接成功 -eq $true)
	{
		$private:连接1 = New-PSSession -HostName ${目的ip地址} -Port $端口 -UserName root -KeyFilePath '/root/.ssh/id_rsa'

		Write-Verbose '【连接1】连接成功。现在开始执行命令：'
		$主控机公钥sha1 = Get-FileHash -Algorithm sha1 -LiteralPath "${global:kasini3000目录}/ssh_key_files_old1/authorized_keys"
		$被控机公钥sha1 = Invoke-Command -Session $private:连接1 -ScriptBlock { Get-FileHash -Algorithm sha1 -LiteralPath '/root/.ssh/authorized_keys' }
		Write-Verbose ("公钥： {0} '---' {1} " -f ${主控机公钥sha1}.Hash,${被控机公钥sha1}.Hash)
		if ($主控机公钥sha1.Hash -ne $被控机公钥sha1.Hash)
		{
			Write-Verbose "主控机-被控机之间，公钥有不同"
			Copy-Item -LiteralPath "${global:kasini3000目录}/ssh_key_files_old1/authorized_keys" -Destination '/root/.ssh/' -ToSession $private:连接1 -Force
		}

		Invoke-Command -Session $private:连接1 -ScriptBlock { & '/etc/kasini3000/node_script/kasini3000/tj在被控机添加path.ps1' }

		if ( $有脚本文件 -eq $true)
		{
			Invoke-Command -Session $private:连接1 -FilePath $脚本文件名 -ArgumentList $传入参数
		}
		else
		{
			Invoke-Command -Session $private:连接1 -ScriptBlock $powershell代码块 -ArgumentList $传入参数
		}
		Write-Verbose '【连接1】执行命令完成，即将断开连接。'
		Remove-PSSession -Session $private:连接1
	}
	else
	{
		Write-Verbose '使用ssh秘钥2，连接开始'
		[string]$private:temp012 = ssh -l root -i "${global:kasini3000目录}/ssh_key_files_old1/id_rsa" ${目的ip地址} -p $端口 -o PasswordAuthentication=no 'date' *>&1
		Write-Verbose $private:temp012
		if ( $private:temp012.ToLower().Contains('Permission denied'.ToLower()) )
		{
			$秘钥2连接成功 = $false
			Write-Verbose "使用ssh秘钥2，在${目的ip地址}上连接失败"
		}
		else
		{
			$秘钥2连接成功 = $true
		}

		if ($秘钥2连接成功 -eq $true)
		{
			$private:连接2 = New-PSSession -HostName ${目的ip地址} -Port $端口 -UserName root -KeyFilePath  "${global:kasini3000目录}/ssh_key_files_old1/id_rsa"

			Write-Verbose '【连接2】连接成功。现在开始执行命令：'
			$主控机公钥sha1 = Get-FileHash -Algorithm sha1 -LiteralPath "${global:kasini3000目录}/ssh_key_files_old1/authorized_keys"
			$被控机公钥sha1 = Invoke-Command -Session $private:连接2 -ScriptBlock { Get-FileHash -Algorithm sha1 -LiteralPath '/root/.ssh/authorized_keys' }
			Write-Verbose ("公钥： {0} '---' {1} " -f ${主控机公钥sha1}.Hash,${被控机公钥sha1}.Hash)
			if ($主控机公钥sha1.Hash -ne $被控机公钥sha1.Hash)
			{
				Write-Verbose "主控机-被控机之间，公钥有不同"
				Copy-Item -LiteralPath "${global:kasini3000目录}/ssh_key_files_old1/authorized_keys" -Destination '/root/.ssh/' -ToSession $private:连接2 -Force
			}

			Invoke-Command -Session $private:连接2 -ScriptBlock { & '/etc/kasini3000/node_script/kasini3000/tj在被控机添加path.ps1' }

			if ( $有脚本文件 -eq $true)
			{
				Invoke-Command -Session $private:连接2 -FilePath $脚本文件名 -ArgumentList $传入参数
			}
			else
			{
				Invoke-Command -Session $private:连接2 -ScriptBlock $powershell代码块 -ArgumentList $传入参数
			}
			Write-Verbose '【连接2】执行命令完成，即将断开连接。'
			Remove-PSSession -Session $private:连接2
		}
		else
		{
			if ($是第二次执行命令 -eq $true)
			{
				Write-Error "使用ssh密码连接成功，但使用ssh秘钥连接失败"
				exit 4
			}

			& 'zd只读nodelist文件.ps1'
			$当前被控机 = $global:所有被控机 | Where-Object { $_.ip -eq $目的ip地址 }
			if ($当前被控机.ip -ne $目的ip地址)
			{
				Write-Error "错误：当前被控机获取失败 ${当前被控机}"
				exit 13
			}

			#依赖sshpass
			[string]$temp2 = /usr/bin/which sshpass
			if ( $temp2.tolower().contains('no sshpass') )
			{
				Write-Error '此linux主控机上没有 sshpass程序'
				exit 14
			}

			Write-Verbose '使用ssh密码，连接开始'
			$ssh命令 = @"
sshpass -p '$(${当前被控机}.当前密码)' ssh -o StrictHostKeyChecking=no root@${目的ip地址} "if [ -h /usr/bin/pwsh ];then echo 'you'; else echo 'wu' ;fi"
"@

			$ssh命令返回 = $ssh命令 | /usr/bin/bash
			if ($LASTEXITCODE -ne 0)
			{
				Write-Error "使用ssh密码，在${目的ip地址}上连接失败"
				exit 2
			}
			else
			{
				$密码连接成功 = $true
				Write-Verbose "使用ssh密码，在${目的ip地址}上连接成功。"
			}

			if ($密码连接成功 -eq $true)
			{
				#检测ps安装，推送公钥。
				if ($ssh命令返回 -eq 'you')
				{

					$检测subsystemcmd = "`"if grep -Eq 'powershell' /etc/ssh/sshd_config; then echo 'powershell_ssh_good';fi `" "
					$执行pwsh命令的cmd = @"
sshpass -p '$(${当前被控机}.当前密码)' ssh -o StrictHostKeyChecking=no root@${目的ip地址} $检测subsystemcmd
"@
					$ssh命令返回 = $执行pwsh命令的cmd | /usr/bin/bash
					if ($ssh命令返回 -ne 'powershell_ssh_good')
					{
						Write-Verbose "在${目的ip地址}上，sshd_config文件的subsystem配置错误"
						& 'zkj_install_powershell_从linux主控机到linux被控机.ps1'  -目的ip地址 $目的ip地址
					}
					else
					{
						Write-Verbose "在${目的ip地址}上，sshd_config文件的subsystem配置正确"
						$ssh命令 = @"
sshpass -p '$(${当前被控机}.当前密码)' ssh -o StrictHostKeyChecking=no root@${目的ip地址} "mkdir '/etc/kasini3000'"
"@

						$ssh命令返回 = $ssh命令 | /usr/bin/bash
					}

					Write-Verbose '推送ssh公钥开始'
					& 'ts推送主控机公钥到被控机_linux2linux_pwd.ps1' -目的ip地址 $目的ip地址 -ssh密码 $(${当前被控机}.当前密码)
					Write-Verbose '推送ssh公钥完毕，即将断开连接。'
				}

				if ($ssh命令返回 -eq 'wu')
				{
					Write-Verbose "在${目的ip地址}上,未安装pwsh"
					& 'zkj_install_powershell_从linux主控机到linux被控机.ps1'  -目的ip地址 $目的ip地址
				}

				Write-Verbose '推送ssh公钥开始'
				& 'ts推送主控机公钥到被控机_linux2linux_pwd.ps1' -目的ip地址 $目的ip地址 -ssh密码 $(${当前被控机}.当前密码)
				Write-Verbose '推送ssh公钥完毕，即将断开连接。'
			}
			& 'run_linux2linux_key_pwd.ps1' -目的ip地址 $目的ip地址  -端口 $端口  -脚本文件名 $脚本文件名  -传入参数 $传入参数 -powershell代码块 $powershell代码块 -是第二次执行命令 $true
		}
	}
}

Write-Verbose "完成在linux2linux被控机【${目的ip地址}】上执行"
exit 0
