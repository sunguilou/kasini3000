﻿#建议保存编码为：bom头 + utf8
#此脚本只能在被控机上运行。
# 启用winrm，安装插件。非必要安装ps6，或ps7。
#Requires -RunAsAdministrator

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

if (($PSVersionTable.PSVersion.Major -lt 5) -and ($PSVersionTable.PSVersion.Minor -lt 1))
{
	Write-Error '错误：powershell版本太低。必须是powershell5.1'
	Exit 11
}

Write-Error '信息：此脚本只能在被控机上运行。'
& '/etc/kasini3000/0k_source.ps1'

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	$win7 = powershell.exe -command { (Get-WmiObject -Class Win32_OperatingSystem).Caption -like "*7*" }
	$win2008 = powershell.exe -command { (Get-WmiObject -Class Win32_OperatingSystem).Caption -like "*2008*" }
	if ( $win7 -or $win2008 )
	{
		Write-Warning '卡死你3000被控机，支持win7，支持win2008。'
	}

	#设定winrm
	Enable-PSRemoting -SkipNetworkProfileCheck -Force
	Set-NetFirewallRule -Name "WINRM-HTTP-In-TCP-NoScope" -RemoteAddress Any
	Set-NetFirewallRule -Name "WINRM-HTTP-In-TCP" -RemoteAddress Any
	Restart-Service WinRM   #并重启服务
	Set-Item WSMan:\localhost\Client\TrustedHosts -value * -Force
	Restart-Service WinRM   #并重启服务


	#安装ps依赖库。
	Install-PackageProvider -Name NuGet  -Force
	Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
	Install-Module -Name powershellget -Force
	Install-Module -Name PackageManagement -Force
	Install-Module -Name PSReadLine
	powershell.exe -command { Install-Module -Name powershell-yaml }
	powershell.exe -command { Install-Module -Name PoshRSJob }
	powershell.exe -command { Install-Module -Name ThreadJob }
	powershell.exe -command { Install-Module -Name winscp }


	#安装pwsh
	Write-Warning '现在开始安装：非必要插件,请等待。。。'
	$json2 = Invoke-RestMethod 'https://api.github.com/repos/powershell/powershell/releases/latest'
	foreach ($temp1 in $json2.assets.browser_download_url)
	{
		if ($temp1 -match 'win-x64.msi')
		{
			$安装包url = $temp1
		}
	}
	Start-Process -FilePath 'C:\Windows\system32\msiexec.exe' -ArgumentList "/i $安装包url /qn /l*v msiexec_log.txt " -Verb runas
	Start-Sleep -Seconds 3
	if ((Test-Path -LiteralPath 'c:\Program Files\PowerShell\6\pwsh.exe') -or (Test-Path -LiteralPath 'c:\Program Files\PowerShell\7\pwsh.exe') -or (Test-Path -LiteralPath 'c:\Program Files\PowerShell\7-preview\pwsh.exe'))
	{

	}
	else
	{
		Write-Error '错误： pwsh安装失败'
		exit 5
	}

	#建立目录
	mkdir 'c:\ProgramData\kasini3000'
	mkdir 'c:\ProgramData\kasini3000\node_script'
	mkdir 'c:\ProgramData\kasini3000\lib'
}
exit 0
