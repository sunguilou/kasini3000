﻿#建议保存编码为：bom头 + utf8

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

if ($global:begin_invoke_福报表 -eq $True)
{
	Write-Verbose '本【异步收取任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 1
}

if ($global:end_invoke_福报表 -eq $True)
{
	Write-Verbose '本【异步开始任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 2
}

if ($global:begin_invoke_k库_任务表 -eq $True)
{
	Write-Verbose '本【异步开始任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 3
}

if ($global:end_invoke_k库_任务表 -eq $True)
{
	Write-Verbose '本【异步开始任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 4
}

if ($global:begin_invoke_u库_任务表 -eq $True)
{
	Write-Verbose '本【异步开始任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 5
}

if ($global:end_invoke_u库_任务表 -eq $True)
{
	Write-Verbose '本【异步开始任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 6
}

if ($global:remove_free_session中 -eq $True)
{
	Write-Verbose '本【移除空闲session】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 9
}

if ($global:begin_invoke_福报表_rs -eq $True)
{
	Write-Verbose '本【异步收取任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 7
}

if ($global:end_invoke_福报表_rs -eq $True)
{
	Write-Verbose '本【异步开始任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 8
}

if ($global:begin_invoke_u库_任务表_rs -eq $True)
{
	Write-Verbose '本【异步开始任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 11
}

if ($global:end_invoke_u库_任务表_rs -eq $True)
{
	Write-Verbose '本【异步开始任务】脚本，即将退出。不建议同时执行，异步开始任务，异步收取任务。'
	exit 12
}

$global:end_invoke_福报表 = $True

Get-PSSession | Where-Object { ($_.State -eq 'Closed') } | Remove-PSSession
Get-Job | Where-Object { ($_.State -eq 'NotStarted') } | Remove-Job -Force

if ( (Get-Job).count -le 0)
{
	Write-Verbose '信息:福报表，进程级别，end invoke，无事可做退出。退出码21'
	$global:end_invoke_福报表 = $false
	exit 21
}

$private:所有完成job = Get-Job | Where-Object { ($_.State -eq 'Completed') -or ($_.State -eq 'Failed') }
if ( ( $private:所有完成job.count -le 0) -or ($private:所有完成job -eq $null) )
{
	Write-Verbose '信息:福报表，进程级别，无【已经成功完成】任务，无【已经失败完成】任务。'
}
else
{
	foreach ($private:job2 in $private:所有完成job)
	{
		$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'fb福报库_任务表_读取主控机线程uuid114.ps1'
		if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
		{
			Write-Error "找不到 'fb福报库_任务表_读取主控机线程uuid114.ps1' "
			continue
		}
		else
		{
			$脚本名 = $private:temp999
		}

		$private:不可重任务id,$null = & $脚本名 -主控机线程uuid ${private:job2}.InstanceId
		if ( ($private:不可重任务id -eq $null) -or ($private:不可重任务id -eq '') )
		{
			Write-Verbose ("错误:福报表，进程级别，无匹配的任务。【{0}】" -f ${private:job2}.InstanceId)
			continue
		}
		else
		{
			$private:不可重任务id = $private:不可重任务id['不可重任务id']

			#写入任务返回结果
			if ($private:job2.HasMoreData -eq $True)
			{
				$private:任务输出 = Receive-Job -InstanceID $private:job2.InstanceID *>&1
				#$private:任务输出 = $private:任务输出.tostring().replace('"',"'")
				$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'fb福报库_任务表_写入任务输出111.ps1'
				if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
				{
					Write-Error "找不到 'fb福报库_任务表_写入任务输出111.ps1' "
					continue
				}
				else
				{
					$脚本名 = $private:temp999
				}

				& $脚本名 -福报表不可重任务id $private:不可重任务id -任务标准输出 $private:任务输出
			}

			#写入任务运行状态
			if ($private:job2.State -eq 'Completed')
			{
				$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'fb福报库_任务表_变更任务状态为正常完成106.ps1'
				if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
				{
					Write-Error "找不到 'fb福报库_任务表_变更任务状态为正常完成106.ps1' "
					continue
				}
				else
				{
					$private:脚本名 = $private:temp999
				}

				& $private:脚本名 -福报表不可重任务id $private:不可重任务id
			}
			else
			{
				$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'fb福报库_任务表_变更任务状态为出错停止103.ps1'
				if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
				{
					Write-Error "找不到 'fb福报库_任务表_变更任务状态为出错停止103.ps1' "
					continue
				}
				else
				{
					$private:脚本名 = $private:temp999
				}

				& $private:脚本名 -福报表不可重任务id $private:不可重任务id
			}

			#写入任务实际结束时间
			$private:任务实际结束时间 = $private:job2.PSEndTime
			$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'fb福报库_任务表_写入任务结束时间110.ps1'
			if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
			{
				Write-Error "找不到 'fb福报库_任务表_写入任务结束时间110.ps1' "
				continue
			}
			else
			{
				$private:脚本名 = $private:temp999
			}

			& $private:脚本名 -福报表不可重任务id $private:不可重任务id -任务实际结束时间 $private:任务实际结束时间
			Remove-Job -InstanceID $private:job2.InstanceID -Force
		}
	}
}

$private:所有超时job = Get-Job | Where-Object { $_.State -eq 'Running' }
if ( ( $private:所有超时job.count -le 0) -or ($private:所有超时job -eq $null) )
{
	Write-Verbose '信息:福报表，进程级别，无【正在运行】任务。'
}
else
{
	foreach ($private:job3 in $private:所有超时job)
	{
		$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'fb福报库_任务表_读取主控机线程uuid114.ps1'
		if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
		{
			Write-Error "找不到 'fb福报库_任务表_读取主控机线程uuid114.ps1' "
			continue
		}
		else
		{
			$private:脚本名 = $private:temp999
		}

		$private:返回 = & $private:脚本名 -主控机线程uuid ${private:job3}.InstanceId
		$private:不可重任务id = [UInt32]$private:返回['不可重任务id']
		$private:超时时间 = [datetime]$private:返回['任务预定超时时间']

		if ( ($private:不可重任务id -eq $null) -or ($private:不可重任务id -eq '') )
		{
			Write-Error ("错误:福报表，进程级别，无匹配的任务。【{0}】" -f ${private:job3}.InstanceId)
			continue
		}
		else
		{
			if ($private:超时时间 -lt (Get-Date))
			{
				Write-Error ("信息:福报表，进程级别，此任务已经超时：【{0}】" -f ${private:job3}.InstanceId)

				#变更任务状态为超时停止
				$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'fb福报库_任务表_变更任务状态为超时停止101.ps1'
				if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
				{
					Write-Error "找不到 'fb福报库_任务表_变更任务状态为超时停止101.ps1' "
					continue
				}
				else
				{
					$private:脚本名 = $private:temp999
				}

				& $private:脚本名 -福报表不可重任务id $private:不可重任务id

				Remove-Job -InstanceID $private:job3.InstanceID -Force
			}
			else
			{
				Write-Verbose ("信息:福报表，进程级别，此任务依旧在运行：【{0}】" -f ${private:job3}.InstanceId)
			}
		}
	}
}

#所有福报表内，显示正在运行的任务，但不匹配job.InstanceID的，运行状态都设置成出错。
$private:所有job = Get-Job

$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'fb福报库_任务表_读取所有正在运行任务1071.ps1'
if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
{
	Write-Error "找不到 'fb福报库_任务表_读取所有正在运行任务1071.ps1' "
	continue
}
else
{
	$private:脚本名 = $private:temp999
}

$private:福报表内显示_所有正在运行任务 = & $private:脚本名
foreach ($private:福报表内显示_单个正在运行任务 in $private:福报表内显示_所有正在运行任务)
{
	$private:temp993 = $private:福报表内显示_单个正在运行任务['不可重任务id']
	$private:temp992 = $private:福报表内显示_单个正在运行任务['主控机线程uuid']
	if ($private:temp992 -in $private:所有job.InstanceID )
	{
	}
	else
	{
		$private:temp999 = & 'kdir-cmdb.ps1' -被查找的库文件名 'fb福报库_任务表_变更任务状态为出错停止103.ps1'
		if ( ($private:temp999 -eq '输入的库路径有错误') -and ($private:temp999 -eq '无返回') )
		{
			Write-Error "找不到 'fb福报库_任务表_变更任务状态为出错停止103.ps1' "
			continue
		}
		else
		{
			$private:脚本名 = $private:temp999
		}
		& $private:脚本名 -福报表不可重任务id $private:temp993
	}
}


$global:end_invoke_福报表 = $false

exit 0
