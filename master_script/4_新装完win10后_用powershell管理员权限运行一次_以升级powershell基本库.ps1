#Requires -RunAsAdministrator

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	Write-Warning '信息：开始安装新版 nuget'
	Install-PackageProvider -Name NuGet  -Force

	Set-PSRepository -Name PSGallery -InstallationPolicy Trusted

	Write-Warning '信息：开始安装新版 powershellget'
	powershell.exe -command { Install-Module powershellget -Force }

	Write-Warning '信息：开始安装新版 PackageManagement'
	powershell.exe -command { Install-Module PackageManagement -Force }

	Write-Warning '信息：开始安装新版 psreadline'
	powershell.exe -command { Install-Module -Name PSReadLine -AllowPrerelease -Force -Scope CurrentUser }

	Write-Warning '信息：安装完毕，请重开powershell。'
}
else
{
	Write-Error '错误：不是win系统'
}

$msg =
@'
问：这个脚本谁写的？有问题找谁技术支持？
答：QQ群号=183173532
名称=powershell交流群
华夏脚之峰。Everest of the powershell

新装完win10后，用powershell管理员权限运行一次，以升级powershell基本库。安装后，重开powershell即可。
'@

Write-Host $msg
