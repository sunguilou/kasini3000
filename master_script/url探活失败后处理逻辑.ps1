#建议保存编码为：bom头 + utf8

<#
1 平常只做慢探活，或中探活。
2 失败则做，快探活。
3 再次失败，则运行探死逻辑。
#>

$private:慢探活结果 = & 'url探活慢.ps1'
if ($private:慢探活结果 -eq $true)
{
	exit 0
}
else
{
	$private:快探活结果 = & 'url探活快.ps1'
	if ($private:快探活结果 -eq $true)
	{
		exit 0
	}
	else
	{
		Write-Error '错误：应用已死'
		#写入log
		#后续报警
		#后续重启应用
	}
}


