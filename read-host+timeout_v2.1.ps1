﻿#用法： & read-host-string-timeout-v2.ps1 -变量名 zzz -变量值 111
param
(
	$变量名 = 'a',
	$变量值 = 'b'
)

$ErrorActionPreference = 'SilentlyContinue'
Remove-Variable -Scope Global -Name ${变量名}

Write-Host "按任意键输入【ssh-key的密码】："

$i = 20
$end = $false
do
{
	if ([console]::KeyAvailable -eq $true) #有按键
	{
		$null = Read-Host
		break
	}

	Write-Host '等待 ' -NoNewline
	Write-Host $i -NoNewline -ForegroundColor Yellow
	Write-Host " 秒后不按键，将无密码创建ssh-key。`r" -NoNewline
	$i--

	if ($i -lt 0)
	{
		New-Variable -Scope Global -Name ${变量名} -Value ${变量值}
		Write-Host "用户输入超时，使用默认值： ${变量名} = 空 " -ForegroundColor Yellow
		exit 0 #用户未输入
	}
	Start-Sleep -Seconds 1
} while ($end -eq $false)

#问：这个脚本谁写的？有问题找谁技术支持？
#答：QQ群号=183173532
#名称=powershell交流群
#win，linux通用，测试通过。

New-Variable -Scope Global -Name ${变量名} -Value 'n'
exit 1 #用户有输入


