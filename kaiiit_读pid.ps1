﻿#建议保存编码为：bom头 + utf8
#

$log日志文件全目录 = "${global:主控机db目录}/d当前库"

if (Test-Path -LiteralPath $log日志文件全目录)
{
	$kaiiit的pid文件 = "$log日志文件全目录/kaiiit_pid.txt"
	if (Test-Path -LiteralPath $kaiiit的pid文件)
	{
		$kaiiit的pid = Get-Content -LiteralPath $kaiiit的pid文件
		return $kaiiit的pid
	}

}
else
{
	Write-Error '找不到日志文件目录'
	exit 1
}

exit 0
