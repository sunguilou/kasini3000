﻿#建议保存编码为：bom头 + utf8

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
	if (Test-Path -LiteralPath $global:被控机列表文件)
	{

	}
	else
	{
		Write-Error "错误，找不到被控机列表文件 $global:被控机列表文件 。`n请按照示例文件 ‘c:\ProgramData\kasini3000\docs\examples\nodelist.csv’ `n请按照示例文件 ‘c:\ProgramData\kasini3000\docs\examples\nodelist.xlsx’ `n生成被控机列表文件‘c:\ProgramData\kasini3000\nodelist.csv’"
		exit 1
	}
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
	if (Test-Path -LiteralPath $global:被控机列表文件)
	{

	}
	else
	{
		Write-Error "错误，找不到被控机列表文件 $global:被控机列表文件 。`n请按照示例文件 ‘/etc/kasini3000/docs/examples/nodelist.csv’ `n请按照示例文件 ‘/etc/kasini3000/docs/examples/nodelist.xlsx’ `n生成被控机列表文件‘/etc/kasini3000/nodelist.csv’"
		exit 1
	}
}

$private:所有被控机999 = @()

foreach ($private:temp1 in $global:所有被控机)
{
	$private:temp2 = [PSCustomObject]@{
		ip        = $private:temp1.ip
		端口        = $private:temp1.端口
		用户名       = $private:temp1.用户名
		当前密码      = $private:temp1.当前密码
		旧密码       = $private:temp1.旧密码
		被控机os类型   = ''
		被控机显示名    = $private:temp1.被控机显示名
		被控机分组名    = $private:temp1.被控机分组名
		密码写入节点时间戳 = $private:temp1.密码写入节点时间戳
		被控机uuid   = $private:temp1.被控机uuid
	}
	$private:所有被控机999 = $private:所有被控机999 + $private:temp2
}

if ( $PSVersionTable.psversion.major -lt 6 )
{
	$private:所有被控机999 | Export-Csv -LiteralPath $global:被控机列表文件 -Encoding UTF8 -NoTypeInformation
}
else
{
	$private:所有被控机999 | Export-Csv -LiteralPath $global:被控机列表文件 -Encoding UTF8bom -NoTypeInformation
}
$private:所有被控机999 = $null

$global:zd只读nodelist文件 = $false
& 'zd只读nodelist文件.ps1'
& 'sxa刷新所有被控机对象的_os类型属性.ps1'

exit 0
