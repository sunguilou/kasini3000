﻿#建议保存编码为：bom头 + utf8
#
if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

$log日志文件全目录 = "${global:主控机db目录}/d当前库"

if (Test-Path -LiteralPath $log日志文件全目录)
{
	$kaiiit的pid文件_rs = "$log日志文件全目录/kaiiit_pid_rs.txt"
	Set-Content -LiteralPath $kaiiit的pid文件_rs -Value $pid -Encoding UTF8 -Force
}
else
{
	Write-Error '找不到日志文件目录'
	exit 1
}

exit 0
