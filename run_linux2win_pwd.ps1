﻿#建议保存编码为：bom头 + utf8

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[Alias("ipaddress")][String]$目的ip地址,

	[scriptblock]$powershell代码块
)


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}

if ($IsLinux -eq $True)
{
	& '/etc/kasini3000/0k_source.ps1'
}

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	Write-Error "应该使用另一个脚本！"
	exit 1
}


if ($IsLinux -eq $True)
{
	Write-Error "不支持从linux【主控机】，经过winrm协议，控制win【被控机】！"
	exit 2
}

exit 0

