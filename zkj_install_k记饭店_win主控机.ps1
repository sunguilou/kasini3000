﻿#建议保存编码为：bom头 + utf8
#Requires -RunAsAdministrator
#安装pwsh，openssh，设定winrm，安装powershell依赖库，安装pwsh依赖库，任务计划安装k_crontab.ps1。

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$global:kasini3000目录 = 'c:\ProgramData\kasini3000'

if ($PSVersionTable.PSVersion.Major -lt 3)
{
	Write-Error '错误：powershell版本太低'
	Exit 11
}

if ($PSCommandPath -ne 'c:\ProgramData\kasini3000\zkj_install_k记饭店_win主控机.ps1')
{
	Write-Error '错误：此脚本不应该存储在这个目录中。'
	Write-Warning $PSCommandPath
	Exit 4
}


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	& 'c:\ProgramData\kasini3000\0k_source.ps1'
}
Set-Location $env:temp


if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{

	$win7 = powershell.exe -command { (Get-WmiObject -Class Win32_OperatingSystem).Caption -like "*7*" }
	$win2008 = powershell.exe -command { (Get-WmiObject -Class Win32_OperatingSystem).Caption -like "*2008*" }
	if ( $win7 -or $win2008 )
	{
		Write-Error 'win7,win2008缺少部分库。无法支持win7,无法支持win2008'
		Exit 1
	}

	if ([System.Environment]::Is64BitOperatingSystem -eq $True)
	{

	}
	else
	{
		Write-Error '不支持32位操作系统！'
		Exit 2
	}


	#安装pwsh
	if ((Test-Path -LiteralPath 'c:\Program Files\PowerShell\7-preview\pwsh.exe') -or (Test-Path -LiteralPath 'c:\Program Files\PowerShell\7\pwsh.exe') )
	{

	}
	else
	{
		Write-Warning '正在下载并安装pwsh，请等待。。。'
		foreach ($temp11 in 1..10)
		{
			$json2 = Invoke-RestMethod 'https://api.github.com/repos/powershell/powershell/releases/latest'
			if ( ($json2 -eq $null) -or ($json2 -eq '') )
			{
				Start-Sleep 6
			}
			else
			{
				break
			}
		}

		if ( ($json2 -eq $null) -or ($json2 -eq '') )
		{
			Write-Error "错误：pwsh下载路径获取失败！请手动下载：`nhttps://github.com/powershell/powershell/releases"
			exit 3
		}

		foreach ($temp1 in $json2.assets.browser_download_url)
		{
			if ($temp1 -match 'win-x64.msi')
			{
				$安装包url = $temp1
			}
		}
		Start-Process -FilePath 'C:\Windows\system32\msiexec.exe' -ArgumentList "/i $安装包url /qn /l*v msiexec_log.txt " -Verb runas
		Start-Sleep -Seconds 3

		if ( (Test-Path -LiteralPath 'c:\Program Files\PowerShell\7\pwsh.exe') -or (Test-Path -LiteralPath 'c:\Program Files\PowerShell\7-preview\pwsh.exe'))
		{

		}
		else
		{
			Write-Error '错误： pwsh安装失败'
			exit 5
		}
	}

	#安装openssh
	$temp21 = ssh.exe -V *>&1
	$temp22 = $temp21.tostring().ToLower().Contains('openssh')
	if ( (Test-Path -LiteralPath 'c:\Windows\System32\OpenSSH\ssh.exe') -or $temp22)
	{

	}
	else
	{
		Write-Warning '正在下载并安装openssh，请等待。。。'
		foreach ($temp12 in 1..10)
		{
			$openssh = Invoke-RestMethod 'https://api.github.com/repos/powershell/Win32-OpenSSH/releases/latest'
			if ( ($openssh -eq $null) -or ($openssh -eq '') )
			{
				Start-Sleep 6
			}
			else
			{
				break
			}
		}

		if ( ($openssh -eq $null) -or ($openssh -eq '') )
		{
			Write-Error "错误：openssh下载路径获取失败！请手动下载：`nhttps://github.com/powershell/Win32-OpenSSH/releases"
			exit 7
		}

		foreach ($temp2 in $openssh.assets.browser_download_url)
		{
			if ($temp2 -match 'win64.zip')
			{
				$安装包url = $temp2
			}
			$文件名 = Split-Path -Path $安装包url -Leaf
			$文件名2 = "${pwd}\${文件名}"
			#(New-Object 'System.Net.WebClient').DownloadFile($安装包url,$文件名2)
			Invoke-RestMethod -Uri $安装包url -OutFile $文件名2
			Start-Sleep -Seconds 3

			if (Test-Path -LiteralPath $文件名2)
			{
				Expand-Archive -LiteralPath $文件名2 -DestinationPath "./" -Force
				mkdir "${global:kasini3000目录}\openssh"
				Copy-Item -LiteralPath ".\OpenSSH-Win64\*" -Destination "${global:kasini3000目录}\openssh" -Force
				[string]$新path = $env:Path + ";${global:kasini3000目录}\openssh"
				[System.Environment]::SetEnvironmentvariable('path',$新path,'user')
			}
			else
			{
				Write-Error '错误： openssh安装失败'
				exit 6
			}
		}
	}


	#设定winrm
	Enable-PSRemoting -SkipNetworkProfileCheck -Force
	Set-NetFirewallRule -Name "WINRM-HTTP-In-TCP-NoScope" -RemoteAddress Any
	Set-NetFirewallRule -Name "WINRM-HTTP-In-TCP" -RemoteAddress Any
	Restart-Service WinRM   #并重启服务
	Start-Sleep -Seconds 1
	Set-Item WSMan:\localhost\Client\TrustedHosts -value * -Force
	Restart-Service WinRM   #并重启服务


	#安装ps依赖库。
	Write-Warning '正在下载并安装powershell依赖库，请等待。。。'
	powershell.exe -command { Install-PackageProvider -Name NuGet  -Force }
	powershell.exe -command { Set-PSRepository -Name PSGallery -InstallationPolicy Trusted }
	powershell.exe -command { Install-Module -Name powershellget -Force }
	powershell.exe -command { Install-Module -Name PackageManagement -Force }
	powershell.exe -command { Install-Module -Name PSReadLine -Force }
	powershell.exe -command { Install-Module -Name powershell-yaml }
	powershell.exe -command { Install-Module -Name PoshRSJob }
	powershell.exe -command { Install-Module -Name ThreadJob }
	powershell.exe -command { Install-Module -Name winscp }
	powershell.exe -command { Install-Module -Name ImportExcel }

	if (Test-Path -LiteralPath 'c:\Program Files\PowerShell\6\pwsh.exe')
	{
		& 'c:\Program Files\PowerShell\6\pwsh.exe' -command { Install-Module -Name powershell-yaml }
		& 'c:\Program Files\PowerShell\6\pwsh.exe' -command { Install-Module -Name PoshRSJob }
		& 'c:\Program Files\PowerShell\6\pwsh.exe' -command { Install-Module -Name winscp }
		& 'c:\Program Files\PowerShell\6\pwsh.exe' -command { Install-Module -Name ImportExcel }
	}

	if (Test-Path -LiteralPath 'c:\Program Files\PowerShell\7\pwsh.exe')
	{
		& 'c:\Program Files\PowerShell\7\pwsh.exe' -command { Install-Module -Name powershell-yaml }
		& 'c:\Program Files\PowerShell\7\pwsh.exe' -command { Install-Module -Name PoshRSJob }
		& 'c:\Program Files\PowerShell\7\pwsh.exe' -command { Install-Module -Name winscp }
		& 'c:\Program Files\PowerShell\7\pwsh.exe' -command { Install-Module -Name ImportExcel }
	}

	if (Test-Path -LiteralPath 'c:\Program Files\PowerShell\7-preview\pwsh.exe')
	{
		& 'c:\Program Files\PowerShell\7-preview\pwsh.exe' -command { Install-Module -Name powershell-yaml }
		& 'c:\Program Files\PowerShell\7-preview\pwsh.exe' -command { Install-Module -Name PoshRSJob }
		& 'c:\Program Files\PowerShell\7-preview\pwsh.exe' -command { Install-Module -Name winscp }
		& 'c:\Program Files\PowerShell\7-preview\pwsh.exe' -command { Install-Module -Name ImportExcel }
	}

	$winscp_ver = '5.15.2.0'
	Copy-Item -Force -LiteralPath "${global:kasini3000目录}\lib\kasini3000\WinSCP\$winscp_ver\lib\WinSCPnet.dll" -Destination "c:\Users\user001\Documents\PowerShell\Modules\WinSCP\$winscp_ver\lib\WinSCPnet.dll"
	Copy-Item -Force -LiteralPath "${global:kasini3000目录}\lib\kasini3000\WinSCP\$winscp_ver\bin\WinSCP.exe" -Destination "c:\Users\user001\Documents\PowerShell\Modules\WinSCP\$winscp_ver\bin\WinSCP.exe"

	#安装sqlite
	Copy-Item -LiteralPath 'c:\ProgramData\kasini3000\cmdb\win.SQLite.Interop.dll' -Destination 'c:\ProgramData\kasini3000\cmdb\SQLite.Interop.dll' -Force

	#安装 k_crontab
	& 'u库定时任务_建立任务计划_win.ps1'


	#复制快捷方式
	Copy-Item -LiteralPath 'c:\ProgramData\kasini3000\kc.lnk' -Destination "$env:USERPROFILE\Desktop\"
	Copy-Item -LiteralPath 'c:\ProgramData\kasini3000\kcp.lnk' -Destination "$env:USERPROFILE\Desktop\"
	Copy-Item -LiteralPath 'c:\ProgramData\kasini3000\admin_gui\多线程list.lnk' -Destination "$env:USERPROFILE\Desktop\"
	Copy-Item -LiteralPath 'c:\ProgramData\kasini3000\admin_gui\多线程table.lnk' -Destination "$env:USERPROFILE\Desktop\"
	Copy-Item -LiteralPath 'c:\ProgramData\kasini3000\admin_gui\多进程list.lnk' -Destination "$env:USERPROFILE\Desktop\"
	Copy-Item -LiteralPath 'c:\ProgramData\kasini3000\admin_gui\多进程table.lnk' -Destination "$env:USERPROFILE\Desktop\"
	Copy-Item -LiteralPath 'c:\ProgramData\kasini3000\admin_gui\定时任务_list输出.lnk' -Destination "$env:USERPROFILE\Desktop\"

}

exit 0
