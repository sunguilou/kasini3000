﻿#建议保存编码为：bom头 + utf8

if ($global:已经setpwsh -eq $true)
{
	exit
}
$global:已经setpwsh = $true

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	$private:ps7pvw = 'c:\Program Files\PowerShell\7-preview\pwsh.exe'
	$private:ps7w = 'c:\Program Files\PowerShell\7\pwsh.exe'
	$private:ps6w = 'c:\Program Files\PowerShell\6\pwsh.exe'
	$private:ps51w = 'C:\windows\System32\WindowsPowerShell\v1.0\powershell.exe'

	if (Test-Path -LiteralPath $private:ps7w)
	{
		[string]$global:p_w_s_h_7 = $private:ps7w
	}
	elseif (Test-Path -LiteralPath $private:ps7pvw)
	{
		[string]$global:p_w_s_h_7 = $private:ps7pvw
	}

	if (Test-Path -LiteralPath $private:ps6w)
	{
		[string]$global:p_w_s_h_6 = $private:ps6w
	}

	if (Test-Path -LiteralPath $private:ps7w)
	{
		[string]$global:p_w_s_h = $private:ps7w
	}
	elseif (Test-Path -LiteralPath $private:ps7pvw)
	{
		[string]$global:p_w_s_h = $private:ps7pvw
	}
	elseif (Test-Path -LiteralPath $private:ps6w)
	{
		[string]$global:p_w_s_h = $private:ps6w
	}
	else
	{
		[string]$global:p_w_s_h = $private:ps51w
	}
}

if ($IsLinux -eq $True)
{
	$private:ps7pvl = '/opt/microsoft/powershell/7-preview/pwsh'
	$private:ps7l = '/opt/microsoft/powershell/7/pwsh'
	$private:ps6l = '/opt/microsoft/powershell/6/pwsh'

	if (Test-Path -LiteralPath $private:ps7l)
	{
		[string]$global:p_w_s_h_7 = $private:ps7l
	}
	elseif (Test-Path -LiteralPath $private:ps7pvl)
	{
		[string]$global:p_w_s_h_7 = $private:ps7pvl
	}

	if (Test-Path -LiteralPath $private:ps6l)
	{
		[string]$global:p_w_s_h_6 = $private:ps6l
	}

	if (Test-Path -LiteralPath $private:ps7l)
	{
		[string]$global:p_w_s_h = $private:ps7l
	}
	elseif (Test-Path -LiteralPath $private:ps7pvl)
	{
		[string]$global:p_w_s_h = $private:ps7pvl
	}
	else
	{
		[string]$global:p_w_s_h = $private:ps6l
	}
}

Write-Verbose [string]$global:p_w_s_h
Write-Verbose [string]$global:p_w_s_h_7
Write-Verbose [string]$global:p_w_s_h_6
exit 0
